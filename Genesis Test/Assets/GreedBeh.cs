using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

namespace Mainspace
{
    public class GreedBeh : MonoBehaviour
    {
        [SerializeField] private RectTransform textHolder;
        [SerializeField] private TextMeshProUGUI text;
        [SerializeField] private Image fillAriaImage;
        [SerializeField] private Color green;
        [SerializeField] private Color red;
        [Space]
        [SerializeField] private int    greedMaxLVL;
        [SerializeField] private float  greedMaxHP;
        [SerializeField] private float  timeToDifficultyUp;
        [Space]
        [SerializeField] private float  coinsPriceOnHpMult;

        private float _greedHpCurrent;
        private int _greedLvlCurrent;
        public static int GreedLVL;

        private Coroutine _greed;
        private Coroutine _diff;

        private void Start()
        {
            _greedHpCurrent = greedMaxHP;
            _greedLvlCurrent = 1;
            GreedLVL = _greedLvlCurrent;
            StopRoutines();
            GameCore.Managers.EventManager.Subscribe<GameEvents.Battle.CoinCollected>(SomethingCollected);
            GameCore.Managers.EventManager.Subscribe<GameEvents.Battle.Pause>(Pause);
            GameCore.Managers.EventManager.Subscribe<GameEvents.Battle.Unpause>(Unpause);
            GameCore.Managers.EventManager.Subscribe<GameEvents.Battle.StartGame>(StartGame);
            GameCore.Managers.EventManager.Subscribe<GameEvents.Battle.PlayerDie>(StopGame);
        }

        private void OnDestroy()
        {
            GameCore.Managers.EventManager.Unsubscribe<GameEvents.Battle.CoinCollected>(SomethingCollected);
            GameCore.Managers.EventManager.Unsubscribe<GameEvents.Battle.Pause>(Pause);
            GameCore.Managers.EventManager.Unsubscribe<GameEvents.Battle.Unpause>(Unpause);
            GameCore.Managers.EventManager.Unsubscribe<GameEvents.Battle.StartGame>(StartGame);
            GameCore.Managers.EventManager.Unsubscribe<GameEvents.Battle.PlayerDie>(StopGame);
        }
        private void StartGame(GameEvents.Battle.StartGame e)
        {
            Unpause(new GameEvents.Battle.Unpause { });
        }
        private void StopGame(GameEvents.Battle.PlayerDie e)
        {
            StopRoutines();
        }
        private void Pause(GameEvents.Battle.Pause e)
        {
            StopRoutines();
        }
        private void Unpause(GameEvents.Battle.Unpause e)
        {
            if (PlayerController.PlayerIsAlive)
            {
                StopRoutines();
                _greed = StartCoroutine(GreedController());
                _diff = StartCoroutine(DifficultyApper());
            }
        }
        private void StopRoutines()
        {
            if(_greed != null)
            {
                StopCoroutine(_greed);
                _greed = null;
            }
            if (_diff != null)
            {
                StopCoroutine(_diff);
                _diff = null;
            }
        }
        private IEnumerator GreedController()
        {
            while (_greedHpCurrent > 0)
            {
                var delta = Time.deltaTime * (_greedLvlCurrent * _greedLvlCurrent);
                _greedHpCurrent -= delta;
                var progress = _greedHpCurrent / greedMaxHP;
                fillAriaImage.fillAmount = progress;
                fillAriaImage.color = Color.Lerp(red, green, progress);
                yield return null;
            }
            GameCore.Managers.EventManager.Trigger(new GameEvents.Battle.GreedHp0 { });
        }
        private IEnumerator DifficultyApper()
        {
            while (_greedHpCurrent > 0)
            {
                yield return new WaitForSeconds(timeToDifficultyUp * _greedLvlCurrent);
                _greedLvlCurrent = Mathf.Clamp(_greedLvlCurrent + 1, 1, greedMaxLVL);
                if(_greedLvlCurrent <= greedMaxLVL)
                {
                    GreedLVL = _greedLvlCurrent;
                    text.text = _greedLvlCurrent.ToString();
                    Punch(0.3f);
                }
            }
        }
        private void SomethingCollected(GameEvents.Battle.CoinCollected e)
        {
            _greedHpCurrent += e.price * coinsPriceOnHpMult;
            _greedHpCurrent = Mathf.Clamp(_greedHpCurrent + e.price, 0, greedMaxHP);
        }
        public void Punch(float power)
        {
            textHolder.DOPunchScale(Vector3.one * power, 0.1f, 1, 1);
        }
    }
}