using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mainspace
{
    public class CameraBeh : MonoBehaviour
    {
        [SerializeField] private float lerpVal;
        [SerializeField] private Transform player;
        private Transform trans;

        private float initialDistToPlayer;
        private float camHight;

        private void Start()
        {
            trans = transform;
            camHight = trans.position.y;
            initialDistToPlayer = Vector3.Distance(player.position, trans.position);
            //trans.position = player.position;
        }

        private void LateUpdate()
        {
            var rot = Quaternion.LookRotation((player.position - trans.position).normalized);
            trans.rotation = Quaternion.Lerp(trans.rotation, rot, lerpVal);
            trans.position = Vector3.Lerp(trans.position, (player.position - player.forward * initialDistToPlayer).SwapCoord(0, camHight, 0), lerpVal);
        }
    }
}