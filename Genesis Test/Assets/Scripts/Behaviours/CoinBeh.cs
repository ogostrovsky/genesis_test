using System.Collections;
using UnityEngine;

namespace Mainspace
{
    public class CoinBeh : MonoBehaviour
    {
        [SerializeField] private int price;
        [SerializeField] private float fallLasting;
        [SerializeField] private float fallHight;
        [SerializeField] private AnimationCurve curve;
        [SerializeField] private bool enableRotation;
        [SerializeField] private float rotationSpeed;
        [SerializeField] private float lifetime;
        [SerializeField] private GameObject staticVfx;
        [SerializeField] private GameObject collectVfx;
        [SerializeField] private ParticleSystem onCollisioVfx;
        [SerializeField] private float collisionPlayTimePoint;

        private Transform trans;
        private ShadowBeh shadow;
        private ParticleSystem _staticVfx;
        private bool _isCollisionVFXPlayed;

        private void Awake()
        {
            shadow = GetComponent<ShadowBeh>();
            trans = transform;
        }
        private void OnEnable()
        {
            _isCollisionVFXPlayed = false;
            StartCoroutine(Fall());
            StartCoroutine(DelayedDie());
            if (enableRotation)
            {
                StartCoroutine(Rotator());
            }
        }
        private IEnumerator Rotator()
        {
            while (true)
            {
                trans.Rotate(Vector3.up, rotationSpeed);
                yield return null;
            }
        }
        private IEnumerator Fall()
        {
            var startPoint = trans.position + Vector3.up * fallHight;
            var endPoint = trans.position + new Vector3(0, -trans.position.y, 0);
            shadow.Init();
            shadow.SetPos(endPoint);

            var timer = 0f;
            while (timer < fallLasting)
            {
                var progress = curve.Evaluate(timer / fallLasting);
                shadow.SetSize(progress);
                trans.position = Vector3.Lerp(startPoint, endPoint, progress);
                yield return null;
                timer += Time.deltaTime;
                PlayOnCollionVFX(progress);
            }
            trans.position = endPoint;
            PlayStaticVFX();
        }
        private void PlayOnCollionVFX(float timePoint)
        {
            if (!_isCollisionVFXPlayed && onCollisioVfx && timePoint > collisionPlayTimePoint)
            {
                _isCollisionVFXPlayed = true;
                onCollisioVfx.Play();
                if(price > 100)
                {
                    //GameCore.Managers.SoundsPlayerManager.PlaySound(SoundName.CoinFall);    
                }
            }
        }
        private void PlayStaticVFX()
        {
            if (staticVfx)
            {
                _staticVfx = PoolManager.Instance.GetObjectWithTimer(staticVfx, 30, true).GetComponent<ParticleSystem>();
                _staticVfx.GetComponent<Transform>().position = trans.position;
                _staticVfx.Play();
            }
        }
        private void CollectVFX()
        { 
            if (collectVfx)
            {
                var vfx = PoolManager.Instance.GetObjectWithTimer(collectVfx, 2, true).GetComponent<ParticleSystem>();
                vfx.GetComponent<Transform>().position = trans.position.AddCoord(0, 1f, 0);
                vfx.Play();
            }
        }

        private IEnumerator DelayedDie()
        {
            yield return new WaitForSeconds(lifetime);
            Die();
        }
        private void Colected()
        {
            GameCore.Managers.EventManager.Trigger(new GameEvents.Battle.CoinCollected { price = price });
            CollectVFX();
            Die();
        }
        private void Die()
        {
            CollectVFX();
            if (staticVfx)
            { 
                _staticVfx.Stop(); 
            }
            shadow.Store();
            GameCore.Managers.PoolManager.StoreObj(gameObject);
        }
        private void OnTriggerEnter(Collider other)
        {
            Colected();
        }
    }
}