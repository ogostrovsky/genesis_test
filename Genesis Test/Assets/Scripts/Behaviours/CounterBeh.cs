using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

namespace Mainspace
{
    public class CounterBeh : MonoBehaviour
    {
        [SerializeField] private RectTransform holderRect;
        [SerializeField] private TextMeshProUGUI value;
        [SerializeField] private float lerpLasting;
        [SerializeField] private AnimationCurve curve;
        private int _coinsCount;
        private Coroutine _lerpRout;
        private void SetValue(int amount) => value.text = amount.ToString();

        void Start()
        {
            GameCore.Managers.EventManager.Subscribe<GameEvents.Battle.CoinCollected>(UpdateCounter);
            GameCore.Managers.EventManager.Subscribe<GameEvents.Battle.PlayerDie>(SendResult);
        }

        private void OnDestroy()
        {
            GameCore.Managers.EventManager.Unsubscribe<GameEvents.Battle.CoinCollected>(UpdateCounter);
            GameCore.Managers.EventManager.Unsubscribe<GameEvents.Battle.PlayerDie>(SendResult);
        }
        private void UpdateCounter(GameEvents.Battle.CoinCollected e)
        {
            _coinsCount += e.price * GreedBeh.GreedLVL;

            if (_lerpRout != null)
            {
                StopCoroutine(_lerpRout);
                _lerpRout = null;
            }

            if (e.price < 100)
            {
                Punch(0.1f);
                GameCore.Managers.SoundsPlayerManager.PlaySound(SoundName.CoinColected);
            }
            else
            {
                GameCore.Managers.SoundsPlayerManager.PlaySound(SoundName.CoinMoreCollect);
                Punch(0.2f);
            }

            _lerpRout = StartCoroutine(LerperNumber(savedRoutValue, _coinsCount));
        }
        private int savedRoutValue;
        private IEnumerator LerperNumber(int start, int end)
        {
            var timer = 0f;

            while (timer < lerpLasting)
            {
                var progress = curve.Evaluate(timer / lerpLasting);
                savedRoutValue = (int)Mathf.Lerp(start, end, progress);
                SetValue(savedRoutValue);
                yield return null;
                timer += Time.deltaTime;
            }
        }
        public void Punch(float power)
        {
            holderRect.DOPunchScale(Vector3.one * power, 0.1f, 1, 1);
        }

        private void SendResult(GameEvents.Battle.PlayerDie e)
        {
            GameCore.Managers.EventManager.Trigger(new GameEvents.Battle.NewScoreResult { result = _coinsCount });
        }
    }
}