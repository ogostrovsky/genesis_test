using UnityEngine;

namespace Mainspace
{
    public class PropBeh : MonoBehaviour
    {
        private Transform trans;
        private Transform playerTrans;
        private float startDistToPlayer;
        private ShadowBeh shadow;

        void OnEnable()
        {
            shadow = GetComponent<ShadowBeh>();
            trans = transform;
            shadow.Init();
            shadow.SetPos(trans.position);

            playerTrans = BattleManager.PlayerTrans;
            trans.localScale = Vector3.zero;
            startDistToPlayer = Vector3.Distance(trans.position, playerTrans.position);
        }

        // Update is called once per frame
        void Update()
        {
            var dist = Vector3.Distance(trans.position, playerTrans.position);
            if (dist > startDistToPlayer)
            {
                PoolManager.Instance.StoreObj(gameObject);
            }
            else
            {
                var scale = 1 - (dist / startDistToPlayer);
                trans.localScale = new Vector3(scale, scale, scale);
                shadow.SetSize(scale);
                shadow.SetPos(trans.position);
            }
        }
    }
}