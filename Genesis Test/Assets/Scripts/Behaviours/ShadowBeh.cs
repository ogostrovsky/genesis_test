using UnityEngine;

namespace Mainspace
{
    public class ShadowBeh : MonoBehaviour
    {
        [SerializeField] private Vector3 size;
        [SerializeField] private GameObject shadow;

        private Transform _trans;
        public void Init()
        {
            _trans = GameCore.Managers.PoolManager.GetObj(shadow, true).transform;
            _trans.localScale = size;
            _trans.rotation = Quaternion.Euler(0, 180, 0);
        }

        public void SetPos(Vector3 pos)
        {
            _trans.position = new Vector3(pos.x, 0.51f, pos.z);
        }

        public void SetSize(float size)
        {
            _trans.localScale = this.size * size;
        }
        public void Store()
        {
            GameCore.Managers.PoolManager.StoreObj(_trans.gameObject);
        }

    }
}