using System;

namespace Mainspace
{
    [Serializable]
    public struct BundleInfo
    {
        public BundleInfo(int version, string url)
        {
            BundleVersion = version;
            URL = url;
        }
        public int BundleVersion;
        public string URL;
    }
}