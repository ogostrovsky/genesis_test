using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Mainspace
{
    public class ButtonBehaviour : MonoBehaviour
    {
        [SerializeField] private bool pingPongActive;
        private Button _button;
        private RectTransform rect;
        private void Start()
        {
            _button = GetComponentInChildren<Button>();
            _button.onClick.AddListener(() => GameCore.Managers.SoundsPlayerManager.PlaySound(SoundName.ButtunClick));
            rect = GetComponent<RectTransform>();
            GameCore.Managers.EventManager.Subscribe<GameEvents.Menu.CancasMoveStart>(ButtonOff);
            GameCore.Managers.EventManager.Subscribe<GameEvents.Menu.CancasMoveEnd>(ButtonOn);
            if (pingPongActive)
            {
                PingPong();
            }
        }
        private void OnDestroy()
        {
            GameCore.Managers.EventManager.Unsubscribe<GameEvents.Menu.CancasMoveStart>(ButtonOff);
            GameCore.Managers.EventManager.Unsubscribe<GameEvents.Menu.CancasMoveEnd>(ButtonOn);
        }

        private void ButtonOff(GameEvents.Menu.CancasMoveStart e)
        {
            SetButtunInteractibility(false);
        }

        private void ButtonOn(GameEvents.Menu.CancasMoveEnd e)
        {
            SetButtunInteractibility(true);
        }

        private void SetButtunInteractibility(bool isActive)
        {
            if (_button)
            {
                _button.interactable = isActive;
            }
        }

        public void Punch()
        {
            GameCore.Managers.SoundsPlayerManager.PlaySound(SoundName.ButtunClick);
            rect.DOPunchScale(Vector3.one * 0.1f, 0.1f, 1, 1);
        }
        private void PingPong()
        {
            rect.DOPunchScale(Vector3.one * 0.1f, 0.75f, 1, 1).SetLoops(-1, LoopType.Yoyo);
        }
    }
}