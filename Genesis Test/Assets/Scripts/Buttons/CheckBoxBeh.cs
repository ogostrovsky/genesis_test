using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

namespace Mainspace
{
    public class CheckBoxBeh : MonoBehaviour
    {
        [SerializeField] protected float animLength;
        [SerializeField] protected GameObject On;
        [SerializeField] protected GameObject Off;

        protected float _scaleUp = 1.15f;
        protected Button _button;
        protected bool _isOn;

        private void Start()
        {
            Init();
        }
        protected virtual void Init()
        {
            _button = GetComponent<Button>();
        }
        public virtual void Switcher()
        {
            _isOn = !_isOn;
            if (_isOn)
            {
                Anim(Off, On);
            }
            else
            {
                Anim(On, Off);
            }
        }
        protected virtual void Anim(GameObject objToHide, GameObject objToShow)
        {
            GameCore.Managers.SoundsPlayerManager.PlaySound(SoundName.ButtunClick);
            _button.interactable = false;
            Sequence sequence = DOTween.Sequence();
            sequence.Append(objToHide.transform
                .DOScale(Vector3.one * _scaleUp, animLength * (_scaleUp - 1))
                .SetEase(Ease.InCubic));

            sequence.Append(objToHide.transform
                .DOScale(Vector3.zero, animLength * (2 - _scaleUp))
                .SetEase(Ease.OutCubic)
                .OnComplete(() =>
                {
                    objToHide.SetActive(false);
                    objToShow.SetActive(true);
                    objToShow.transform.localScale = Vector3.zero;
                }));

            sequence.Append(objToShow.transform
                .DOScale(Vector3.one * _scaleUp, animLength * (2 - _scaleUp))
                .SetEase(Ease.InCubic));

            sequence.Append(objToShow.transform
                .DOScale(Vector3.one, animLength * (_scaleUp - 1))
                .SetEase(Ease.OutCubic))
                .OnComplete(() => { _button.interactable = true; GameCore.Managers.SoundsPlayerManager.PlaySound(SoundName.ButtunClick); });
        }
    }
}