namespace Mainspace
{
    public class CheckBoxBehMus : CheckBoxBeh
    {
        protected override void Init()
        {
            base.Init();
            _isOn = GameCore.Managers.SoundsPlayerManager.IsMusicOn;
            On.SetActive(_isOn);
            Off.SetActive(!_isOn);    
        }

        public override void Switcher()
        {
            base.Switcher();
            GameCore.Managers.EventManager.Trigger(new GameEvents.Menu.MusicSettingsChanged { value = _isOn });
        }
    }
}