using DG.Tweening;
using UnityEngine;

namespace Mainspace
{
    public class CheckBoxBehSfx : CheckBoxBeh
    {
        protected override void Init()
        {
            base.Init();
            _isOn = GameCore.Managers.SoundsPlayerManager.IsSfxOn;
            On.SetActive(_isOn);
            Off.SetActive(!_isOn);
        }

        public override void Switcher()
        {
            base.Switcher();
            GameCore.Managers.EventManager.Trigger(new GameEvents.Menu.SfxSettingsChanged { value = _isOn });
        }
        protected override void Anim(GameObject objToHide, GameObject objToShow)
        {
            _button.interactable = false;
            Sequence sequence = DOTween.Sequence();
            sequence.Append(objToHide.transform
                .DOScale(Vector3.one * _scaleUp, animLength * (_scaleUp - 1))
                .SetEase(Ease.InCubic));

            sequence.Append(objToHide.transform
                .DOScale(Vector3.zero, animLength * (2 - _scaleUp))
                .SetEase(Ease.OutCubic)
                .OnComplete(() =>
                {
                    objToHide.SetActive(false);
                    objToShow.SetActive(true);
                    objToShow.transform.localScale = Vector3.zero;
                }));

            sequence.Append(objToShow.transform
                .DOScale(Vector3.one * _scaleUp, animLength * (2 - _scaleUp))
                .SetEase(Ease.InCubic));

            sequence.Append(objToShow.transform
                .DOScale(Vector3.one, animLength * (_scaleUp - 1))
                .SetEase(Ease.OutCubic))
                .OnComplete(() => { 
                    _button.interactable = true;
                    GameCore.Managers.SoundsPlayerManager.PlaySound(SoundName.ButtunClick);
                });
        }
    }
}