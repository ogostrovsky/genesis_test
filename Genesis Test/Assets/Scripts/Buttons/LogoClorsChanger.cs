using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

namespace Mainspace
{
    public class LogoClorsChanger : MonoBehaviour
    {
        [SerializeField] private Color[] colors;
        [SerializeField] private TextMeshProUGUI text;
        [SerializeField] private RectTransform rect;

        private int _colorIndex;

        private Tween _piunchTween;
        public void ColorChange()
        {
            GameCore.Managers.SoundsPlayerManager.PlaySound(SoundName.ButtunClick);
            _colorIndex = _colorIndex < colors.Length - 1 ? _colorIndex + 1 : 0;
            _piunchTween.Kill();
            _piunchTween = rect.DOPunchScale(Vector3.one * 0.05f, 0.1f, 1, 1);
            text.color = colors[_colorIndex];
        }
    }
}