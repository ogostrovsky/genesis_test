using System.IO;
using UnityEditor;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace Mainspace
{
    public class JsonBuilder
    {
        [MenuItem("Assets/ Create Json")]
        private static void BuildAssetBundles()
        {
            var bundleInfo = new BundleInfo(0, "ur");
            var jsonStr = JsonConvert.SerializeObject(bundleInfo);
            Debug.Log("Json at " + Application.persistentDataPath);
            File.WriteAllText(Path.Combine(Application.persistentDataPath, "bundleInfo.json"), jsonStr);
        }
    }
}

