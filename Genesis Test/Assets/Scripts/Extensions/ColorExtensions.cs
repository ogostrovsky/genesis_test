﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ColorExtensions  {
    public static Color ChangeAlpha(this Color col, float a) {
        return new Color(col.r, col.g, col.b, a);
    }
	
}
