﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CoroutineExtensions {
    
    public static void End(this Coroutine routine, ref Coroutine me, MonoBehaviour owner) {
        if (routine != null && me == routine) {
            owner.StopCoroutine(routine);
            me = null;
        }
        else if (me != routine)
        {
            Debug.Log("You truing to stop wrong coroutine!");
        }
    }
}
