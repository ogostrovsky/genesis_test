﻿using System;

public static class DateTimeExtensions {
    public static int TimeStamp(this DateTime time) {
        return (int)((DateTimeOffset)time).ToUnixTimeSeconds();
    }
	
}
