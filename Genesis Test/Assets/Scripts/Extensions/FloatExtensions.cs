﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public static class FloatExtensions {
    public static int   Sign          (this float val) {
        return val > 0 ? 1 : -1;
    }

    public static bool IsPositive    (this float val) {
        return val > 0;
    }

    public static string ToTimeString(this float time) {
        int minutes = (int)time / 60;
        int seconds = (int)time - 60 * minutes;

        return string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
