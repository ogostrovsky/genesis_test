﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameObjectExtensions {
    public static bool DestroyIfSingletonExist(this MonoBehaviour mono) {
        var monoObjects = GameObject.FindObjectsOfType(mono.GetType());
        for (int i = 0; i < monoObjects.Length; i++) {
            if (monoObjects[i] != mono) {
                GameObject.Destroy(mono.gameObject);
                return true;
            }
        }
        
        return false;
    }

}
