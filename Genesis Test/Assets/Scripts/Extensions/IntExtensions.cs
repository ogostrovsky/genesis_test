﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public static class IntExtensions {
    
    public static int   Sign          (this int val) {
        return val > 0 ? 1 : -1;
    }

    public static bool IsPositive    (this int val) {
        return val > 0;
    }

}
