﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ListExtensions {
    public static T     GetRndElement<T>  (this List<T> list) {
        if (list.Count == 0)
            return default;

        return list[Random.Range(0, list.Count)];
    }

    public static T     GetRandomItemAndRemove<T>  (this List<T> list) {
        if (list.Count == 0)
            return default(T);
        var item = list[Random.Range(0, list.Count)];
        list.Remove(item);

        return item;
    }

    public static List<T> exDistinct<T> (this List<T> list) {
        var length = list.Count;
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                if (j == i)
                    continue;

                if (list[i].Equals(list[j])) {
                    list.RemoveAt(j);
                    length = list.Count;
                }

            }
        }

        return list;
    }
}
