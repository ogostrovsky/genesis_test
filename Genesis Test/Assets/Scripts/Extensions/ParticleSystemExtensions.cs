﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ParticleSystemExtensions {
    public static bool exFullStop      (this ParticleSystem[] mono, bool withChildren = true) {
        var cnt = mono.Length;
        for (int i = 0; i < cnt; i++)
            mono[i]?.Stop(withChildren);

        return false;
    }

    public static bool exFullReset      (this ParticleSystem[] mono, bool withChildren = true) {
        var cnt = mono.Length;
        for (int i = 0; i < cnt; i++)
            mono[i]?.ResetAndPlay(withChildren);

        return false;
    }

    public static bool exFullClear      (this ParticleSystem[] mono, bool withChildren = true) {
        if (mono == null || mono.Length == 0)
            return false;

        var cnt = mono.Length;
        for (int i = 0; i < cnt; i++)
            mono[i]?.exFullClear(withChildren);

        return true;
    }
    public static bool exSetColor       (this ParticleSystem[] mono, Color color) {
        if (mono == null || mono.Length == 0)
            return false;

        var cnt = mono.Length;
        for (int i = 0; i < cnt; i++) {
            if (mono[i] == null)
                continue;

            var main = mono[i].main;
            main.startColor = color;
        }

        return true;
    }


    public static bool ResetAndPlay     (this ParticleSystem mono, bool withChildren = true) {
        mono.Stop   (withChildren);
        mono.Clear  (withChildren);
        mono.Play   (withChildren);


        return false;
    }
    public static bool exFullClear      (this ParticleSystem mono, bool withChildren = true) {
        mono.Stop   (withChildren);
        mono.Clear  (withChildren);

        return false;
    }

}
