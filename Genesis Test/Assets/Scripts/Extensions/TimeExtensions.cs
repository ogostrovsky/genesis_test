﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TimeExtensions  { 
    public static string ToShortForm(this System.TimeSpan t) {
        string shortForm = "";

        if (t.Hours > 0)
        {
            shortForm += string.Format("{0}:", t.Hours.ToString());
        }
        if (t.Minutes > 0)
        {
            shortForm += string.Format("{0}:", t.Minutes.ToString());
        }
        if (t.Seconds > 0)
        {
            shortForm += string.Format("{0}", t.Seconds.ToString());
        }

        return shortForm;
    }

    public static string ToMegaShortForm(this System.TimeSpan t) {
        string shortForm = "";

        if (t.Hours > 0)
        {
            shortForm = string.Format("{0}", t.Hours.ToString());
        }
        else if (t.Minutes > 0)
        {
            shortForm = string.Format("{0}", t.Minutes.ToString());
        }
        else if (t.Seconds > 0)
        {
            shortForm = string.Format("{0}", t.Seconds.ToString());
        }

        return shortForm;
    }

}
