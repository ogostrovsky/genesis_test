﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TransformExtensions {

    public static void SelfDestroy  (this Transform transform) {
        if (transform != null) {
            GameObject.Destroy(transform.gameObject);
        }
    }

    public static void DestroyChildren      (this Transform transform) {
        if (transform.childCount == 0)
            return;

        foreach (Transform child in transform) {
            GameObject.Destroy(child.gameObject);
        }
    }

    public static void DestroyChildrenNow  (this Transform transform) {
        if (transform.childCount == 0)
            return;

        foreach (Transform child in transform) {
            GameObject.DestroyImmediate(child.gameObject);
        }
    }

    public static bool exContains  (this Transform[] transform, string name) {
        var result  = false;
        var count   = transform.Length;
        for (int i = 0; i < count; i++) {
            if (transform[i].name == name) {
                result = true;
                break;
            }

        }

        return result;
    }

    public static bool exContains  (this List<Transform> transform, string name) {
        var result  = false;
        var count   = transform.Count;
        for (int i = 0; i < count; i++) {
            if (transform[i].name == name) {
                result = true;
                break;
            }

        }

        return result;
    }

    public static int exIndex      (this List<Transform> transform, string name, bool ignoreREgister = false) {
        var index  = -1;
        var count   = transform.Count;
        for (int i = 0; i < count; i++) {
            if (ignoreREgister) {
                if (transform[i].name.ToLower() == name.ToLower()) {
                    index = i;
                    break;
                }
            } else {
                if (transform[i].name == name) {
                    index = i;
                    break;
                }
            }

        }

        return index;
    }
}
