﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public static class VectorExtensions {

    public static float Distance(ref Vector3 a, ref Vector3 b) {
        float diff_x = a.x - b.x;
        float diff_y = a.y - b.y;
        float diff_z = a.z - b.z;

        return (float)System.Math.Sqrt(diff_x * diff_x + diff_y * diff_y + diff_z * diff_z);
    }

    public static Vector3 ToV3          (this Vector2 v2) {
        return new Vector3(v2.x, 0, v2.y);
    }
    public static Vector3 ToV3          (this Vector2Int v2) {
        return new Vector3(v2.x, 0, v2.y);
    }

    public static Vector3 Abs           (this Vector3 vec) {
        vec.x = vec.x < 0 ? -vec.x : vec.x;
        vec.y = vec.y < 0 ? -vec.y : vec.y;
        vec.z = vec.z < 0 ? -vec.z : vec.z;

        return vec;
    }

    public static Vector3 Div           (this Vector3 vec, Vector3 sec) {
        vec.x /= sec.x;
        vec.y /= sec.y;
        vec.z /= sec.z;

        return vec;
    }

    public static Vector3 RndDirection  (float angle = 90f, int variations = 4, float startAngleOffset = 45f) {
        var rotation = startAngleOffset + Random.Range(1, variations) * angle;
        return new Vector3( Mathf.Sin(Mathf.Deg2Rad * rotation), 
                            0f, 
                            Mathf.Cos(Mathf.Deg2Rad * rotation));
    }

    public static Vector3 RndDir  () {
        return Random.insideUnitCircle.normalized.ToV3();
    }

    public static Vector3 Mirror        (this Vector3 vec, bool x = true, bool y = true, bool z = true) {
        vec.x = x ? -vec.x : vec.x;
        vec.y = y ? -vec.y : vec.y;
        vec.z = z ? -vec.z : vec.z;

        return vec;
    }

    public static Vector3 SwapCoord     (this Vector3 vec,  Vector3 swapper, bool x, bool y, bool z) {
        vec.x = x ? swapper.x : vec.x;
        vec.y = y ? swapper.y : vec.y;
        vec.z = z ? swapper.z : vec.z;

        return vec;
    }

    public static Vector3 SwapCoord     (this Vector3 vec, float x, float y, float z) {
        vec.x = x == 0 ? vec.x : x;
        vec.y = y == 0 ? vec.y : y;
        vec.z = z == 0 ? vec.z : z;
        
        return vec;
    }

    public static Vector3 AddCoord(this Vector3 vec, float x, float y, float z)
    {
        vec.x += x;
        vec.y += y;
        vec.z += z;

        return vec;
    }


    public static Vector3 Zero          (this Vector3 vec, bool x, bool y, bool z) {
        vec.x = x ? 0 : vec.x;
        vec.y = y ? 0 : vec.y;
        vec.z = z ? 0 : vec.z;

        return vec;
    }

    public static Vector3 RndShft(this Vector3 vec, float magnitude)
    {
        return vec + RndDir() * Random.Range(0, magnitude);
    }

    public static Vector3 RndAbsShift(this Vector3 vec, float magnitude)
    {
        return vec + RndDir() * magnitude;
    }
}
