using UnityEngine;
using DG.Tweening;
using System.Collections;

namespace Mainspace 
{
    public class BattleManager : MonoBehaviour
    {
        [SerializeField] private Transform player;
        [SerializeField] private GameObject[] Coins;
        [SerializeField] private GameObject[] CoinsRare;
        [SerializeField] private GameObject[] props;
        [SerializeField] private float chanceForRare;

        [SerializeField] private float delayBetweenCooinSpawn;
        [SerializeField] private float delayBetweenPropSpawn;
        [SerializeField] private float widthToSpawnCoins;

        private static Transform playerTransform;
        public static Transform PlayerTrans => playerTransform;

        private Coroutine _coins;
        private Coroutine _props;

        private void Start()
        {
            playerTransform = player;
            GameCore.Managers.EventManager.Subscribe<GameEvents.Battle.Pause>(Pause);
            GameCore.Managers.EventManager.Subscribe<GameEvents.Battle.Unpause>(Unpause);
            GameCore.Managers.EventManager.Subscribe<GameEvents.Battle.StartGame>(StartGame);
            GameCore.Managers.EventManager.Subscribe<GameEvents.Battle.PlayerDie>(StopGame);
        }

        private void OnDestroy()
        {
            GameCore.Managers.EventManager.Unsubscribe<GameEvents.Battle.Pause>(Pause);
            GameCore.Managers.EventManager.Unsubscribe<GameEvents.Battle.Unpause>(Unpause);
            GameCore.Managers.EventManager.Unsubscribe<GameEvents.Battle.StartGame>(StartGame);
            GameCore.Managers.EventManager.Unsubscribe<GameEvents.Battle.PlayerDie>(StopGame);
        }
        private void StartRouts()
        {
            if(this != null)
            {
                _coins = StartCoroutine(CoinsDropper());
                _props = StartCoroutine(PropsSpawner());
            }
        }
        private IEnumerator CoinsDropper()
        {
            while (true)
            {
                GameObject coin;
                if(Random.Range(0, 1f) < chanceForRare)
                {
                    coin = GameCore.Managers.PoolManager.GetObj(CoinsRare[Random.Range(0, CoinsRare.Length)]);
                }
                else
                {
                    coin = GameCore.Managers.PoolManager.GetObj(Coins[Random.Range(0, Coins.Length)]);
                }

                coin.transform.position = player.position + player.forward * Random.Range(widthToSpawnCoins / 2, widthToSpawnCoins) + new Vector3(Random.Range(-widthToSpawnCoins / 2, widthToSpawnCoins / 2),0,0);
                coin.SetActive(true);
                yield return new WaitForSeconds(delayBetweenCooinSpawn * Random.Range(0.5f, 1.5f));
            }
        }
        private IEnumerator PropsSpawner()
        {
            while (true)
            {
                var prop = GameCore.Managers.PoolManager.GetObj(props[Random.Range(0, props.Length)]);
                prop.transform.position = player.position + player.forward * widthToSpawnCoins + new Vector3(Random.Range(-widthToSpawnCoins / 2, widthToSpawnCoins / 2), 0, 0);
                prop.SetActive(true);
                yield return new WaitForSeconds(delayBetweenPropSpawn * Random.Range(0.5f, 1.5f));
            }
        }
        private void Pause(GameEvents.Battle.Pause e)
        {
            GameCore.Managers.SoundsPlayerManager.StopPlayingSound(SoundName.BattleMusick);
            StopRoutines();
        }
        private void Unpause(GameEvents.Battle.Unpause e)
        {
            if (PlayerController.PlayerIsAlive)
            {
                GameCore.Managers.SoundsPlayerManager.TryPlayBattleMus();
                StartRouts();
            }
        }
        private void StopGame(GameEvents.Battle.PlayerDie e)
        {
            StopRoutines();
            GameCore.Managers.SoundsPlayerManager.StopPlayingSound(SoundName.BattleMusick);
        }
        private void StartGame(GameEvents.Battle.StartGame e)
        {
            StartRouts();
            GameCore.Managers.SoundsPlayerManager.TryPlayBattleMus();
        }
        private void StopRoutines()
        {
            if (_coins != null)
            {
                StopCoroutine(_coins);
                _coins = null;
            }
            if (_props != null)
            {
                StopCoroutine(_props);
                _props = null;
            }
        }
    }
}