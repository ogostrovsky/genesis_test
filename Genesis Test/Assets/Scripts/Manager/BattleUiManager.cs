using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

namespace Mainspace
{
    public class BattleUiManager : MonoBehaviour
    {
        [SerializeField] private RectTransform mainPanel;
        [SerializeField] private AnimationCurve lerpCurve;
        [SerializeField] private float lerpLasting;
        [SerializeField] private TextMeshProUGUI bestScore;
        [SerializeField] private TextMeshProUGUI bestScore2;
        [SerializeField] private TextMeshProUGUI bestScore2Descript;
        [SerializeField] private TextMeshProUGUI resultScore;
        [SerializeField] private TextMeshProUGUI resultScoreDescript;
        [SerializeField] private TextMeshProUGUI resultText;
        [SerializeField] private Color green;
        [SerializeField] private RectTransform resultWindow;
        [SerializeField] private GameObject counter;
        [SerializeField] private GameObject slider;
        [SerializeField] private float showWindowAnimDur;

        private const string BEST_SCORE = "best_score_save";
        private const string NEW_RECORD = "NEW RECORD!";
        private const string TRY_AGAIN  = "TRY AGAIN";
        private int _bestScore;
        private int screenWidth => 1920;
        private void Start()
        {
            _bestScore = PlayerPrefs.GetInt(BEST_SCORE);
            bestScore.text = _bestScore.ToString();
            StartCoroutine(RectLerper(new Vector2(0, 0.5f), new Vector2(screenWidth, 0.5f)));
            GameCore.Managers.EventManager.Subscribe<GameEvents.Battle.NewScoreResult>(SaveScore);
        }

        private void OnDestroy()
        {
            GameCore.Managers.EventManager.Unsubscribe<GameEvents.Battle.NewScoreResult>(SaveScore);
        }

        private IEnumerator RectLerper(Vector2 startPoint, Vector2 endPoint)
        {
            GameCore.Managers.EventManager.Trigger(new GameEvents.Menu.CancasMoveStart { });
            var timer = 0f;
            while (timer < lerpLasting)
            {
                var progress = lerpCurve.Evaluate(timer / lerpLasting);
                var value = Vector2.Lerp(startPoint, endPoint, progress);

                mainPanel.offsetMin = value;
                mainPanel.offsetMax = value;

                yield return null;
                timer += Time.deltaTime;
            }
            mainPanel.offsetMin = endPoint;
            mainPanel.offsetMax = endPoint;
            GameCore.Managers.EventManager.Trigger(new GameEvents.Menu.CancasMoveEnd { });
        }
        public void GoToMenuScene()
        {
            GameCore.Managers.PoolManager.StoreEverithing();
            GameCore.Managers.EventManager.Trigger(new GameEvents.Menu.GoToMenuSceneClicked { });
        }
        public void GoToBattleScene()
        {
            GameCore.Managers.PoolManager.StoreEverithing();
            GameCore.Managers.EventManager.Trigger(new GameEvents.Menu.GoToBattleSceneClicked { });
        }
        public void GoToSettings()
        {
            GameCore.Managers.EventManager.Trigger(new GameEvents.Battle.Pause { });
            StartCoroutine(RectLerper(new Vector2(0, 0.5f), new Vector2(-screenWidth, 0.5f)));
        }
        public void BackFromSettings()
        {
            GameCore.Managers.EventManager.Trigger(new GameEvents.Battle.Unpause { });
            StartCoroutine(RectLerper(new Vector2(-screenWidth, 0.5f), new Vector2(0, 0.5f)));
        }
        public void StartGame()
        {
            GameCore.Managers.EventManager.Trigger(new GameEvents.Battle.StartGame { });
            StartCoroutine(RectLerper(new Vector2(screenWidth, 0.5f), new Vector2(0, 0.5f)));
        }
        private void SaveScore(GameEvents.Battle.NewScoreResult e)
        {
            if(_bestScore < e.result)
            {
                PlayerPrefs.SetInt(BEST_SCORE, e.result);
                _bestScore = e.result;
                ShowResultWIndow(e.result, true);
            }
            else
            {
                ShowResultWIndow(e.result, false);
            }
        }
        private void ShowResultWIndow(int result, bool isRecord)
        {
            bestScore.text = _bestScore.ToString();
            bestScore2.text = _bestScore.ToString();
            resultScore.text = result.ToString();
            if (isRecord)
            {
                resultText.text = NEW_RECORD;
                resultScoreDescript.color = green;
            }
            else
            {
                resultText.text = TRY_AGAIN;
                bestScore2Descript.color = green;
            }
            resultWindow.transform.localScale = Vector3.zero;
            counter.gameObject.SetActive(false);
            slider.gameObject.SetActive(false);
            resultWindow.gameObject.SetActive(true);
            Sequence sequence = DOTween.Sequence();
            sequence.Append(resultWindow.DOScale(Vector3.one * 1.15f, showWindowAnimDur * 0.8f).SetEase(Ease.InCubic));
            sequence.Append(resultWindow.DOScale(Vector3.one, showWindowAnimDur * 0.2f).SetEase(Ease.OutCubic));
        }
    }
}