﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Mainspace
{
    public class EventManager : Singleton<EventManager>, IManager
    {
        public bool ManagerLoaded { get; private set; }
        private Dictionary<Type, IList> eventsDict;

        public void InitManager()
        {
            DontDestroyOnLoad(this);
            eventsDict = new Dictionary<Type, IList>(16);
            ManagerLoaded = true;
        }

        public void Subscribe<T>(Action<T> callback)
        {
            var type = typeof(T);
            if (!eventsDict.ContainsKey(type))
                eventsDict.Add(type, new List<Action<T>>(16));

            var list = eventsDict[type] as List<Action<T>>;
            var found = false;
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i] == null)
                {
                    list[i] = callback;
                    found = true;
                    break;
                }
            }

            if (!found)
                list.Add(callback);
        }
        public void Unsubscribe<T>(Action<T> callback)
        {
            var type = typeof(T);
            if (eventsDict.ContainsKey(type))
            {
                var list = eventsDict[type] as List<Action<T>>;
                var index = list.IndexOf(callback);
                if (index != -1)
                    list[index] = null;
            }
        }
        public void Trigger<T>(T e) where T : GameEvents.ICustomEvent
        {
            var type = typeof(T);
            if (eventsDict.ContainsKey(type))
            {
                var list = eventsDict[type] as List<Action<T>>;
                for (int i = 0; i < list.Count; i++)
                {
                    list[i]?.Invoke(e);
                }
            }
        }
        public void ClearEventsOf<T>()
        {
            var type = typeof(T);
            if (eventsDict.ContainsKey(type))
            {
                var list = eventsDict[type] as List<Action<T>>;
                list.Clear();
            }
        }
        public void ClearAllEvents()
        {
            foreach (var list in eventsDict)
            {
                list.Value.Clear();
            }

        }
        public void ClearAllEventsBut(params Type[] except)
        {
            foreach (var list in eventsDict)
            {
                var type = list.Key;
                var events = list.Value;

                if (!except.Contains(type))
                    events.Clear();
            }
        }
        public void ReloadManager() 
        {
            ClearAllEvents();
        }
        private void OnDisable()
        {
            ClearAllEvents();
        }
    }
}

namespace GameEvents
{
    public interface ICustomEvent { }

    namespace Menu
    {
        public struct CancasMoveStart : ICustomEvent { }
        public struct CancasMoveEnd : ICustomEvent { }
        public struct GoToBattleSceneClicked : ICustomEvent { }
        public struct GoToMenuSceneClicked : ICustomEvent { }
        public struct MusicSettingsChanged : ICustomEvent   { public bool value; }
        public struct SfxSettingsChanged : ICustomEvent     { public bool value; }

        public struct MenuSeceneLoaded : ICustomEvent { }
    }

    namespace Battle
    {
        public struct CoinCollected : ICustomEvent { public int price; }
        public struct PlayerDie : ICustomEvent { }
        public struct GreedHp0 : ICustomEvent { }
        public struct StartGame : ICustomEvent { }
        public struct Pause : ICustomEvent { }
        public struct Unpause : ICustomEvent { }
        public struct NewScoreResult : ICustomEvent { public int result; }
        public struct BattleSceneLoaded : ICustomEvent { }
    }
}