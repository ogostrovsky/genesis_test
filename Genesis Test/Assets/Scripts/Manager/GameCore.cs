﻿using System.Collections;
using UnityEngine;

namespace Mainspace
{
    public class GameCore : Singleton<GameCore>
    {
        private static bool gameCoreIsLoaded = false;

        private IManager[] managersArr;

        void Awake()
        {
            StartCoroutine(InitCore());
        }
        private IEnumerator InitCore()
        {
            DontDestroyOnLoad(gameObject);
            managersArr = new IManager[] {
                EventManager.Instance,
                ScenesManager.Instance,
                PoolManager.Instance,
                SoundsPlayerManager.Instance
            };
            yield return StartCoroutine(ManagersInit());
        }

        private IEnumerator ManagersInit()
        {
            for (int i = 0; i < managersArr.Length;)
            {
                managersArr[i].InitManager();
                if (managersArr[i].ManagerLoaded)
                {
                    i++;
                }
                else
                {
                    yield return null;
                }
            }

            gameCoreIsLoaded = true;
        }

        public static class Info
        {
            public static bool GameCoreIsLoaded => gameCoreIsLoaded;
        }

        public static class Managers
        {
            public static ScenesManager ScenesManager => ScenesManager.Instance;
            public static EventManager EventManager => EventManager.Instance;
            public static PoolManager PoolManager => PoolManager.Instance;
            public static MenuManager MenuManager => MenuManager.Instance;
            public static SoundsPlayerManager SoundsPlayerManager => SoundsPlayerManager.Instance;
        }
    }
}