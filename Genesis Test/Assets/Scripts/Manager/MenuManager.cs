using UnityEngine;
using DG.Tweening;
using System.Collections;

namespace Mainspace 
{
    public class MenuManager : Singleton<MenuManager>
    {
        [SerializeField] private RectTransform mainPanel;
        [SerializeField] private AnimationCurve lerpCurve;
        [SerializeField] private float lerpLasting;

        private int screenWidth => 1920;
        public void GoToSettings()
        {
            StartCoroutine(RectLerper(new Vector2(0, 0.5f), new Vector2(-screenWidth, 0.5f)));
        }
        public void BackToMainWindow()
        {
            StartCoroutine(RectLerper(new Vector2(-screenWidth, 0.5f), new Vector2(0, 0.5f)));
        }
        private IEnumerator RectLerper(Vector2 startPoint, Vector2 endPoint)
        {
            GameCore.Managers.EventManager.Trigger(new GameEvents.Menu.CancasMoveStart { });
            var timer = 0f;
            while (timer < lerpLasting)
            {
                var progress = lerpCurve.Evaluate(timer / lerpLasting);
                var value = Vector2.Lerp(startPoint, endPoint, progress);

                mainPanel.offsetMin = value;
                mainPanel.offsetMax = value;

                yield return null;
                timer += Time.deltaTime;
            }
            mainPanel.offsetMin = endPoint;
            mainPanel.offsetMax = endPoint;
            GameCore.Managers.EventManager.Trigger(new GameEvents.Menu.CancasMoveEnd { });
        }
        public void PlayClicked()
        {
            GameCore.Managers.SoundsPlayerManager.StopPlayingSound(SoundName.MenuMusic);
            GameCore.Managers.EventManager.Trigger(new GameEvents.Menu.GoToBattleSceneClicked { });
        }
        public void QuitTheGame()
        {
            Application.Quit();
        }
    }
}