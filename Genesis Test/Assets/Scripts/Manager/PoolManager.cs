﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using System.Linq;
using System;


namespace Mainspace
{
    public class PoolManager : Singleton<PoolManager>, IManager
    {
        private Dictionary<string, Queue<GameObject>> poolsDict;
        private List<GameObject> _outOfPoolObjects;
        public bool         ManagerLoaded { get; private set; }
        public void         InitManager()
        {
            DontDestroyOnLoad(this);
            poolsDict = new Dictionary<string, Queue<GameObject>>(16);
            _outOfPoolObjects = new List<GameObject>(128);
            ManagerLoaded = true;
        }
        public void         ReloadManager() { }
        public GameObject GetObj(GameObject obj, bool isEnabled = false)
        {
            var name = obj.name + "(Clone)";
            if (!poolsDict.ContainsKey(name))
                poolsDict.Add(name, new Queue<GameObject>(16));

            if (!poolsDict[name].Any())
            {
                var temp = Instantiate(obj);
                temp.transform.SetParent(transform);

                if (isEnabled)
                    temp.SetActive(true);
                else
                    temp.SetActive(false);

                _outOfPoolObjects.Add(temp);
                return temp;
            }
            else
            {
                var temp = poolsDict[name].Dequeue();
                temp.transform.SetParent(transform);

                if (isEnabled)
                    temp.SetActive(true);
                else
                    temp.SetActive(false);

                _outOfPoolObjects.Add(temp);
                return temp;
            }
        }
        public GameObject   GetObjectWithTimer(GameObject obj, float timer, bool isEnabled = false)
        {
            var newObj = GetObj(obj, isEnabled);
            DelayedStore(newObj, timer);
            return newObj;
        }
        public void         StoreObj(GameObject obj)
        {
            if (poolsDict[obj.name].Contains(obj))
            {
                Debug.Log($"Check {obj.name}, you trying to put back to pool this object twice!");
                return;
            }
            obj.SetActive(false);
            _outOfPoolObjects.Remove(obj);
            poolsDict[obj.name].Enqueue(obj);
        }
        public void         DelayedStore(GameObject obj, float timer)
        {
            StartCoroutine(DelayedExecuteRout(timer, () => StoreObj(obj)));
        }
        private IEnumerator DelayedExecuteRout(float timer, System.Action method)
        {
            yield return new WaitForSeconds(timer);
            method();
        }
        public void StoreEverithing()
        {
            for (int i = _outOfPoolObjects.Count - 1; i > -1 ; i--)
            {
                StoreObj(_outOfPoolObjects[i]);
            }
        }
    }
}