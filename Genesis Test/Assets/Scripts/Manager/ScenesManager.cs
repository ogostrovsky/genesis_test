using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System.IO;
using UnityEngine.Networking;
using Newtonsoft.Json;

namespace Mainspace 
{
    /// <summary>
    ///  Description  
    ///     - we have JsonInfo on google disk with wersion of latest bundles and url from where to load tham 
    ///     - if client version is lover than in loaded json 
    ///     - than we loading new bundle version
    /// </summary>
    public class ScenesManager : Singleton<ScenesManager>, IManager
    {
        [SerializeField] private Canvas canvas;
        [SerializeField] private Image Panel;
        [SerializeField] private Image LoadingImage;
        [Space]
        [SerializeField] private Sprite dinoImage;
        [SerializeField] private Sprite loadImage;
        [Space]
        [SerializeField] private float fadeInLasting;
        [SerializeField] private float fadeOutLasting;
        [SerializeField] private TextMeshProUGUI LoadingText;
        [Space]
        [SerializeField] private string json_BundleInfoURL;

        public bool ManagerLoaded { get; private set; }
        public bool PlayerIsInBattle { get; private set; }

        private static AssetBundle assetBundle;
        private bool loadingStart = false;
        private Tween _fadeIn;
        private WWW www;

        private string  _bundlesLoadedSceneName;
        private int     _loadedBundleVersion;
        private string  _loadedBundleURL;

        private const string SAVED_VERSION = "savedVersion";
        private const string SAVED_URL = "savedUrl";
        private int _savedBundleVersion;
        private string _savedBundleUrl;

        public void InitManager() //todo musock
        {
            DontDestroyOnLoad(this);
            StartCoroutine(LoadMenu());
            GameCore.Managers.EventManager.Subscribe<GameEvents.Menu.GoToBattleSceneClicked>(LoadBattle);
            GameCore.Managers.EventManager.Subscribe<GameEvents.Menu.GoToMenuSceneClicked>(LoadMenu);

            _savedBundleVersion = PlayerPrefs.GetInt(SAVED_VERSION);
            _savedBundleUrl = PlayerPrefs.GetString(SAVED_URL);

            ManagerLoaded = true; 
        }
        public void ReloadManager() { }
        public void LoadBattle(GameEvents.Menu.GoToBattleSceneClicked e)
        {
            StartCoroutine(LoadBattleRout());
        }
        public void LoadMenu(GameEvents.Menu.GoToMenuSceneClicked e)
        {
            StartCoroutine(LoadMenu());
        }
        private IEnumerator LoadMenu()
        {
            FadeIn();
            DisableImage(false);
            var sceneAwaiter = SceneManager.LoadSceneAsync(1);
            while (!sceneAwaiter.isDone)
            {
                yield return null;
                LoadingImage.fillAmount = sceneAwaiter.progress;
            }

            PlayerIsInBattle = false;
            GameCore.Managers.EventManager.Trigger(new GameEvents.Menu.MenuSeceneLoaded { });
            FadeOut();
        }
        private IEnumerator LoadBattleRout()
        {
            FadeIn();
            DisableImage(false);
            LoadingText.text = "Loading";
            LoadingImage.sprite = loadImage;

            //#if /*UNITY_EDITOR*/
            //var sceneAwaiter = SceneManager.LoadSceneAsync(2);  but dont forget to add the scene in build settings whan lounching in editor 
            //#elif UNITY_ANDROID
            yield return StartCoroutine(LoadBundle());
            var sceneAwaiter = SceneManager.LoadSceneAsync(_bundlesLoadedSceneName);
            //#endif

            while (!sceneAwaiter.isDone)
            {
                LoadingText.text = "Scene loading " + (int)sceneAwaiter.progress + "%";
                yield return null;
                LoadingImage.fillAmount = sceneAwaiter.progress;
            }

            PlayerIsInBattle = true;
            GameCore.Managers.EventManager.Trigger(new GameEvents.Battle.BattleSceneLoaded { });
            FadeOut();
        }
        private void FadeOut()
        {
            DisableImage(true);
            _fadeIn.Kill();
            _fadeIn = Panel.DOFade(0, fadeOutLasting).SetEase(Ease.InCubic);
        }
        private void FadeIn()
        {
            DisableImage(true);
            _fadeIn.Kill();
            Panel.color = new Color(Panel.color.r, Panel.color.g, Panel.color.b, 1);
        }
        private void DisableImage(bool val)
        {
            LoadingImage.enabled = !val;
            LoadingText.enabled = !val;
        }
        private void Update()
        {
            if (loadingStart)
            {
                double v = www.progress;
                LoadingImage.fillAmount = (float)v;
                v = System.Math.Round(v, 2);
                v *= 100;
                LoadingText.text = "Bundles loading " + (int)v + "%";
            }
        }
        private IEnumerator LoadBundle()
        {
            while(Application.internetReachability == NetworkReachability.NotReachable)
            {
                LoadingText.text = "No Internet Connection!";
                LoadingImage.sprite = dinoImage;
                LoadingImage.rectTransform.DOPunchPosition(LoadingImage.rectTransform.position.SwapCoord(0, 50, 0), 0.25f, 1, 1);
                yield return new WaitForSeconds(1);
            }
            LoadingImage.sprite = loadImage;
            LoadingImage.fillAmount = 0;

            yield return StartCoroutine(GetBundlesVersionInfo());

            if (_loadedBundleURL.Equals(string.Empty))
            {
                LoadingText.text = "Developers sucks";
                yield break;
            }

            if(_savedBundleVersion < _loadedBundleVersion)
            {
                PlayerPrefs.SetInt(SAVED_VERSION, _loadedBundleVersion);
                PlayerPrefs.SetString(SAVED_URL, _loadedBundleURL);
                _savedBundleVersion = _loadedBundleVersion;
                _savedBundleUrl     = _loadedBundleURL;
                Debug.Log("Bundles version updated to: " + _loadedBundleVersion);
            }

            if (!assetBundle)
            {
                using (www = new WWW(_savedBundleUrl))
                {
                    loadingStart = true;
                    yield return www;
                    if (!string.IsNullOrEmpty(www.error))
                    {
                        print(www.error);
                        LoadingText.text = "Developers sucks";
                        yield break;
                    }
                    assetBundle = www.assetBundle;
                }
            }

            loadingStart = false;
            string[] scenes = assetBundle.GetAllScenePaths();
            _bundlesLoadedSceneName = Path.GetFileNameWithoutExtension(scenes[0]);
        }
        private IEnumerator GetBundlesVersionInfo()
        {
            using (UnityWebRequest webRequest = UnityWebRequest.Get(json_BundleInfoURL))
            {
                yield return webRequest.SendWebRequest();

                string[] pages = json_BundleInfoURL.Split('/');
                int page = pages.Length - 1;

                switch (webRequest.result)
                {
                    case UnityWebRequest.Result.ConnectionError:
                    case UnityWebRequest.Result.DataProcessingError:
                        Debug.LogError(pages[page] + ": Error: " + webRequest.error);
                        break;
                    case UnityWebRequest.Result.ProtocolError:
                        Debug.LogError(pages[page] + ": HTTP Error: " + webRequest.error);
                        break;
                    case UnityWebRequest.Result.Success:

                        var str = webRequest.downloadHandler.text;
                        var bundleJson = (BundleInfo)JsonConvert.DeserializeObject(str, typeof(BundleInfo));

                        _loadedBundleVersion = bundleJson.BundleVersion;
                        _loadedBundleURL = bundleJson.URL;
                        break;
                }
            }
        }
    }
}