﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace Mainspace
{
    public class SoundsPlayerManager : Singleton<SoundsPlayerManager>, IManager
    {
        [SerializeField] private AudioInfo[] AudioClipsArr;
        [Space]
        [SerializeField] private GameObject audioSourcesHolder;

        private List<AudioSource> _audioSourcesList = new List<AudioSource>(8);
        private Dictionary<SoundName, AudioInfo> _soundsDict = new Dictionary<SoundName, AudioInfo>(8);

        public bool ManagerLoaded { get; private set; }
        public bool IsMusicOn { get; private set; }
        public bool IsSfxOn { get; private set; }
        public void InitManager() 
        {
            DontDestroyOnLoad(this);
            IsMusicOn = PlayerPrefs.GetInt("IsMusicOn") != 1 ? true : false;
            IsSfxOn = PlayerPrefs.GetInt("IsSfxOn") != 1 ? true : false;
            foreach (var info in AudioClipsArr)
            {
                _soundsDict.Add(info.soundName, info);
            }

            GameCore.Managers.EventManager.Subscribe<GameEvents.Menu.MusicSettingsChanged>(MusicSettingsChanged);
            GameCore.Managers.EventManager.Subscribe<GameEvents.Menu.SfxSettingsChanged>(SfxSettingsChanged);
            GameCore.Managers.EventManager.Subscribe<GameEvents.Menu.MenuSeceneLoaded>(OnMenuSceneChange);

            if (IsMusicOn)
            {
                PlayLoopingSound(SoundName.MenuMusic);
            }

            ManagerLoaded = true;
        }
        public void ReloadManager() { }
        public void PlaySound(SoundName name)
        {
            if(_soundsDict.TryGetValue(name, out var info))
            {
                if (info.AudioMixerGroup == SoundType.SFX && !IsSfxOn)
                    return;

                var source = GetSource();
                source.clip = info.audioClip;
                source.Play();
            }
        }
        public void StopPlayingSound(SoundName name)
        {
            if (_soundsDict.TryGetValue(name, out var info))
            {
                for (int i = 0; i < _audioSourcesList.Count; i++)
                {
                    if (_audioSourcesList[i].isPlaying && _audioSourcesList[i].clip.name == info.audioClip.name)
                    {
                        _audioSourcesList[i].Stop();
                    }
                }
            }
        }
        public void PlayLoopingSound(SoundName name)
        {
            if (_soundsDict.TryGetValue(name, out var info))
            {
                var source = GetSource();
                source.loop = true;
                source.clip = info.audioClip;
                source.Play();
            }
        }
        private void MusicSettingsChanged(GameEvents.Menu.MusicSettingsChanged e)
        {
            IsMusicOn = e.value;
            PlayerPrefs.SetInt("IsMusicOn", IsMusicOn ? 2 : 1);
            if (GameCore.Managers.ScenesManager.PlayerIsInBattle)
            {
                TryPlayBattleMus();
            }
            else
            {
                TryPlayMenuMus();
            }
        }
        private void SfxSettingsChanged(GameEvents.Menu.SfxSettingsChanged e)
        {
            IsSfxOn = e.value;
            PlayerPrefs.SetInt("IsSfxOn", IsSfxOn ? 2 : 1);
        }
        public void TryPlayMenuMus()
        {
            StopPlayingSound(SoundName.MenuMusic);
            if (IsMusicOn && !GameCore.Managers.ScenesManager.PlayerIsInBattle)
            {
                StopPlayingSound(SoundName.BattleMusick);
                PlayLoopingSound(SoundName.MenuMusic);
            }
        }
        public void TryPlayBattleMus()
        {
            StopPlayingSound(SoundName.MenuMusic);
            if (IsMusicOn && GameCore.Managers.ScenesManager.PlayerIsInBattle)
            {
                StopPlayingSound(SoundName.BattleMusick);
                PlayLoopingSound(SoundName.BattleMusick);
            }
        }
        private void OnMenuSceneChange(GameEvents.Menu.MenuSeceneLoaded e)
        {
            TryPlayMenuMus();
        }


        private AudioSource GetSource()
        {
            for (int i = 0; i < _audioSourcesList.Count; i++)
            {
                if (!_audioSourcesList[i].isPlaying)
                {
                    _audioSourcesList[i].loop = false;
                    return _audioSourcesList[i];
                }
            }
            var newSource = audioSourcesHolder.AddComponent<AudioSource>();
            newSource.playOnAwake = false;
            newSource.loop = false;
            _audioSourcesList.Add(newSource);
            return newSource;
        }

        [Serializable]
        private struct AudioInfo
        {
            public SoundName soundName;
            public SoundType AudioMixerGroup;
            public AudioClip audioClip;
        }
    }

    public enum SoundName
    {
        ButtunClick = 0,
        MenuMusic = 1,
        BattleMusick = 2,
        CoinColected = 3,
        Hello = 4,
        CoinFall = 5,
        CoinMoreCollect = 6,
        Die = 7
    }

    public enum SoundType
    {
        Music = 0,
        SFX   = 1
    }
}
 