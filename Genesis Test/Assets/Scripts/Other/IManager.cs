﻿namespace Mainspace
{
    public interface IManager
    {
        bool ManagerLoaded { get; }
        void ReloadManager();
        void InitManager();
    }
}
