using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowRotator : MonoBehaviour
{
    //[SerializeField] private Transform player;
    private Transform trans;
    private void Start()
    {
        trans = transform;
    }
    void Update()
    {
        trans.rotation = Quaternion.LookRotation(Vector3.forward);
    }
}
