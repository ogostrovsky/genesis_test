﻿using UnityEngine;
namespace Mainspace
{
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;
        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = (T)FindObjectOfType(typeof(T));
                    if (_instance == null)
                    {
                        throw new System.Exception($"No {typeof(T)} on the scene");
                    }
                }
                return _instance;
            }
        }
        protected void KillThis()
        {
            _instance = null;
        }
    }
}