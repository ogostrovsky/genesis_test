using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mainspace
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private ParticleSystem dieEffect;
        [SerializeField] private ParticleSystem collectEffect;
        [SerializeField] private GameObject playerBody;
        [Space]
        [SerializeField] private GameObject[] hats;
        [SerializeField] private Color[] HatColors;
        [Space]
        [SerializeField] private float moveSpeed;
        [SerializeField] private int rotationSlowDown = 1;
        [Space]
        [SerializeField] private Animator anim;
        [SerializeField] private float rotationLasting;
        [SerializeField] private float helloFrequency;

        private Vector3 FirstPoint;
        private Vector3 SecondPoint;
        private float yAngle;
        private float yAngleTemp;

        private Transform playerTrans;
        private Coroutine _runRout;
        private Coroutine _helloRout;

        public static bool PlayerIsAlive;

        private void Start()
        {
            PlayerIsAlive = true;
            SetRandoHat();
            playerTrans = transform;
            _helloRout = StartCoroutine(SayHello());
            GameCore.Managers.EventManager.Subscribe<GameEvents.Battle.StartGame>(StartRun);
            GameCore.Managers.EventManager.Subscribe<GameEvents.Battle.Pause>(StopRun);
            GameCore.Managers.EventManager.Subscribe<GameEvents.Battle.Unpause>(ContinueRun);
            GameCore.Managers.EventManager.Subscribe<GameEvents.Battle.GreedHp0>(GreedDeath);
        }

        private void OnDestroy()
        {
            GameCore.Managers.EventManager.Unsubscribe<GameEvents.Battle.StartGame>(StartRun);
            GameCore.Managers.EventManager.Unsubscribe<GameEvents.Battle.Pause>(StopRun);
            GameCore.Managers.EventManager.Unsubscribe<GameEvents.Battle.Unpause>(ContinueRun);
            GameCore.Managers.EventManager.Unsubscribe<GameEvents.Battle.GreedHp0>(GreedDeath);
        }
        private void Update()
        {
#if !UNITY_EDITOR
        DeviceInput();
#else
            UnityInput();
#endif
        }
        private void DeviceInput()
        {
            if (Input.touchCount > 0)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    FirstPoint = Input.GetTouch(0).position;
                    yAngleTemp = yAngle;
                }
                if (Input.GetTouch(0).phase == TouchPhase.Moved)
                {
                    SecondPoint = Input.GetTouch(0).position;
                    yAngle = (yAngleTemp + (SecondPoint.x - FirstPoint.x) / rotationSlowDown);
                    playerTrans.rotation = Quaternion.Euler(0, yAngle, 0);
                }
            }
        }
        private void UnityInput()
        {
            if (Input.GetMouseButtonDown(0))
            {
                FirstPoint = Input.mousePosition;
            }
            if (Input.GetMouseButton(0))
            {
                SecondPoint = Input.mousePosition;
                yAngle = yAngle + (SecondPoint.x - FirstPoint.x) / (rotationSlowDown * 5f);
                playerTrans.rotation = Quaternion.Euler(0, yAngle, 0);
            }
            FirstPoint = Input.mousePosition;
        }
        private void SetRandoHat()
        {
            var hat = hats[Random.Range(0, hats.Length)];
            hat.SetActive(true);
            hat.GetComponent<MeshRenderer>().material.color = HatColors[Random.Range(0, HatColors.Length)];
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == 8)
            {
                PlayerDie();
            }
            else
            {
                collectEffect.Play();
            }
        }
        private void PlayerDie()
        {
            PlayerIsAlive = false;
            playerBody.SetActive(false);
            dieEffect.Play();
            StopCoroutine(_helloRout);
            StopRun(new GameEvents.Battle.Pause { });
            GameCore.Managers.EventManager.Trigger(new GameEvents.Battle.PlayerDie { });
            GameCore.Managers.SoundsPlayerManager.PlaySound(SoundName.Die);
        }
        private void GreedDeath(GameEvents.Battle.GreedHp0 e)
        {
            PlayerDie();
        }
        private void StartRun(GameEvents.Battle.StartGame e)
        {
            _runRout = StartCoroutine(RunRout());
        }
        private void StopRun(GameEvents.Battle.Pause e)
        {
            if (_runRout != null)
            {
                StopCoroutine(_runRout);
                _runRout = null;
            }

            anim.SetBool("Run", false);
            StartCoroutine(Rotator());
        } 
        private void ContinueRun(GameEvents.Battle.Unpause e)
        {
            if (PlayerIsAlive)
            {
                _runRout = StartCoroutine(RunRout());
            }
        }
        private IEnumerator RunRout()
        {
            anim.SetBool("Run", true);
            yield return StartCoroutine(Rotator());
            while (true)
            {
                playerTrans.position += playerTrans.forward * (moveSpeed + GreedBeh.GreedLVL) * Time.deltaTime;
                yield return null;
            }
        }
        private IEnumerator SayHello()
        {
            while (true)
            {
                yield return new WaitForSeconds(helloFrequency * Random.Range(0.5f, 1.5f));
                if (!anim.GetBool("Run"))
                {
                    anim.SetTrigger("Hello");
                    GameCore.Managers.SoundsPlayerManager.PlaySound(SoundName.Hello);
                }
            }
        }
        private IEnumerator Rotator()
        {
            var start = Quaternion.LookRotation(playerBody.transform.forward);
            var end = Quaternion.LookRotation(-playerBody.transform.forward);

            var bodyTrans = playerBody.transform;
            var timer = 0f;
            while (timer < rotationLasting)
            {
                var progress = timer / rotationLasting;
                bodyTrans.rotation = Quaternion.Lerp(start, end, progress);
                yield return null;
                timer += Time.deltaTime;
            }
            bodyTrans.rotation = end;
        }
    }
}