﻿Shader "Unlit/SimpleGradientToonColorShader"
{
    Properties
    {
        [NoScaleOffset]
        _MainTex ("Texture", 2D) = "white" {}
        [NoScaleOffset]
        _Grad ("Gradient Texture", 2D) = "white" {}
        _Shadow ("Shadow strngth", Range(0,1)) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" 
             "LightMode" = "ForwardBase"}
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            //#pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float3 dot : COLOR0;
                //UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _Grad;
            half _Shadow;
            //float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.dot = clamp((dot(mul(unity_ObjectToWorld,v.normal),_WorldSpaceLightPos0) + 1) * 0.5, 0.01,0.99);
                //o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.uv = v.uv;
                //UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                fixed4 grad = tex2D(_Grad, half2(i.dot,0));
                //fixed4 grad = tex2D(_Grad, i.dot);
                //col.rgb *= ;

                //col *= i.dot;
                // apply fog
                //UNITY_APPLY_FOG(i.fogCoord, col);
                fixed4 fin = col * lerp(grad.r,1,_Shadow);
                return fin;
            }
            ENDCG
        }
    }
}
