﻿Shader "Custom/Toon/LitColorVirus"
{
    Properties
    {	
        _MainTex("Textura", 2D) = "white"{}
        _Cascades("Cascades", Float) = 5
		_Brightness("Brightness", Float) = 0
    }
    SubShader
    {
        
        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite Off
        
        

        Tags
		{ 
			"RenderType" = "Opaque"
			"LightMode" = "ForwardBase"
		}
        ZWrite On
        Cull Back
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fwdbase
			#pragma fragmentoption ARB_fog_exp2
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "AutoLight.cginc"
            #include "UnityCG.cginc"
			#include "Lighting.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
				float3 normal : NORMAL;
                float4 vcol : COLOR0;

            };

            struct v2f
            {
                //float4 vcol : COLRO0;
                float4 vcol : COLOR0;
				float3 worldNormal : NORMAL;
                float4 vertex : SV_POSITION;
                float3 lightDir : TEXCOORD1;
                LIGHTING_COORDS(2,3)
            };


            float4 _MainTex;
			float  _Cascades;
			float  _Brightness;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.worldNormal = normalize(mul((float3x3)UNITY_MATRIX_M, v.normal));
                o.vcol = v.vcol;
                //o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.lightDir = ObjSpaceLightDir( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o);
                return o;
            }

            float4 frag (v2f i) : SV_Target
            {
                float atten = LIGHT_ATTENUATION(i);
				float NdotL = dot(i.worldNormal, _WorldSpaceLightPos0);
				float light = saturate(floor(NdotL * _Cascades) / _Cascades) * _LightColor0;
                float3 viewDir = UNITY_MATRIX_IT_MV[2].xyz;
				float NdotC = dot(i.worldNormal, viewDir);

                float4 col = i.vcol;
                col.rgb *= (light + unity_AmbientSky);
                float shadow = dot(atten, _WorldSpaceLightPos0);
                col.rgb *= (.1 - shadow) + .9;
                col.rgb *= _Brightness;
                col.a = i.vcol.a;
                return col;
            }
            ENDCG
        }
    }

    Fallback "VertexLit"

}
