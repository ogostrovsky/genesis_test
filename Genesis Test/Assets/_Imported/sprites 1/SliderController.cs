﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//namespace Jam.UI {
//    public class SliderController : MonoBehaviour {
//        [SerializeField] private Slider slider;

//        private void Start() {
//            InitManagers();
//            StartCoroutine(SliderFakeMoving(LoadNextScene));
//        }


//        private IEnumerator SliderFakeMoving(System.Action onFinish) {
//            yield return Core.yields.WhaitFor(.3f);
//            slider.value = .1f;
//            for (int i = 0; i < 40; i++) {
//                slider.value += .01f;
//                yield return null;
//            }
//            yield return Core.yields.WhaitFor(.5f);
//            for (int i = 0; i < 20; i++) {
//                slider.value += .01f;
//                yield return null;
//            }
//            yield return Core.yields.WhaitFor(.5f);
//            for (int i = 0; i < 10; i++) {
//                slider.value += .02f;
//                yield return null;
//            }
//            slider.value = 1f;
//            yield return Core.yields.WhaitFor(.5f);
//            onFinish?.Invoke();
//        }

//        public void LoadNextScene() {
//            Core.scenes.LoadCurrentLevel(true);
//            Core.windows.OpenWindow<Windows.StartWindow>();
//        }
//    }
//}