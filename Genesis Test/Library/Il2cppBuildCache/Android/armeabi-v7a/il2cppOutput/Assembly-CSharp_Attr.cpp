﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCoinsDropperU3Ed__16_tB255480E26513BDB1C6E8741FD8348B2C44B83EE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDelayedDieU3Ed__22_t13921E7880F014290B349242E2F41F1A80227638_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDelayedExecuteRoutU3Ed__12_tB4B9F1609DA131FA9762A04E9878ED5B781C2144_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDifficultyApperU3Ed__24_t904C0FB3DB6C190E3DB3FCE00435363F1E101970_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFallU3Ed__18_t165EC7F06440CB52F1578139076D6BA440E3C330_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetBundlesVersionInfoU3Ed__35_t723284E5DB9205EE817B2FFA5A0711AC94FD84B0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGreedControllerU3Ed__23_tD199267145EA542A06DB4BE74C527EB42673B3D8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInitCoreU3Ed__3_tDFD7C3A276A8679CE027A37454FC646A663895B5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLerperNumberU3Ed__11_t28CF8F99BC897162AE9B411D48413A6B85662D50_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadBattleRoutU3Ed__29_tF474E0A73CD058CD0A109CD9822D8B23B25D88BC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadBundleU3Ed__34_t5883FB0ED07D57BAA2813F454550FAEFB16158C4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadMenuU3Ed__28_tB58C1122E8D0ED148523461E8CD31F85CDD1D0D4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CManagersInitU3Ed__4_tFA7811EED864BC0384726559E3D01C81BFC920AA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CPropsSpawnerU3Ed__17_tFF2684BDB6250DFA569D66790A7747F407DE4815_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRectLerperU3Ed__22_t1529E126C3ABC63AAA4C8A3C18CB37944708E770_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRectLerperU3Ed__7_t627D2B2D7285C0085962BF78194CD59D57F049EA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRotatorU3Ed__17_t51FCCC2D459A39620C3DC1DF763DAB8F5A256B6A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRotatorU3Ed__32_t42629F9D4CE344E0CEE7196E0647CB2BDC4369A5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRunRoutU3Ed__30_tD0905ED021E61471FA6F363886A33A1AC7330457_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSayHelloU3Ed__31_tE0422E39EEC3F88FBDEE3F3891BA2FA0933F935E_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.SpaceAttribute::height
	float ___height_0;

public:
	inline static int32_t get_offset_of_height_0() { return static_cast<int32_t>(offsetof(SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8, ___height_0)); }
	inline float get_height_0() const { return ___height_0; }
	inline float* get_address_of_height_0() { return &___height_0; }
	inline void set_height_0(float value)
	{
		___height_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SpaceAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * __this, const RuntimeMethod* method);
// System.Void System.ParamArrayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719 (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[3];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void ColorExtensions_t72892A69207663404B47720DD90667E8A6A4A22F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ColorExtensions_t72892A69207663404B47720DD90667E8A6A4A22F_CustomAttributesCacheGenerator_ColorExtensions_ChangeAlpha_mC3CC7B4AE0E805D7101B9A31F686BA42DB3AF9FC(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void CoroutineExtensions_t2E926427EDAB1AE4124D7B4C7ECA80C2C62E3336_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void CoroutineExtensions_t2E926427EDAB1AE4124D7B4C7ECA80C2C62E3336_CustomAttributesCacheGenerator_CoroutineExtensions_End_m517E0787B1E601B3B2E985FA47A1690FB9C57202(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DateTimeExtensions_t6A5D20B8382B0B207849FC576D23AA2B9E8D228A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DateTimeExtensions_t6A5D20B8382B0B207849FC576D23AA2B9E8D228A_CustomAttributesCacheGenerator_DateTimeExtensions_TimeStamp_mC3066A49B5AF458185559BBC314A323D68923E34(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DictionaryExtensions_t8809E869D7D42CA740EA25A4F8EB9280D1C0A94D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DictionaryExtensions_t8809E869D7D42CA740EA25A4F8EB9280D1C0A94D_CustomAttributesCacheGenerator_DictionaryExtensions_exKeysList_m15799EFECB4BB73F219417A85E43085B18A88794(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DictionaryExtensions_t8809E869D7D42CA740EA25A4F8EB9280D1C0A94D_CustomAttributesCacheGenerator_DictionaryExtensions_exValsList_m6DE4210697AA6ECFCCBDC8CAB8C52C46AA32ABA1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DictionaryExtensions_t8809E869D7D42CA740EA25A4F8EB9280D1C0A94D_CustomAttributesCacheGenerator_DictionaryExtensions_exFirstPair_m94016A9AB7A82AD9D26EB70F0B24EC28802C4A42(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DictionaryExtensions_t8809E869D7D42CA740EA25A4F8EB9280D1C0A94D_CustomAttributesCacheGenerator_DictionaryExtensions_exFirstKey_mFEAC204CBF5794658954941F95FB92CA6FC5A5AA(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DictionaryExtensions_t8809E869D7D42CA740EA25A4F8EB9280D1C0A94D_CustomAttributesCacheGenerator_DictionaryExtensions_exFirstValue_m60711462BAAB56DE869364146093251B6943CC07(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DictionaryExtensions_t8809E869D7D42CA740EA25A4F8EB9280D1C0A94D_CustomAttributesCacheGenerator_DictionaryExtensions_exRandomItem_mA929CF2A15196D188AE6E43DC81EE125017C0445(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DictionaryExtensions_t8809E869D7D42CA740EA25A4F8EB9280D1C0A94D_CustomAttributesCacheGenerator_DictionaryExtensions_exRandomValue_m5DA4982DCD1ACAD359E8801F391C5406892D36D9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void DictionaryExtensions_t8809E869D7D42CA740EA25A4F8EB9280D1C0A94D_CustomAttributesCacheGenerator_DictionaryExtensions_exRandomKey_m31739BD3C89EBB1FD87D227EBC5F97092EC68F21(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void FloatExtensions_tC1D69ACC51899B26B456B8D4B9EBD59D1FDB69D6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void FloatExtensions_tC1D69ACC51899B26B456B8D4B9EBD59D1FDB69D6_CustomAttributesCacheGenerator_FloatExtensions_Sign_m4FCF92814F87B927385F0B7C06A5253CBE787BFF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void FloatExtensions_tC1D69ACC51899B26B456B8D4B9EBD59D1FDB69D6_CustomAttributesCacheGenerator_FloatExtensions_IsPositive_m56E710C581B36D7E5BB1C87D5B93B554B6E144B9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void FloatExtensions_tC1D69ACC51899B26B456B8D4B9EBD59D1FDB69D6_CustomAttributesCacheGenerator_FloatExtensions_ToTimeString_m7476A4FFB42E9C3E1CC9FBB461948769AFA00897(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GameObjectExtensions_t9CAB6FA68B5E68D39CC079EF641BB6D922DE6F22_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void GameObjectExtensions_t9CAB6FA68B5E68D39CC079EF641BB6D922DE6F22_CustomAttributesCacheGenerator_GameObjectExtensions_DestroyIfSingletonExist_m2057689DFF6902B1D0E38402274E19371EDA74CF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void IntExtensions_tA94D6C4CF640582935395B9F371352AEDB8F1318_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void IntExtensions_tA94D6C4CF640582935395B9F371352AEDB8F1318_CustomAttributesCacheGenerator_IntExtensions_Sign_mEB374FC99F30A2BC1ACD044FD33B8DC406F9175A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void IntExtensions_tA94D6C4CF640582935395B9F371352AEDB8F1318_CustomAttributesCacheGenerator_IntExtensions_IsPositive_mC99FA2A59F97A83B5636EE374566A000C3954F8B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ListExtensions_t1AE27B309243C7540DD8185472EC958FC2953B29_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ListExtensions_t1AE27B309243C7540DD8185472EC958FC2953B29_CustomAttributesCacheGenerator_ListExtensions_GetRndElement_m36ADAF78794F42C4A234EA803EA85E5A29FEBC63(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ListExtensions_t1AE27B309243C7540DD8185472EC958FC2953B29_CustomAttributesCacheGenerator_ListExtensions_GetRandomItemAndRemove_m3006365AEA77521A687383AF79771696D880134A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ListExtensions_t1AE27B309243C7540DD8185472EC958FC2953B29_CustomAttributesCacheGenerator_ListExtensions_exDistinct_m6480814E7853C469D672251687AA5FA39669C219(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ParticleSystemExtensions_t06BEB0CB36800B29681FCEB5B231786B1531E43B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ParticleSystemExtensions_t06BEB0CB36800B29681FCEB5B231786B1531E43B_CustomAttributesCacheGenerator_ParticleSystemExtensions_exFullStop_m611C3ADD0DD46034D10439A26365D53CF195FE0C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ParticleSystemExtensions_t06BEB0CB36800B29681FCEB5B231786B1531E43B_CustomAttributesCacheGenerator_ParticleSystemExtensions_exFullReset_m8CC35A22CD32BF1AC5FCA09D6D9990C2733E3035(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ParticleSystemExtensions_t06BEB0CB36800B29681FCEB5B231786B1531E43B_CustomAttributesCacheGenerator_ParticleSystemExtensions_exFullClear_mC09E34569DC27BABFB2EE01E34A4E5D9761570F8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ParticleSystemExtensions_t06BEB0CB36800B29681FCEB5B231786B1531E43B_CustomAttributesCacheGenerator_ParticleSystemExtensions_exSetColor_m1711396E1D9B438D40258E2C02801EB5FE7DA7F9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ParticleSystemExtensions_t06BEB0CB36800B29681FCEB5B231786B1531E43B_CustomAttributesCacheGenerator_ParticleSystemExtensions_ResetAndPlay_m5E0F57B158F49C0DFFFDC6A09AA42723B675718B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ParticleSystemExtensions_t06BEB0CB36800B29681FCEB5B231786B1531E43B_CustomAttributesCacheGenerator_ParticleSystemExtensions_exFullClear_m8DE597433DED9D59E7AB1FA06B1BDA0371C2D71D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SpriteExtensions_t51D56CB1D5836CADD7C7317E6630B570AB79C44A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void SpriteExtensions_t51D56CB1D5836CADD7C7317E6630B570AB79C44A_CustomAttributesCacheGenerator_SpriteExtensions_getUvOffset_mDCB6E2B56CB261598C275D2EEBF5A3CCB56712F3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_tC367E6F67B4C112531BF21341C582782806E4F69_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_tC367E6F67B4C112531BF21341C582782806E4F69_CustomAttributesCacheGenerator_StringExtensions_Reverse_m921801DC6F50A3C8D3A5C50E2BD3E56A964D6806(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TimeExtensions_t86985F861458B7EC3F5B38A4810F388D5380D1BE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TimeExtensions_t86985F861458B7EC3F5B38A4810F388D5380D1BE_CustomAttributesCacheGenerator_TimeExtensions_ToShortForm_m495F5B791C0C842B7C045D19BD4A28BA256F17DF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TimeExtensions_t86985F861458B7EC3F5B38A4810F388D5380D1BE_CustomAttributesCacheGenerator_TimeExtensions_ToMegaShortForm_m0552A35DB7EF0ECDFE80FC12082116AAC9B10180(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TransformExtensions_t0663C8668959986C21AECDDB309DBF244F5B6014_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TransformExtensions_t0663C8668959986C21AECDDB309DBF244F5B6014_CustomAttributesCacheGenerator_TransformExtensions_SelfDestroy_m37BF9588254B27591768734A57AE42637B6C2061(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TransformExtensions_t0663C8668959986C21AECDDB309DBF244F5B6014_CustomAttributesCacheGenerator_TransformExtensions_DestroyChildren_mA936B8276E3B8037E2A0564903923F0039ECD9FE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TransformExtensions_t0663C8668959986C21AECDDB309DBF244F5B6014_CustomAttributesCacheGenerator_TransformExtensions_DestroyChildrenNow_m1B49213F608A5FF49CF742D4B2C1FD8C3D8AAD6B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TransformExtensions_t0663C8668959986C21AECDDB309DBF244F5B6014_CustomAttributesCacheGenerator_TransformExtensions_exContains_m86563D73F6C59BD08C28DD3D0740AE331E1F8EC6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TransformExtensions_t0663C8668959986C21AECDDB309DBF244F5B6014_CustomAttributesCacheGenerator_TransformExtensions_exContains_m35F876DB602247DCAE89E2230E0FAB83919BF0A4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TransformExtensions_t0663C8668959986C21AECDDB309DBF244F5B6014_CustomAttributesCacheGenerator_TransformExtensions_exIndex_m29F4E08C5007EDD88E1E04FF545BDA782FBAEAFF(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void VectorExtensions_t317DEEAE473359EDB7F5D249CF83CF8BBD9F9802_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void VectorExtensions_t317DEEAE473359EDB7F5D249CF83CF8BBD9F9802_CustomAttributesCacheGenerator_VectorExtensions_ToV3_mEEC4EE674FCE50FF782B34554FAD934BA6A25089(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void VectorExtensions_t317DEEAE473359EDB7F5D249CF83CF8BBD9F9802_CustomAttributesCacheGenerator_VectorExtensions_ToV3_m96F471D1DAD72302AD1F22EBF54B52AD1213744F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void VectorExtensions_t317DEEAE473359EDB7F5D249CF83CF8BBD9F9802_CustomAttributesCacheGenerator_VectorExtensions_Abs_m0C727BD6192E0F080F46BF5E3C17551347FBFF8F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void VectorExtensions_t317DEEAE473359EDB7F5D249CF83CF8BBD9F9802_CustomAttributesCacheGenerator_VectorExtensions_Div_mA05A365EC50132940174346D7B03E0B09352CCB1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void VectorExtensions_t317DEEAE473359EDB7F5D249CF83CF8BBD9F9802_CustomAttributesCacheGenerator_VectorExtensions_Mirror_m1C4157865EAACC117CE8503CFC6F1F4B95DF3C4E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void VectorExtensions_t317DEEAE473359EDB7F5D249CF83CF8BBD9F9802_CustomAttributesCacheGenerator_VectorExtensions_SwapCoord_mB1C58FE21EC7D3F0E297309CAD6BAA5CF5C32FE8(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void VectorExtensions_t317DEEAE473359EDB7F5D249CF83CF8BBD9F9802_CustomAttributesCacheGenerator_VectorExtensions_SwapCoord_m8D37CE7CA45E4AC7833C7FE6B98E1772CD1AD58F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void VectorExtensions_t317DEEAE473359EDB7F5D249CF83CF8BBD9F9802_CustomAttributesCacheGenerator_VectorExtensions_AddCoord_m03E0EF7A6F887BBE97A986B702C88256FC8FE184(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void VectorExtensions_t317DEEAE473359EDB7F5D249CF83CF8BBD9F9802_CustomAttributesCacheGenerator_VectorExtensions_Zero_m9FA2283836BE1DD565A1A3441E018E34F041D1FB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void VectorExtensions_t317DEEAE473359EDB7F5D249CF83CF8BBD9F9802_CustomAttributesCacheGenerator_VectorExtensions_RndShft_m650D9A88BA0DC7D81A2846828B78B4B4741845A9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void VectorExtensions_t317DEEAE473359EDB7F5D249CF83CF8BBD9F9802_CustomAttributesCacheGenerator_VectorExtensions_RndAbsShift_m54D3E249B07A759147A68FE962C1FF2F8D95C6AE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ETFXProjectileScript_tED63BF9EB347489D6D56FF6E6265BA4EC7457B03_CustomAttributesCacheGenerator_colliderRadius(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x64\x6A\x75\x73\x74\x20\x69\x66\x20\x6E\x6F\x74\x20\x75\x73\x69\x6E\x67\x20\x53\x70\x68\x65\x72\x65\x20\x43\x6F\x6C\x6C\x69\x64\x65\x72"), NULL);
	}
}
static void ETFXProjectileScript_tED63BF9EB347489D6D56FF6E6265BA4EC7457B03_CustomAttributesCacheGenerator_collideOffset(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void FPSWalkerEnhanced_t54D7B7F25F9BE895544D79E770EDD93B466DB0AB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(CharacterController_tCCF68621C784CCB3391E0C66FE134F6F93DD6C2E_0_0_0_var), NULL);
	}
}
static void SmoothMouseLook_tEE1CC92D0DBE9D333976C6B9BBC6EEB6AB8AC7DA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6D\x65\x72\x61\x2D\x43\x6F\x6E\x74\x72\x6F\x6C\x2F\x53\x6D\x6F\x6F\x74\x68\x20\x4D\x6F\x75\x73\x65\x20\x4C\x6F\x6F\x6B"), NULL);
	}
}
static void ETFXFireProjectile_t13466BBD977B94CACB65A92CE5452EF030A99328_CustomAttributesCacheGenerator_projectiles(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ETFXFireProjectile_t13466BBD977B94CACB65A92CE5452EF030A99328_CustomAttributesCacheGenerator_spawnPosition(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x73\x73\x69\x6C\x65\x20\x73\x70\x61\x77\x6E\x73\x20\x61\x74\x20\x61\x74\x74\x61\x63\x68\x65\x64\x20\x67\x61\x6D\x65\x20\x6F\x62\x6A\x65\x63\x74"), NULL);
	}
}
static void ETFXFireProjectile_t13466BBD977B94CACB65A92CE5452EF030A99328_CustomAttributesCacheGenerator_currentProjectile(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ETFXLoopScript_tF4DFDBD5AF30EE9D60CDB5D8E64685454AD7CA75_CustomAttributesCacheGenerator_spawnWithoutLight(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x61\x77\x6E\x20\x77\x69\x74\x68\x6F\x75\x74"), NULL);
	}
}
static void ETFXLoopScript_tF4DFDBD5AF30EE9D60CDB5D8E64685454AD7CA75_CustomAttributesCacheGenerator_ETFXLoopScript_EffectLoop_m728B76DFD989B681D2C93D7A88E9D76C9696CAF4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_0_0_0_var), NULL);
	}
}
static void U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_CustomAttributesCacheGenerator_U3CEffectLoopU3Ed__6__ctor_m553F1C586BBB6C68EEB9710F62438A8EDCE136DA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_CustomAttributesCacheGenerator_U3CEffectLoopU3Ed__6_System_IDisposable_Dispose_m8142A69BF60253C0139648E9350176EB9EFD8BFA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_CustomAttributesCacheGenerator_U3CEffectLoopU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m93EE11485A4BA42D80F240C1703D673774889545(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_CustomAttributesCacheGenerator_U3CEffectLoopU3Ed__6_System_Collections_IEnumerator_Reset_mB35745D308CDED6CAB69D52DEFC2D84AB6CC5896(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_CustomAttributesCacheGenerator_U3CEffectLoopU3Ed__6_System_Collections_IEnumerator_get_Current_mB56FD34E205892C6B85E8FD18DAE68F19ECBDE2E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ETFXTarget_tF0B3C5AAE0244F70D1701A4B14AED2D0A8F4B02F_CustomAttributesCacheGenerator_hitParticle(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x66\x66\x65\x63\x74\x20\x73\x68\x6F\x77\x6E\x20\x6F\x6E\x20\x74\x61\x72\x67\x65\x74\x20\x68\x69\x74"), NULL);
	}
}
static void ETFXTarget_tF0B3C5AAE0244F70D1701A4B14AED2D0A8F4B02F_CustomAttributesCacheGenerator_respawnParticle(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x66\x66\x65\x63\x74\x20\x73\x68\x6F\x77\x6E\x20\x6F\x6E\x20\x74\x61\x72\x67\x65\x74\x20\x72\x65\x73\x70\x61\x77\x6E"), NULL);
	}
}
static void ETFXTarget_tF0B3C5AAE0244F70D1701A4B14AED2D0A8F4B02F_CustomAttributesCacheGenerator_ETFXTarget_Respawn_m679D0B0188B3DA281D2BE313E7212FC1D6F2B905(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_0_0_0_var), NULL);
	}
}
static void U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_CustomAttributesCacheGenerator_U3CRespawnU3Ed__7__ctor_m415513F665DE8F3777A3B21AF9C5B45A90CB1BF3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_CustomAttributesCacheGenerator_U3CRespawnU3Ed__7_System_IDisposable_Dispose_m63C5D414DB8D33EB86AFF6D5569B6C4E18C32615(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_CustomAttributesCacheGenerator_U3CRespawnU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF70DB558C29B7FFE4A790553093711C3478A2C54(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_CustomAttributesCacheGenerator_U3CRespawnU3Ed__7_System_Collections_IEnumerator_Reset_m37DFF9115E528EA7997B687DD51321AB843D9BE7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_CustomAttributesCacheGenerator_U3CRespawnU3Ed__7_System_Collections_IEnumerator_get_Current_mA84FD67FBB5DE784889C67146E66570FC2E5D80E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ETFXLightFade_t2C9509BEFEF0040FB2595BFE837F88894E570BCB_CustomAttributesCacheGenerator_life(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x63\x6F\x6E\x64\x73\x20\x74\x6F\x20\x64\x69\x6D\x20\x74\x68\x65\x20\x6C\x69\x67\x68\x74"), NULL);
	}
}
static void ETFXRotation_tFAD3FD3D4A0D634146B68B89E9BBF223473BA881_CustomAttributesCacheGenerator_rotateVector(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x6F\x74\x61\x74\x65\x20\x61\x78\x69\x73\x65\x73\x20\x62\x79\x20\x64\x65\x67\x72\x65\x65\x73\x20\x70\x65\x72\x20\x73\x65\x63\x6F\x6E\x64"), NULL);
	}
}
static void GreedBeh_t161E4AD27111A77E72F5EB4F6087221B35167544_CustomAttributesCacheGenerator_textHolder(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GreedBeh_t161E4AD27111A77E72F5EB4F6087221B35167544_CustomAttributesCacheGenerator_text(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GreedBeh_t161E4AD27111A77E72F5EB4F6087221B35167544_CustomAttributesCacheGenerator_fillAria(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GreedBeh_t161E4AD27111A77E72F5EB4F6087221B35167544_CustomAttributesCacheGenerator_fillAriaImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GreedBeh_t161E4AD27111A77E72F5EB4F6087221B35167544_CustomAttributesCacheGenerator_green(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GreedBeh_t161E4AD27111A77E72F5EB4F6087221B35167544_CustomAttributesCacheGenerator_red(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GreedBeh_t161E4AD27111A77E72F5EB4F6087221B35167544_CustomAttributesCacheGenerator_greedMaxLVL(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void GreedBeh_t161E4AD27111A77E72F5EB4F6087221B35167544_CustomAttributesCacheGenerator_greedMaxHP(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GreedBeh_t161E4AD27111A77E72F5EB4F6087221B35167544_CustomAttributesCacheGenerator_timeToDifficultyUp(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GreedBeh_t161E4AD27111A77E72F5EB4F6087221B35167544_CustomAttributesCacheGenerator_coinsPriceOnHpMult(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GreedBeh_t161E4AD27111A77E72F5EB4F6087221B35167544_CustomAttributesCacheGenerator_GreedBeh_GreedController_m9AD3972A3D4EDF41FE9B6BB5AAC75AF6071E14AD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGreedControllerU3Ed__23_tD199267145EA542A06DB4BE74C527EB42673B3D8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGreedControllerU3Ed__23_tD199267145EA542A06DB4BE74C527EB42673B3D8_0_0_0_var), NULL);
	}
}
static void GreedBeh_t161E4AD27111A77E72F5EB4F6087221B35167544_CustomAttributesCacheGenerator_GreedBeh_DifficultyApper_mD7A702FA04D913E24905B2B1082A39093F270381(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDifficultyApperU3Ed__24_t904C0FB3DB6C190E3DB3FCE00435363F1E101970_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDifficultyApperU3Ed__24_t904C0FB3DB6C190E3DB3FCE00435363F1E101970_0_0_0_var), NULL);
	}
}
static void U3CGreedControllerU3Ed__23_tD199267145EA542A06DB4BE74C527EB42673B3D8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGreedControllerU3Ed__23_tD199267145EA542A06DB4BE74C527EB42673B3D8_CustomAttributesCacheGenerator_U3CGreedControllerU3Ed__23__ctor_mED24E9A036599767030BB969BE03320D52DE3BB8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGreedControllerU3Ed__23_tD199267145EA542A06DB4BE74C527EB42673B3D8_CustomAttributesCacheGenerator_U3CGreedControllerU3Ed__23_System_IDisposable_Dispose_m778A80884DF8544DB4F7E374C965D73586794490(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGreedControllerU3Ed__23_tD199267145EA542A06DB4BE74C527EB42673B3D8_CustomAttributesCacheGenerator_U3CGreedControllerU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8C0F4A03686C2661E36FC7B7A700315ADC72348F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGreedControllerU3Ed__23_tD199267145EA542A06DB4BE74C527EB42673B3D8_CustomAttributesCacheGenerator_U3CGreedControllerU3Ed__23_System_Collections_IEnumerator_Reset_m2114CC24AB04A2370B6F76FD9AE1FEB45F236F55(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGreedControllerU3Ed__23_tD199267145EA542A06DB4BE74C527EB42673B3D8_CustomAttributesCacheGenerator_U3CGreedControllerU3Ed__23_System_Collections_IEnumerator_get_Current_mACA549512D9667723F550D555D208986BF10C8AD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDifficultyApperU3Ed__24_t904C0FB3DB6C190E3DB3FCE00435363F1E101970_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDifficultyApperU3Ed__24_t904C0FB3DB6C190E3DB3FCE00435363F1E101970_CustomAttributesCacheGenerator_U3CDifficultyApperU3Ed__24__ctor_mBA4F037F96B2F476A714F6DBDC8B7A2CA90287BC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDifficultyApperU3Ed__24_t904C0FB3DB6C190E3DB3FCE00435363F1E101970_CustomAttributesCacheGenerator_U3CDifficultyApperU3Ed__24_System_IDisposable_Dispose_m134F71DF2B1632B26105A2275EABFABCE0E632F3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDifficultyApperU3Ed__24_t904C0FB3DB6C190E3DB3FCE00435363F1E101970_CustomAttributesCacheGenerator_U3CDifficultyApperU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C742F57DD6CF120F66DD900101944356EC7B002(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDifficultyApperU3Ed__24_t904C0FB3DB6C190E3DB3FCE00435363F1E101970_CustomAttributesCacheGenerator_U3CDifficultyApperU3Ed__24_System_Collections_IEnumerator_Reset_m3A9261AFA76DEFAEF6A962E88D9F46E5ACBEC7E1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDifficultyApperU3Ed__24_t904C0FB3DB6C190E3DB3FCE00435363F1E101970_CustomAttributesCacheGenerator_U3CDifficultyApperU3Ed__24_System_Collections_IEnumerator_get_Current_m8690A70EEAC79F6399EE311D2B93DC5F7AA85198(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CameraBeh_t20869EEE55151F514702E119C4FD6A91ACA81C15_CustomAttributesCacheGenerator_lerpVal(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraBeh_t20869EEE55151F514702E119C4FD6A91ACA81C15_CustomAttributesCacheGenerator_player(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_price(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_fallLasting(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_fallHight(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_curve(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_enableRotation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_rotationSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_lifetime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_staticVfx(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_collectVfx(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_onCollisioVfx(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_collisionPlayTimePoint(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_CoinBeh_Rotator_m40AA8668E10BFADA112DA289E78E735E46B7E4D5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRotatorU3Ed__17_t51FCCC2D459A39620C3DC1DF763DAB8F5A256B6A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRotatorU3Ed__17_t51FCCC2D459A39620C3DC1DF763DAB8F5A256B6A_0_0_0_var), NULL);
	}
}
static void CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_CoinBeh_Fall_m4200C6B1973602768A994122AFBAF516C305FDC7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFallU3Ed__18_t165EC7F06440CB52F1578139076D6BA440E3C330_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFallU3Ed__18_t165EC7F06440CB52F1578139076D6BA440E3C330_0_0_0_var), NULL);
	}
}
static void CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_CoinBeh_DelayedDie_mF36E21BA2FF53B40C20F22FAC0460B9972D72EAA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDelayedDieU3Ed__22_t13921E7880F014290B349242E2F41F1A80227638_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDelayedDieU3Ed__22_t13921E7880F014290B349242E2F41F1A80227638_0_0_0_var), NULL);
	}
}
static void U3CRotatorU3Ed__17_t51FCCC2D459A39620C3DC1DF763DAB8F5A256B6A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRotatorU3Ed__17_t51FCCC2D459A39620C3DC1DF763DAB8F5A256B6A_CustomAttributesCacheGenerator_U3CRotatorU3Ed__17__ctor_m114D28EEABEDC896272B845F7A91892F68306FFE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRotatorU3Ed__17_t51FCCC2D459A39620C3DC1DF763DAB8F5A256B6A_CustomAttributesCacheGenerator_U3CRotatorU3Ed__17_System_IDisposable_Dispose_m6315BB8051C0C7EE5D1625FC4E03031B2219CD05(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRotatorU3Ed__17_t51FCCC2D459A39620C3DC1DF763DAB8F5A256B6A_CustomAttributesCacheGenerator_U3CRotatorU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8A3D8CB7F45697A23FC98BAB9AA05B83C4A447CC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRotatorU3Ed__17_t51FCCC2D459A39620C3DC1DF763DAB8F5A256B6A_CustomAttributesCacheGenerator_U3CRotatorU3Ed__17_System_Collections_IEnumerator_Reset_m81DCA7E77FC0A97396961845CDD5DCBA2F9C5154(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRotatorU3Ed__17_t51FCCC2D459A39620C3DC1DF763DAB8F5A256B6A_CustomAttributesCacheGenerator_U3CRotatorU3Ed__17_System_Collections_IEnumerator_get_Current_m9CE4E05E678854976D2A6843AE0B6262F41E8693(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFallU3Ed__18_t165EC7F06440CB52F1578139076D6BA440E3C330_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFallU3Ed__18_t165EC7F06440CB52F1578139076D6BA440E3C330_CustomAttributesCacheGenerator_U3CFallU3Ed__18__ctor_mA1BD6BEE887A2EE42B90B2F75315E7E1FB4EC535(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFallU3Ed__18_t165EC7F06440CB52F1578139076D6BA440E3C330_CustomAttributesCacheGenerator_U3CFallU3Ed__18_System_IDisposable_Dispose_mF309B4C364F44A922728EA07BDA6CF9635CFA32F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFallU3Ed__18_t165EC7F06440CB52F1578139076D6BA440E3C330_CustomAttributesCacheGenerator_U3CFallU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAC3583E0B3BBC249538DAFF1FC5D2A1D0F69BC0F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFallU3Ed__18_t165EC7F06440CB52F1578139076D6BA440E3C330_CustomAttributesCacheGenerator_U3CFallU3Ed__18_System_Collections_IEnumerator_Reset_m9D7DEE6CEADC090BFB3D63C3937B4341E28F3A8D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFallU3Ed__18_t165EC7F06440CB52F1578139076D6BA440E3C330_CustomAttributesCacheGenerator_U3CFallU3Ed__18_System_Collections_IEnumerator_get_Current_m1211A1CDA0C02D5916843A755AE7A032E0954ADE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedDieU3Ed__22_t13921E7880F014290B349242E2F41F1A80227638_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDelayedDieU3Ed__22_t13921E7880F014290B349242E2F41F1A80227638_CustomAttributesCacheGenerator_U3CDelayedDieU3Ed__22__ctor_m83D599148B986FE4A385354A2414B1B1F207E131(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedDieU3Ed__22_t13921E7880F014290B349242E2F41F1A80227638_CustomAttributesCacheGenerator_U3CDelayedDieU3Ed__22_System_IDisposable_Dispose_mC5D8224A1F4D3929672FDDE8AD96BE060E09CAFA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedDieU3Ed__22_t13921E7880F014290B349242E2F41F1A80227638_CustomAttributesCacheGenerator_U3CDelayedDieU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAD32D4B0BCD495AC64F7C0E2B6B380AA6D349240(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedDieU3Ed__22_t13921E7880F014290B349242E2F41F1A80227638_CustomAttributesCacheGenerator_U3CDelayedDieU3Ed__22_System_Collections_IEnumerator_Reset_m787F6488D83FA9BC2AD52E672155FEB4EA8658C7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedDieU3Ed__22_t13921E7880F014290B349242E2F41F1A80227638_CustomAttributesCacheGenerator_U3CDelayedDieU3Ed__22_System_Collections_IEnumerator_get_Current_mA3ED1713C218EF84A4DC28FB92BA6E398DF2C36C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CounterBeh_t9616A7B7C30413FA3FEE01EC6752F51630E2490B_CustomAttributesCacheGenerator_holderRect(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CounterBeh_t9616A7B7C30413FA3FEE01EC6752F51630E2490B_CustomAttributesCacheGenerator_value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CounterBeh_t9616A7B7C30413FA3FEE01EC6752F51630E2490B_CustomAttributesCacheGenerator_lerpLasting(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CounterBeh_t9616A7B7C30413FA3FEE01EC6752F51630E2490B_CustomAttributesCacheGenerator_curve(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CounterBeh_t9616A7B7C30413FA3FEE01EC6752F51630E2490B_CustomAttributesCacheGenerator_CounterBeh_LerperNumber_m599BE2B23127C037AA97382BA0BA024FFF2D3D2F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLerperNumberU3Ed__11_t28CF8F99BC897162AE9B411D48413A6B85662D50_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLerperNumberU3Ed__11_t28CF8F99BC897162AE9B411D48413A6B85662D50_0_0_0_var), NULL);
	}
}
static void U3CLerperNumberU3Ed__11_t28CF8F99BC897162AE9B411D48413A6B85662D50_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLerperNumberU3Ed__11_t28CF8F99BC897162AE9B411D48413A6B85662D50_CustomAttributesCacheGenerator_U3CLerperNumberU3Ed__11__ctor_mD9FD4C2897BEC16FC1A9C2B18F6FE69619970BE8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLerperNumberU3Ed__11_t28CF8F99BC897162AE9B411D48413A6B85662D50_CustomAttributesCacheGenerator_U3CLerperNumberU3Ed__11_System_IDisposable_Dispose_m6660869AC0EA25A327B028E9F253931B2072A96B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLerperNumberU3Ed__11_t28CF8F99BC897162AE9B411D48413A6B85662D50_CustomAttributesCacheGenerator_U3CLerperNumberU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB9730D565D67363A535F32E2EA770B2950AF17ED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLerperNumberU3Ed__11_t28CF8F99BC897162AE9B411D48413A6B85662D50_CustomAttributesCacheGenerator_U3CLerperNumberU3Ed__11_System_Collections_IEnumerator_Reset_m868765C8474E200FCC40548774D832A1BB02DD86(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLerperNumberU3Ed__11_t28CF8F99BC897162AE9B411D48413A6B85662D50_CustomAttributesCacheGenerator_U3CLerperNumberU3Ed__11_System_Collections_IEnumerator_get_Current_mB0792E3850208F01A09783A4C819C80F9558CF92(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ShadowBeh_t7FA3CA91153DC03624C8AC2CFE086E52208B33E4_CustomAttributesCacheGenerator_size(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShadowBeh_t7FA3CA91153DC03624C8AC2CFE086E52208B33E4_CustomAttributesCacheGenerator_shadow(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ButtonBehaviour_tA8AD449AB299BAA40D5E8D4A51E69F8F581C32D1_CustomAttributesCacheGenerator_pingPongActive(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec_t1428115A37A81A4A4EA416136A8B924C3A57D2DE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CheckBoxBeh_tAC6E37B116D6534F7D76B8F761BA09C9ED620886_CustomAttributesCacheGenerator_animLength(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CheckBoxBeh_tAC6E37B116D6534F7D76B8F761BA09C9ED620886_CustomAttributesCacheGenerator_On(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CheckBoxBeh_tAC6E37B116D6534F7D76B8F761BA09C9ED620886_CustomAttributesCacheGenerator_Off(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_t9A0B5962E13A7FFE7F5669E11A2AF53CD05CC83C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_tEBD52E4B85CC8898B0CBAC4D9EC3E40631F2E733_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LogoClorsChanger_tB87C1458A459E43528CDFEB168040A64AAAF368A_CustomAttributesCacheGenerator_colors(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LogoClorsChanger_tB87C1458A459E43528CDFEB168040A64AAAF368A_CustomAttributesCacheGenerator_text(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LogoClorsChanger_tB87C1458A459E43528CDFEB168040A64AAAF368A_CustomAttributesCacheGenerator_rect(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_SetDefaultScale_m756116B110805E0750BD50CFB5AC9EAD5FEB0E0C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_SetPivotAndAnchors_m3739A3FDBB6ECF890D2794F3A0D140C241EAD927(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_GetSize_m5E15B6433CDE84F88039436390CC83E1D87EC4D9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_GetWidth_m65D963AF477887BF495B2F9CB66E81B921CA32F3(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_GetHeight_m061AB7A3F469AD5BB5C432F69063FB29E198E3E0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_SetPositionOfPivot_mB94D62C0DA9123D1A5237B7F9352F36C40ECDCE9(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_SetLeftBottomPosition_m9A969464222C09A18146A52B02C4A16646B12A54(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_SetLeftTopPosition_m262E8022AE28161E22ED3442CF58FBB35909667B(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_SetRightBottomPosition_m0E6A4FE54AD6CF0F22617AA9E942207757C1D64C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_SetRightTopPosition_mED1168AD12631EECFD0C863BD87E7430B5CF183C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_SetSize_mECA4608AA6FAE6B4F3955D38542102448BF50C7C(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_SetWidth_mF6725AB65F1C2BB7355870EAE41A5A3BF5E16CF5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_SetHeight_m23A05DB16E19D8931FC02B61AD32D22B52ECEE83(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void BattleManager_tBA2D4B3875E580ACB0E65449760F24CECAE8FC40_CustomAttributesCacheGenerator_player(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BattleManager_tBA2D4B3875E580ACB0E65449760F24CECAE8FC40_CustomAttributesCacheGenerator_Coins(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BattleManager_tBA2D4B3875E580ACB0E65449760F24CECAE8FC40_CustomAttributesCacheGenerator_CoinsRare(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BattleManager_tBA2D4B3875E580ACB0E65449760F24CECAE8FC40_CustomAttributesCacheGenerator_props(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BattleManager_tBA2D4B3875E580ACB0E65449760F24CECAE8FC40_CustomAttributesCacheGenerator_chanceForRare(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BattleManager_tBA2D4B3875E580ACB0E65449760F24CECAE8FC40_CustomAttributesCacheGenerator_delayBetweenCooinSpawn(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BattleManager_tBA2D4B3875E580ACB0E65449760F24CECAE8FC40_CustomAttributesCacheGenerator_delayBetweenPropSpawn(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BattleManager_tBA2D4B3875E580ACB0E65449760F24CECAE8FC40_CustomAttributesCacheGenerator_widthToSpawnCoins(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BattleManager_tBA2D4B3875E580ACB0E65449760F24CECAE8FC40_CustomAttributesCacheGenerator_BattleManager_CoinsDropper_m394A526819130312751C5AB1A3730B358709802F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCoinsDropperU3Ed__16_tB255480E26513BDB1C6E8741FD8348B2C44B83EE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCoinsDropperU3Ed__16_tB255480E26513BDB1C6E8741FD8348B2C44B83EE_0_0_0_var), NULL);
	}
}
static void BattleManager_tBA2D4B3875E580ACB0E65449760F24CECAE8FC40_CustomAttributesCacheGenerator_BattleManager_PropsSpawner_mC5067D1A2AFFABEC2BFDD6FEBD7F92E0A275DB29(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPropsSpawnerU3Ed__17_tFF2684BDB6250DFA569D66790A7747F407DE4815_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CPropsSpawnerU3Ed__17_tFF2684BDB6250DFA569D66790A7747F407DE4815_0_0_0_var), NULL);
	}
}
static void U3CCoinsDropperU3Ed__16_tB255480E26513BDB1C6E8741FD8348B2C44B83EE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCoinsDropperU3Ed__16_tB255480E26513BDB1C6E8741FD8348B2C44B83EE_CustomAttributesCacheGenerator_U3CCoinsDropperU3Ed__16__ctor_m5B20A37FCEFC5792A1ECBBE76109AAA212EDFE78(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoinsDropperU3Ed__16_tB255480E26513BDB1C6E8741FD8348B2C44B83EE_CustomAttributesCacheGenerator_U3CCoinsDropperU3Ed__16_System_IDisposable_Dispose_m2513921149CA83BF9D4ED0949FB0A1CB476A73C4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoinsDropperU3Ed__16_tB255480E26513BDB1C6E8741FD8348B2C44B83EE_CustomAttributesCacheGenerator_U3CCoinsDropperU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mED52FFF71BA21C416E7500AE61BDD67E9BA46538(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoinsDropperU3Ed__16_tB255480E26513BDB1C6E8741FD8348B2C44B83EE_CustomAttributesCacheGenerator_U3CCoinsDropperU3Ed__16_System_Collections_IEnumerator_Reset_m0D4F891AA49D23F2917BBE2D2C97BB9318997040(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCoinsDropperU3Ed__16_tB255480E26513BDB1C6E8741FD8348B2C44B83EE_CustomAttributesCacheGenerator_U3CCoinsDropperU3Ed__16_System_Collections_IEnumerator_get_Current_mF4B5A889FECEAB4450FCD66D115160011B90FF50(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPropsSpawnerU3Ed__17_tFF2684BDB6250DFA569D66790A7747F407DE4815_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPropsSpawnerU3Ed__17_tFF2684BDB6250DFA569D66790A7747F407DE4815_CustomAttributesCacheGenerator_U3CPropsSpawnerU3Ed__17__ctor_m0C53E3153219B6888EFF50E1FBEBDA78A1E9FE2B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPropsSpawnerU3Ed__17_tFF2684BDB6250DFA569D66790A7747F407DE4815_CustomAttributesCacheGenerator_U3CPropsSpawnerU3Ed__17_System_IDisposable_Dispose_m5F2BA115EA1588C1E45604A9E7EE633291E6D742(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPropsSpawnerU3Ed__17_tFF2684BDB6250DFA569D66790A7747F407DE4815_CustomAttributesCacheGenerator_U3CPropsSpawnerU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD2BCDF6BD7EA85A558C6090E32A48DAFF313AB19(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPropsSpawnerU3Ed__17_tFF2684BDB6250DFA569D66790A7747F407DE4815_CustomAttributesCacheGenerator_U3CPropsSpawnerU3Ed__17_System_Collections_IEnumerator_Reset_m8670F90638804486CC750EBA42D27124A4A4F4D1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPropsSpawnerU3Ed__17_tFF2684BDB6250DFA569D66790A7747F407DE4815_CustomAttributesCacheGenerator_U3CPropsSpawnerU3Ed__17_System_Collections_IEnumerator_get_Current_m54019008A86128E9F048DF85037C0518527FEB58(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_mainPanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_lerpCurve(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_lerpLasting(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_bestScore(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_bestScore2(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_bestScore2Descript(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_resultScore(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_resultScoreDescript(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_resultText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_green(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_resultWindow(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_counter(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_slider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_showWindowAnimDur(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_BattleUiManager_RectLerper_m0381C4B0C57388EBA10CA3C1C8EDEAA217AB3EF8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRectLerperU3Ed__22_t1529E126C3ABC63AAA4C8A3C18CB37944708E770_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRectLerperU3Ed__22_t1529E126C3ABC63AAA4C8A3C18CB37944708E770_0_0_0_var), NULL);
	}
}
static void U3CRectLerperU3Ed__22_t1529E126C3ABC63AAA4C8A3C18CB37944708E770_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRectLerperU3Ed__22_t1529E126C3ABC63AAA4C8A3C18CB37944708E770_CustomAttributesCacheGenerator_U3CRectLerperU3Ed__22__ctor_mFE4BC35956BB1F5CAEB16BB2382C1C7BAB5FA335(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRectLerperU3Ed__22_t1529E126C3ABC63AAA4C8A3C18CB37944708E770_CustomAttributesCacheGenerator_U3CRectLerperU3Ed__22_System_IDisposable_Dispose_m3BA1315708803BD83C616C77D7A255BC598135F1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRectLerperU3Ed__22_t1529E126C3ABC63AAA4C8A3C18CB37944708E770_CustomAttributesCacheGenerator_U3CRectLerperU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9E6376E59EBF96B900D203887FE32F6EE74A3AC7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRectLerperU3Ed__22_t1529E126C3ABC63AAA4C8A3C18CB37944708E770_CustomAttributesCacheGenerator_U3CRectLerperU3Ed__22_System_Collections_IEnumerator_Reset_m0395461C746FA32266CEAB64C0CAF0B1F5D14219(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRectLerperU3Ed__22_t1529E126C3ABC63AAA4C8A3C18CB37944708E770_CustomAttributesCacheGenerator_U3CRectLerperU3Ed__22_System_Collections_IEnumerator_get_Current_m2F39E8D4723F7EA8CE96441AB4D337E0956F07A5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void EventManager_t979564A250C00C4A14D6D8CD2B21B4EE72AD62E3_CustomAttributesCacheGenerator_U3CManagerLoadedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventManager_t979564A250C00C4A14D6D8CD2B21B4EE72AD62E3_CustomAttributesCacheGenerator_EventManager_get_ManagerLoaded_m0CDF1ABA9D71D9FDF98A55DED0580ABE7BB09119(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventManager_t979564A250C00C4A14D6D8CD2B21B4EE72AD62E3_CustomAttributesCacheGenerator_EventManager_set_ManagerLoaded_mF465E0FC890BAFCD46E08A7B45C341CCFF51180C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventManager_t979564A250C00C4A14D6D8CD2B21B4EE72AD62E3_CustomAttributesCacheGenerator_EventManager_ClearAllEventsBut_mDE1909B896E1E3A4B6F71B021849CB2A01EEA723____except0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void GameCore_tBDD99C41346679D5795D34FAA7EA0D1916B358CA_CustomAttributesCacheGenerator_GameCore_InitCore_m8B8D858F49C6159AC07980638C0F6155DCFF0B3C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInitCoreU3Ed__3_tDFD7C3A276A8679CE027A37454FC646A663895B5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CInitCoreU3Ed__3_tDFD7C3A276A8679CE027A37454FC646A663895B5_0_0_0_var), NULL);
	}
}
static void GameCore_tBDD99C41346679D5795D34FAA7EA0D1916B358CA_CustomAttributesCacheGenerator_GameCore_ManagersInit_m628635156AB71C4AB20CD389F9FA13204DA224C6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CManagersInitU3Ed__4_tFA7811EED864BC0384726559E3D01C81BFC920AA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CManagersInitU3Ed__4_tFA7811EED864BC0384726559E3D01C81BFC920AA_0_0_0_var), NULL);
	}
}
static void U3CInitCoreU3Ed__3_tDFD7C3A276A8679CE027A37454FC646A663895B5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInitCoreU3Ed__3_tDFD7C3A276A8679CE027A37454FC646A663895B5_CustomAttributesCacheGenerator_U3CInitCoreU3Ed__3__ctor_mCCAD943136895CF43DF40DAD28D6D05461041FA7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitCoreU3Ed__3_tDFD7C3A276A8679CE027A37454FC646A663895B5_CustomAttributesCacheGenerator_U3CInitCoreU3Ed__3_System_IDisposable_Dispose_m214566DFB3B6CC50D2C9CC9AD7B65452E2FDBCF9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitCoreU3Ed__3_tDFD7C3A276A8679CE027A37454FC646A663895B5_CustomAttributesCacheGenerator_U3CInitCoreU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF66EBC679A96E1B1F5BFF2DD05003DFA1A3C8BF7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitCoreU3Ed__3_tDFD7C3A276A8679CE027A37454FC646A663895B5_CustomAttributesCacheGenerator_U3CInitCoreU3Ed__3_System_Collections_IEnumerator_Reset_mE2AC84D59ACB0435151AC467DBC64F928500E0EA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitCoreU3Ed__3_tDFD7C3A276A8679CE027A37454FC646A663895B5_CustomAttributesCacheGenerator_U3CInitCoreU3Ed__3_System_Collections_IEnumerator_get_Current_mAAA0F1C1AC6138B672DC1A1CD62FC68E5CE1ED33(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CManagersInitU3Ed__4_tFA7811EED864BC0384726559E3D01C81BFC920AA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CManagersInitU3Ed__4_tFA7811EED864BC0384726559E3D01C81BFC920AA_CustomAttributesCacheGenerator_U3CManagersInitU3Ed__4__ctor_m9D17335FFA81B6E19CFAC5F36D195B5B6465EA49(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CManagersInitU3Ed__4_tFA7811EED864BC0384726559E3D01C81BFC920AA_CustomAttributesCacheGenerator_U3CManagersInitU3Ed__4_System_IDisposable_Dispose_m3BC8E51F2662DD30EA6D7F7F5ECF3D3DA7599584(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CManagersInitU3Ed__4_tFA7811EED864BC0384726559E3D01C81BFC920AA_CustomAttributesCacheGenerator_U3CManagersInitU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDBAA898997E6235197484255E05B6FF5E23EDAE8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CManagersInitU3Ed__4_tFA7811EED864BC0384726559E3D01C81BFC920AA_CustomAttributesCacheGenerator_U3CManagersInitU3Ed__4_System_Collections_IEnumerator_Reset_m2FBF32B1158983877657F90DEFDA128510E8FCA9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CManagersInitU3Ed__4_tFA7811EED864BC0384726559E3D01C81BFC920AA_CustomAttributesCacheGenerator_U3CManagersInitU3Ed__4_System_Collections_IEnumerator_get_Current_m00C0BEFE87BD041DAEEDC27CA297B07357197B3D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MenuManager_t4D94FC94EBCD4BC8FC4248EBC26B768A175333F1_CustomAttributesCacheGenerator_mainPanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuManager_t4D94FC94EBCD4BC8FC4248EBC26B768A175333F1_CustomAttributesCacheGenerator_lerpCurve(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuManager_t4D94FC94EBCD4BC8FC4248EBC26B768A175333F1_CustomAttributesCacheGenerator_lerpLasting(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuManager_t4D94FC94EBCD4BC8FC4248EBC26B768A175333F1_CustomAttributesCacheGenerator_MenuManager_RectLerper_m673FD921A02A52C9AEEA52EBBB0BA25822612C27(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRectLerperU3Ed__7_t627D2B2D7285C0085962BF78194CD59D57F049EA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRectLerperU3Ed__7_t627D2B2D7285C0085962BF78194CD59D57F049EA_0_0_0_var), NULL);
	}
}
static void U3CRectLerperU3Ed__7_t627D2B2D7285C0085962BF78194CD59D57F049EA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRectLerperU3Ed__7_t627D2B2D7285C0085962BF78194CD59D57F049EA_CustomAttributesCacheGenerator_U3CRectLerperU3Ed__7__ctor_m30B56C1B8F94B603CDD28B9094630DBCC58DBB37(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRectLerperU3Ed__7_t627D2B2D7285C0085962BF78194CD59D57F049EA_CustomAttributesCacheGenerator_U3CRectLerperU3Ed__7_System_IDisposable_Dispose_m07B7C2EF1F8B0D6485FC4D0AC507D3047424491C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRectLerperU3Ed__7_t627D2B2D7285C0085962BF78194CD59D57F049EA_CustomAttributesCacheGenerator_U3CRectLerperU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0DE627B04D8C907BA9536B54F2DDCCA4E18CB2D5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRectLerperU3Ed__7_t627D2B2D7285C0085962BF78194CD59D57F049EA_CustomAttributesCacheGenerator_U3CRectLerperU3Ed__7_System_Collections_IEnumerator_Reset_m087D959A04E82FA45CF5482C60AE988A0E886B50(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRectLerperU3Ed__7_t627D2B2D7285C0085962BF78194CD59D57F049EA_CustomAttributesCacheGenerator_U3CRectLerperU3Ed__7_System_Collections_IEnumerator_get_Current_mD7CE3F95967A694D0005D7CD0A7792718586875E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PoolManager_t884433F1D81180C1DFD89A0ED0F37F5746FED153_CustomAttributesCacheGenerator_U3CManagerLoadedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PoolManager_t884433F1D81180C1DFD89A0ED0F37F5746FED153_CustomAttributesCacheGenerator_PoolManager_get_ManagerLoaded_mEA8DC0C106DAFF98B4E44BE29BD05D25B898CFD6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PoolManager_t884433F1D81180C1DFD89A0ED0F37F5746FED153_CustomAttributesCacheGenerator_PoolManager_set_ManagerLoaded_mB1934AED08E82FA5E05C24928872827F339EB026(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PoolManager_t884433F1D81180C1DFD89A0ED0F37F5746FED153_CustomAttributesCacheGenerator_PoolManager_DelayedExecuteRout_m925F863DF8679D4EAD358D9CEF05963B6DA83ADB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDelayedExecuteRoutU3Ed__12_tB4B9F1609DA131FA9762A04E9878ED5B781C2144_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDelayedExecuteRoutU3Ed__12_tB4B9F1609DA131FA9762A04E9878ED5B781C2144_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass11_0_t2C5F534CB7E996542C21936DC0E910BCF2E76DD8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDelayedExecuteRoutU3Ed__12_tB4B9F1609DA131FA9762A04E9878ED5B781C2144_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDelayedExecuteRoutU3Ed__12_tB4B9F1609DA131FA9762A04E9878ED5B781C2144_CustomAttributesCacheGenerator_U3CDelayedExecuteRoutU3Ed__12__ctor_mD9E6417E2A02D10C7437F9EB77CE8D88F83AB8E8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedExecuteRoutU3Ed__12_tB4B9F1609DA131FA9762A04E9878ED5B781C2144_CustomAttributesCacheGenerator_U3CDelayedExecuteRoutU3Ed__12_System_IDisposable_Dispose_m9ACD873B22A38611DBA9C4ACBD113A473FE78DB8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedExecuteRoutU3Ed__12_tB4B9F1609DA131FA9762A04E9878ED5B781C2144_CustomAttributesCacheGenerator_U3CDelayedExecuteRoutU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDE80C087E891DEAEC694EA8BC3153EEAD3D96953(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedExecuteRoutU3Ed__12_tB4B9F1609DA131FA9762A04E9878ED5B781C2144_CustomAttributesCacheGenerator_U3CDelayedExecuteRoutU3Ed__12_System_Collections_IEnumerator_Reset_mC12C650FE8CF3A3377E0CB43B7C950128D2666F0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayedExecuteRoutU3Ed__12_tB4B9F1609DA131FA9762A04E9878ED5B781C2144_CustomAttributesCacheGenerator_U3CDelayedExecuteRoutU3Ed__12_System_Collections_IEnumerator_get_Current_m9C8E239CAC36693CAB41741151A0461C56FD1137(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_canvas(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_Panel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_LoadingImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_dinoImage(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_loadImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_fadeInLasting(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_fadeOutLasting(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_LoadingText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_json_BundleInfoURL(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_U3CManagerLoadedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_ScenesManager_get_ManagerLoaded_m9853E6F6C20C9A0A8B8AE1CCB175DBC4A6CC1A7B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_ScenesManager_set_ManagerLoaded_m498FBC0E72C9775D90B33FB968326DDA1AE027BB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_ScenesManager_LoadMenu_mB5F53C0CBB07D0F8DCCD0582718698BE354350A9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadMenuU3Ed__28_tB58C1122E8D0ED148523461E8CD31F85CDD1D0D4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadMenuU3Ed__28_tB58C1122E8D0ED148523461E8CD31F85CDD1D0D4_0_0_0_var), NULL);
	}
}
static void ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_ScenesManager_LoadBattleRout_m6E134CC1BC70F1A493C32FE27BDE87D0B9796218(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadBattleRoutU3Ed__29_tF474E0A73CD058CD0A109CD9822D8B23B25D88BC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadBattleRoutU3Ed__29_tF474E0A73CD058CD0A109CD9822D8B23B25D88BC_0_0_0_var), NULL);
	}
}
static void ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_ScenesManager_LoadBundle_m6E2FD6AEC587F263C243CE835383630A5C4A840D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadBundleU3Ed__34_t5883FB0ED07D57BAA2813F454550FAEFB16158C4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadBundleU3Ed__34_t5883FB0ED07D57BAA2813F454550FAEFB16158C4_0_0_0_var), NULL);
	}
}
static void ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_ScenesManager_GetBundlesVersionInfo_m877050724519A0516789CF3A91D43C14F5CFC385(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetBundlesVersionInfoU3Ed__35_t723284E5DB9205EE817B2FFA5A0711AC94FD84B0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetBundlesVersionInfoU3Ed__35_t723284E5DB9205EE817B2FFA5A0711AC94FD84B0_0_0_0_var), NULL);
	}
}
static void U3CLoadMenuU3Ed__28_tB58C1122E8D0ED148523461E8CD31F85CDD1D0D4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadMenuU3Ed__28_tB58C1122E8D0ED148523461E8CD31F85CDD1D0D4_CustomAttributesCacheGenerator_U3CLoadMenuU3Ed__28__ctor_m83C8D766D0CA9BCE7DEC0052E90A6478B49DF083(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadMenuU3Ed__28_tB58C1122E8D0ED148523461E8CD31F85CDD1D0D4_CustomAttributesCacheGenerator_U3CLoadMenuU3Ed__28_System_IDisposable_Dispose_mCCA53E59ABB7B1BF4C661A5FEE7D434EC29D695A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadMenuU3Ed__28_tB58C1122E8D0ED148523461E8CD31F85CDD1D0D4_CustomAttributesCacheGenerator_U3CLoadMenuU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD0B1FF8419F91341A03CF453DDF1D7BBA16C16AB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadMenuU3Ed__28_tB58C1122E8D0ED148523461E8CD31F85CDD1D0D4_CustomAttributesCacheGenerator_U3CLoadMenuU3Ed__28_System_Collections_IEnumerator_Reset_mF54E18EE6E6C2D73AA2A862B2003F8A6FF3873BF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadMenuU3Ed__28_tB58C1122E8D0ED148523461E8CD31F85CDD1D0D4_CustomAttributesCacheGenerator_U3CLoadMenuU3Ed__28_System_Collections_IEnumerator_get_Current_m8C521593D1D04B269A3554CC7C18CEB8E5C17F28(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadBattleRoutU3Ed__29_tF474E0A73CD058CD0A109CD9822D8B23B25D88BC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadBattleRoutU3Ed__29_tF474E0A73CD058CD0A109CD9822D8B23B25D88BC_CustomAttributesCacheGenerator_U3CLoadBattleRoutU3Ed__29__ctor_m49F6DDD14DC52A8E2123C7F81BED86B23CB4A997(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadBattleRoutU3Ed__29_tF474E0A73CD058CD0A109CD9822D8B23B25D88BC_CustomAttributesCacheGenerator_U3CLoadBattleRoutU3Ed__29_System_IDisposable_Dispose_m75BE0F49721F852BAEEBCEC8EE7B1B74CBF57D76(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadBattleRoutU3Ed__29_tF474E0A73CD058CD0A109CD9822D8B23B25D88BC_CustomAttributesCacheGenerator_U3CLoadBattleRoutU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1BF2ABDA675CFC41535E484C711B664B0E3FE2F3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadBattleRoutU3Ed__29_tF474E0A73CD058CD0A109CD9822D8B23B25D88BC_CustomAttributesCacheGenerator_U3CLoadBattleRoutU3Ed__29_System_Collections_IEnumerator_Reset_mEA0F3257C44B1B2EE12C96B014A43D9F5C06BA3C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadBattleRoutU3Ed__29_tF474E0A73CD058CD0A109CD9822D8B23B25D88BC_CustomAttributesCacheGenerator_U3CLoadBattleRoutU3Ed__29_System_Collections_IEnumerator_get_Current_mC09B797D42E91CDDD03F19717E8BD5EFBD03573C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadBundleU3Ed__34_t5883FB0ED07D57BAA2813F454550FAEFB16158C4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadBundleU3Ed__34_t5883FB0ED07D57BAA2813F454550FAEFB16158C4_CustomAttributesCacheGenerator_U3CLoadBundleU3Ed__34__ctor_mA0E45B9A1CAC6DF82370452648B4E06B8C8D4AB2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadBundleU3Ed__34_t5883FB0ED07D57BAA2813F454550FAEFB16158C4_CustomAttributesCacheGenerator_U3CLoadBundleU3Ed__34_System_IDisposable_Dispose_mD7EDC1DDC94917E540440B118748896313146141(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadBundleU3Ed__34_t5883FB0ED07D57BAA2813F454550FAEFB16158C4_CustomAttributesCacheGenerator_U3CLoadBundleU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC471A946990DDDC262A7554F9921392ED0015CB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadBundleU3Ed__34_t5883FB0ED07D57BAA2813F454550FAEFB16158C4_CustomAttributesCacheGenerator_U3CLoadBundleU3Ed__34_System_Collections_IEnumerator_Reset_mC3E16195AF7E2154763F4D93431FE337E2BCF4F7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadBundleU3Ed__34_t5883FB0ED07D57BAA2813F454550FAEFB16158C4_CustomAttributesCacheGenerator_U3CLoadBundleU3Ed__34_System_Collections_IEnumerator_get_Current_mDFF6DFD397416B0B367B626FC77730FB584084D0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetBundlesVersionInfoU3Ed__35_t723284E5DB9205EE817B2FFA5A0711AC94FD84B0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetBundlesVersionInfoU3Ed__35_t723284E5DB9205EE817B2FFA5A0711AC94FD84B0_CustomAttributesCacheGenerator_U3CGetBundlesVersionInfoU3Ed__35__ctor_mDDE8FD2C504647E80E61EA74994A1088842B0AFB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetBundlesVersionInfoU3Ed__35_t723284E5DB9205EE817B2FFA5A0711AC94FD84B0_CustomAttributesCacheGenerator_U3CGetBundlesVersionInfoU3Ed__35_System_IDisposable_Dispose_m51692A5E82E73E57C977E6505ACFE136F76DDCEE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetBundlesVersionInfoU3Ed__35_t723284E5DB9205EE817B2FFA5A0711AC94FD84B0_CustomAttributesCacheGenerator_U3CGetBundlesVersionInfoU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m57156DE3AB27AB6D386535BCB919FFB23D356260(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetBundlesVersionInfoU3Ed__35_t723284E5DB9205EE817B2FFA5A0711AC94FD84B0_CustomAttributesCacheGenerator_U3CGetBundlesVersionInfoU3Ed__35_System_Collections_IEnumerator_Reset_m09D18BFEF78670C3C13CD082C67704B45BB9B75D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetBundlesVersionInfoU3Ed__35_t723284E5DB9205EE817B2FFA5A0711AC94FD84B0_CustomAttributesCacheGenerator_U3CGetBundlesVersionInfoU3Ed__35_System_Collections_IEnumerator_get_Current_m5C57BB4F73F450C9DA8370F4F95A126E7ADF487D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SoundsPlayerManager_tE090F04B1A39F37D66CB73E0B0CD4BBF36E0D0E7_CustomAttributesCacheGenerator_AudioClipsArr(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundsPlayerManager_tE090F04B1A39F37D66CB73E0B0CD4BBF36E0D0E7_CustomAttributesCacheGenerator_audioSourcesHolder(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundsPlayerManager_tE090F04B1A39F37D66CB73E0B0CD4BBF36E0D0E7_CustomAttributesCacheGenerator_U3CManagerLoadedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SoundsPlayerManager_tE090F04B1A39F37D66CB73E0B0CD4BBF36E0D0E7_CustomAttributesCacheGenerator_U3CIsMusicOnU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SoundsPlayerManager_tE090F04B1A39F37D66CB73E0B0CD4BBF36E0D0E7_CustomAttributesCacheGenerator_U3CIsSfxOnU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SoundsPlayerManager_tE090F04B1A39F37D66CB73E0B0CD4BBF36E0D0E7_CustomAttributesCacheGenerator_SoundsPlayerManager_get_ManagerLoaded_m9414366FE26AB2E3DA2A51F41BF130FB941BD6DD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SoundsPlayerManager_tE090F04B1A39F37D66CB73E0B0CD4BBF36E0D0E7_CustomAttributesCacheGenerator_SoundsPlayerManager_set_ManagerLoaded_m19ECE5D36E48CADC44A635B447A3118F27CEACB2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SoundsPlayerManager_tE090F04B1A39F37D66CB73E0B0CD4BBF36E0D0E7_CustomAttributesCacheGenerator_SoundsPlayerManager_get_IsMusicOn_m1B4AE98BA33C0B42882EAE5DA37784ADDD1A85C7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SoundsPlayerManager_tE090F04B1A39F37D66CB73E0B0CD4BBF36E0D0E7_CustomAttributesCacheGenerator_SoundsPlayerManager_set_IsMusicOn_m76345AC43E617E1DF6892232BFE151BE108EF821(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SoundsPlayerManager_tE090F04B1A39F37D66CB73E0B0CD4BBF36E0D0E7_CustomAttributesCacheGenerator_SoundsPlayerManager_get_IsSfxOn_m34E199685D320CE232180EAC17708945FB22ED31(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SoundsPlayerManager_tE090F04B1A39F37D66CB73E0B0CD4BBF36E0D0E7_CustomAttributesCacheGenerator_SoundsPlayerManager_set_IsSfxOn_m454038EABCA58D8C1169C251EB29032758180BE4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_dieEffect(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_collectEffect(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_playerBody(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_hats(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_HatColors(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_moveSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_rotationSlowDown(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_anim(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_rotationLasting(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_helloFrequency(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_PlayerController_RunRout_mA22B2A74036B92E6C4B3B76B212981F7F44AE3D4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRunRoutU3Ed__30_tD0905ED021E61471FA6F363886A33A1AC7330457_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRunRoutU3Ed__30_tD0905ED021E61471FA6F363886A33A1AC7330457_0_0_0_var), NULL);
	}
}
static void PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_PlayerController_SayHello_mA637BCB7DB14816CCBC35803E77F9CF1B5385C26(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSayHelloU3Ed__31_tE0422E39EEC3F88FBDEE3F3891BA2FA0933F935E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSayHelloU3Ed__31_tE0422E39EEC3F88FBDEE3F3891BA2FA0933F935E_0_0_0_var), NULL);
	}
}
static void PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_PlayerController_Rotator_m441BB00CC5F6329CAF42EE05CD76279368F9ED09(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRotatorU3Ed__32_t42629F9D4CE344E0CEE7196E0647CB2BDC4369A5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRotatorU3Ed__32_t42629F9D4CE344E0CEE7196E0647CB2BDC4369A5_0_0_0_var), NULL);
	}
}
static void U3CRunRoutU3Ed__30_tD0905ED021E61471FA6F363886A33A1AC7330457_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRunRoutU3Ed__30_tD0905ED021E61471FA6F363886A33A1AC7330457_CustomAttributesCacheGenerator_U3CRunRoutU3Ed__30__ctor_mCA94540393D6FF23BFAEE95F33AC7F75D69CDAF9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunRoutU3Ed__30_tD0905ED021E61471FA6F363886A33A1AC7330457_CustomAttributesCacheGenerator_U3CRunRoutU3Ed__30_System_IDisposable_Dispose_mD9B16A132D4A399D11929531A6CE9AFBC63EA4EE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunRoutU3Ed__30_tD0905ED021E61471FA6F363886A33A1AC7330457_CustomAttributesCacheGenerator_U3CRunRoutU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m205313068F1B73E8EA33743E806702693E22766D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunRoutU3Ed__30_tD0905ED021E61471FA6F363886A33A1AC7330457_CustomAttributesCacheGenerator_U3CRunRoutU3Ed__30_System_Collections_IEnumerator_Reset_m5B8DA141B9C088A61F3302CB6C7F279A7E475A88(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunRoutU3Ed__30_tD0905ED021E61471FA6F363886A33A1AC7330457_CustomAttributesCacheGenerator_U3CRunRoutU3Ed__30_System_Collections_IEnumerator_get_Current_m7EEC891E87CA1E9B2C39B4355D7F4553F0C9D0A8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSayHelloU3Ed__31_tE0422E39EEC3F88FBDEE3F3891BA2FA0933F935E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSayHelloU3Ed__31_tE0422E39EEC3F88FBDEE3F3891BA2FA0933F935E_CustomAttributesCacheGenerator_U3CSayHelloU3Ed__31__ctor_mA603E85A8DB0B4D7B67E720F7530EC6BBA9DDF7E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSayHelloU3Ed__31_tE0422E39EEC3F88FBDEE3F3891BA2FA0933F935E_CustomAttributesCacheGenerator_U3CSayHelloU3Ed__31_System_IDisposable_Dispose_m23FC1D002C29FCD2BF7E5430B56A3D92BF2C3816(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSayHelloU3Ed__31_tE0422E39EEC3F88FBDEE3F3891BA2FA0933F935E_CustomAttributesCacheGenerator_U3CSayHelloU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m788DCE676AD27D2E45809A95C2D721FF39525713(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSayHelloU3Ed__31_tE0422E39EEC3F88FBDEE3F3891BA2FA0933F935E_CustomAttributesCacheGenerator_U3CSayHelloU3Ed__31_System_Collections_IEnumerator_Reset_m0CE46B566DA780F05F8FEC06127AB405182FA7E2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSayHelloU3Ed__31_tE0422E39EEC3F88FBDEE3F3891BA2FA0933F935E_CustomAttributesCacheGenerator_U3CSayHelloU3Ed__31_System_Collections_IEnumerator_get_Current_mACA149E6143CEEC49B09738A94AEC131886D2F80(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRotatorU3Ed__32_t42629F9D4CE344E0CEE7196E0647CB2BDC4369A5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRotatorU3Ed__32_t42629F9D4CE344E0CEE7196E0647CB2BDC4369A5_CustomAttributesCacheGenerator_U3CRotatorU3Ed__32__ctor_m48279ADDC8D8C49C9C9B8B818F1CA7CBAF0C1057(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRotatorU3Ed__32_t42629F9D4CE344E0CEE7196E0647CB2BDC4369A5_CustomAttributesCacheGenerator_U3CRotatorU3Ed__32_System_IDisposable_Dispose_mB23FB46D08F56888E6913A5D78182521004F0F81(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRotatorU3Ed__32_t42629F9D4CE344E0CEE7196E0647CB2BDC4369A5_CustomAttributesCacheGenerator_U3CRotatorU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0114479E05754C49ECF28B18186E2E2C8682AD31(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRotatorU3Ed__32_t42629F9D4CE344E0CEE7196E0647CB2BDC4369A5_CustomAttributesCacheGenerator_U3CRotatorU3Ed__32_System_Collections_IEnumerator_Reset_m1FF085920A616461360A3512B2185E8CE39381A3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRotatorU3Ed__32_t42629F9D4CE344E0CEE7196E0647CB2BDC4369A5_CustomAttributesCacheGenerator_U3CRotatorU3Ed__32_System_Collections_IEnumerator_get_Current_mB4B770B1451A7086036EFAB0EC8056DFE025FAB5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[347] = 
{
	ColorExtensions_t72892A69207663404B47720DD90667E8A6A4A22F_CustomAttributesCacheGenerator,
	CoroutineExtensions_t2E926427EDAB1AE4124D7B4C7ECA80C2C62E3336_CustomAttributesCacheGenerator,
	DateTimeExtensions_t6A5D20B8382B0B207849FC576D23AA2B9E8D228A_CustomAttributesCacheGenerator,
	DictionaryExtensions_t8809E869D7D42CA740EA25A4F8EB9280D1C0A94D_CustomAttributesCacheGenerator,
	FloatExtensions_tC1D69ACC51899B26B456B8D4B9EBD59D1FDB69D6_CustomAttributesCacheGenerator,
	GameObjectExtensions_t9CAB6FA68B5E68D39CC079EF641BB6D922DE6F22_CustomAttributesCacheGenerator,
	IntExtensions_tA94D6C4CF640582935395B9F371352AEDB8F1318_CustomAttributesCacheGenerator,
	ListExtensions_t1AE27B309243C7540DD8185472EC958FC2953B29_CustomAttributesCacheGenerator,
	ParticleSystemExtensions_t06BEB0CB36800B29681FCEB5B231786B1531E43B_CustomAttributesCacheGenerator,
	SpriteExtensions_t51D56CB1D5836CADD7C7317E6630B570AB79C44A_CustomAttributesCacheGenerator,
	StringExtensions_tC367E6F67B4C112531BF21341C582782806E4F69_CustomAttributesCacheGenerator,
	TimeExtensions_t86985F861458B7EC3F5B38A4810F388D5380D1BE_CustomAttributesCacheGenerator,
	TransformExtensions_t0663C8668959986C21AECDDB309DBF244F5B6014_CustomAttributesCacheGenerator,
	VectorExtensions_t317DEEAE473359EDB7F5D249CF83CF8BBD9F9802_CustomAttributesCacheGenerator,
	FPSWalkerEnhanced_t54D7B7F25F9BE895544D79E770EDD93B466DB0AB_CustomAttributesCacheGenerator,
	SmoothMouseLook_tEE1CC92D0DBE9D333976C6B9BBC6EEB6AB8AC7DA_CustomAttributesCacheGenerator,
	U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_CustomAttributesCacheGenerator,
	U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_CustomAttributesCacheGenerator,
	U3CGreedControllerU3Ed__23_tD199267145EA542A06DB4BE74C527EB42673B3D8_CustomAttributesCacheGenerator,
	U3CDifficultyApperU3Ed__24_t904C0FB3DB6C190E3DB3FCE00435363F1E101970_CustomAttributesCacheGenerator,
	U3CRotatorU3Ed__17_t51FCCC2D459A39620C3DC1DF763DAB8F5A256B6A_CustomAttributesCacheGenerator,
	U3CFallU3Ed__18_t165EC7F06440CB52F1578139076D6BA440E3C330_CustomAttributesCacheGenerator,
	U3CDelayedDieU3Ed__22_t13921E7880F014290B349242E2F41F1A80227638_CustomAttributesCacheGenerator,
	U3CLerperNumberU3Ed__11_t28CF8F99BC897162AE9B411D48413A6B85662D50_CustomAttributesCacheGenerator,
	U3CU3Ec_t1428115A37A81A4A4EA416136A8B924C3A57D2DE_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_0_t9A0B5962E13A7FFE7F5669E11A2AF53CD05CC83C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_tEBD52E4B85CC8898B0CBAC4D9EC3E40631F2E733_CustomAttributesCacheGenerator,
	RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator,
	U3CCoinsDropperU3Ed__16_tB255480E26513BDB1C6E8741FD8348B2C44B83EE_CustomAttributesCacheGenerator,
	U3CPropsSpawnerU3Ed__17_tFF2684BDB6250DFA569D66790A7747F407DE4815_CustomAttributesCacheGenerator,
	U3CRectLerperU3Ed__22_t1529E126C3ABC63AAA4C8A3C18CB37944708E770_CustomAttributesCacheGenerator,
	U3CInitCoreU3Ed__3_tDFD7C3A276A8679CE027A37454FC646A663895B5_CustomAttributesCacheGenerator,
	U3CManagersInitU3Ed__4_tFA7811EED864BC0384726559E3D01C81BFC920AA_CustomAttributesCacheGenerator,
	U3CRectLerperU3Ed__7_t627D2B2D7285C0085962BF78194CD59D57F049EA_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass11_0_t2C5F534CB7E996542C21936DC0E910BCF2E76DD8_CustomAttributesCacheGenerator,
	U3CDelayedExecuteRoutU3Ed__12_tB4B9F1609DA131FA9762A04E9878ED5B781C2144_CustomAttributesCacheGenerator,
	U3CLoadMenuU3Ed__28_tB58C1122E8D0ED148523461E8CD31F85CDD1D0D4_CustomAttributesCacheGenerator,
	U3CLoadBattleRoutU3Ed__29_tF474E0A73CD058CD0A109CD9822D8B23B25D88BC_CustomAttributesCacheGenerator,
	U3CLoadBundleU3Ed__34_t5883FB0ED07D57BAA2813F454550FAEFB16158C4_CustomAttributesCacheGenerator,
	U3CGetBundlesVersionInfoU3Ed__35_t723284E5DB9205EE817B2FFA5A0711AC94FD84B0_CustomAttributesCacheGenerator,
	U3CRunRoutU3Ed__30_tD0905ED021E61471FA6F363886A33A1AC7330457_CustomAttributesCacheGenerator,
	U3CSayHelloU3Ed__31_tE0422E39EEC3F88FBDEE3F3891BA2FA0933F935E_CustomAttributesCacheGenerator,
	U3CRotatorU3Ed__32_t42629F9D4CE344E0CEE7196E0647CB2BDC4369A5_CustomAttributesCacheGenerator,
	ETFXProjectileScript_tED63BF9EB347489D6D56FF6E6265BA4EC7457B03_CustomAttributesCacheGenerator_colliderRadius,
	ETFXProjectileScript_tED63BF9EB347489D6D56FF6E6265BA4EC7457B03_CustomAttributesCacheGenerator_collideOffset,
	ETFXFireProjectile_t13466BBD977B94CACB65A92CE5452EF030A99328_CustomAttributesCacheGenerator_projectiles,
	ETFXFireProjectile_t13466BBD977B94CACB65A92CE5452EF030A99328_CustomAttributesCacheGenerator_spawnPosition,
	ETFXFireProjectile_t13466BBD977B94CACB65A92CE5452EF030A99328_CustomAttributesCacheGenerator_currentProjectile,
	ETFXLoopScript_tF4DFDBD5AF30EE9D60CDB5D8E64685454AD7CA75_CustomAttributesCacheGenerator_spawnWithoutLight,
	ETFXTarget_tF0B3C5AAE0244F70D1701A4B14AED2D0A8F4B02F_CustomAttributesCacheGenerator_hitParticle,
	ETFXTarget_tF0B3C5AAE0244F70D1701A4B14AED2D0A8F4B02F_CustomAttributesCacheGenerator_respawnParticle,
	ETFXLightFade_t2C9509BEFEF0040FB2595BFE837F88894E570BCB_CustomAttributesCacheGenerator_life,
	ETFXRotation_tFAD3FD3D4A0D634146B68B89E9BBF223473BA881_CustomAttributesCacheGenerator_rotateVector,
	GreedBeh_t161E4AD27111A77E72F5EB4F6087221B35167544_CustomAttributesCacheGenerator_textHolder,
	GreedBeh_t161E4AD27111A77E72F5EB4F6087221B35167544_CustomAttributesCacheGenerator_text,
	GreedBeh_t161E4AD27111A77E72F5EB4F6087221B35167544_CustomAttributesCacheGenerator_fillAria,
	GreedBeh_t161E4AD27111A77E72F5EB4F6087221B35167544_CustomAttributesCacheGenerator_fillAriaImage,
	GreedBeh_t161E4AD27111A77E72F5EB4F6087221B35167544_CustomAttributesCacheGenerator_green,
	GreedBeh_t161E4AD27111A77E72F5EB4F6087221B35167544_CustomAttributesCacheGenerator_red,
	GreedBeh_t161E4AD27111A77E72F5EB4F6087221B35167544_CustomAttributesCacheGenerator_greedMaxLVL,
	GreedBeh_t161E4AD27111A77E72F5EB4F6087221B35167544_CustomAttributesCacheGenerator_greedMaxHP,
	GreedBeh_t161E4AD27111A77E72F5EB4F6087221B35167544_CustomAttributesCacheGenerator_timeToDifficultyUp,
	GreedBeh_t161E4AD27111A77E72F5EB4F6087221B35167544_CustomAttributesCacheGenerator_coinsPriceOnHpMult,
	CameraBeh_t20869EEE55151F514702E119C4FD6A91ACA81C15_CustomAttributesCacheGenerator_lerpVal,
	CameraBeh_t20869EEE55151F514702E119C4FD6A91ACA81C15_CustomAttributesCacheGenerator_player,
	CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_price,
	CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_fallLasting,
	CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_fallHight,
	CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_curve,
	CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_enableRotation,
	CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_rotationSpeed,
	CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_lifetime,
	CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_staticVfx,
	CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_collectVfx,
	CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_onCollisioVfx,
	CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_collisionPlayTimePoint,
	CounterBeh_t9616A7B7C30413FA3FEE01EC6752F51630E2490B_CustomAttributesCacheGenerator_holderRect,
	CounterBeh_t9616A7B7C30413FA3FEE01EC6752F51630E2490B_CustomAttributesCacheGenerator_value,
	CounterBeh_t9616A7B7C30413FA3FEE01EC6752F51630E2490B_CustomAttributesCacheGenerator_lerpLasting,
	CounterBeh_t9616A7B7C30413FA3FEE01EC6752F51630E2490B_CustomAttributesCacheGenerator_curve,
	ShadowBeh_t7FA3CA91153DC03624C8AC2CFE086E52208B33E4_CustomAttributesCacheGenerator_size,
	ShadowBeh_t7FA3CA91153DC03624C8AC2CFE086E52208B33E4_CustomAttributesCacheGenerator_shadow,
	ButtonBehaviour_tA8AD449AB299BAA40D5E8D4A51E69F8F581C32D1_CustomAttributesCacheGenerator_pingPongActive,
	CheckBoxBeh_tAC6E37B116D6534F7D76B8F761BA09C9ED620886_CustomAttributesCacheGenerator_animLength,
	CheckBoxBeh_tAC6E37B116D6534F7D76B8F761BA09C9ED620886_CustomAttributesCacheGenerator_On,
	CheckBoxBeh_tAC6E37B116D6534F7D76B8F761BA09C9ED620886_CustomAttributesCacheGenerator_Off,
	LogoClorsChanger_tB87C1458A459E43528CDFEB168040A64AAAF368A_CustomAttributesCacheGenerator_colors,
	LogoClorsChanger_tB87C1458A459E43528CDFEB168040A64AAAF368A_CustomAttributesCacheGenerator_text,
	LogoClorsChanger_tB87C1458A459E43528CDFEB168040A64AAAF368A_CustomAttributesCacheGenerator_rect,
	BattleManager_tBA2D4B3875E580ACB0E65449760F24CECAE8FC40_CustomAttributesCacheGenerator_player,
	BattleManager_tBA2D4B3875E580ACB0E65449760F24CECAE8FC40_CustomAttributesCacheGenerator_Coins,
	BattleManager_tBA2D4B3875E580ACB0E65449760F24CECAE8FC40_CustomAttributesCacheGenerator_CoinsRare,
	BattleManager_tBA2D4B3875E580ACB0E65449760F24CECAE8FC40_CustomAttributesCacheGenerator_props,
	BattleManager_tBA2D4B3875E580ACB0E65449760F24CECAE8FC40_CustomAttributesCacheGenerator_chanceForRare,
	BattleManager_tBA2D4B3875E580ACB0E65449760F24CECAE8FC40_CustomAttributesCacheGenerator_delayBetweenCooinSpawn,
	BattleManager_tBA2D4B3875E580ACB0E65449760F24CECAE8FC40_CustomAttributesCacheGenerator_delayBetweenPropSpawn,
	BattleManager_tBA2D4B3875E580ACB0E65449760F24CECAE8FC40_CustomAttributesCacheGenerator_widthToSpawnCoins,
	BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_mainPanel,
	BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_lerpCurve,
	BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_lerpLasting,
	BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_bestScore,
	BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_bestScore2,
	BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_bestScore2Descript,
	BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_resultScore,
	BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_resultScoreDescript,
	BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_resultText,
	BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_green,
	BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_resultWindow,
	BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_counter,
	BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_slider,
	BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_showWindowAnimDur,
	EventManager_t979564A250C00C4A14D6D8CD2B21B4EE72AD62E3_CustomAttributesCacheGenerator_U3CManagerLoadedU3Ek__BackingField,
	MenuManager_t4D94FC94EBCD4BC8FC4248EBC26B768A175333F1_CustomAttributesCacheGenerator_mainPanel,
	MenuManager_t4D94FC94EBCD4BC8FC4248EBC26B768A175333F1_CustomAttributesCacheGenerator_lerpCurve,
	MenuManager_t4D94FC94EBCD4BC8FC4248EBC26B768A175333F1_CustomAttributesCacheGenerator_lerpLasting,
	PoolManager_t884433F1D81180C1DFD89A0ED0F37F5746FED153_CustomAttributesCacheGenerator_U3CManagerLoadedU3Ek__BackingField,
	ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_canvas,
	ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_Panel,
	ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_LoadingImage,
	ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_dinoImage,
	ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_loadImage,
	ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_fadeInLasting,
	ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_fadeOutLasting,
	ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_LoadingText,
	ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_json_BundleInfoURL,
	ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_U3CManagerLoadedU3Ek__BackingField,
	SoundsPlayerManager_tE090F04B1A39F37D66CB73E0B0CD4BBF36E0D0E7_CustomAttributesCacheGenerator_AudioClipsArr,
	SoundsPlayerManager_tE090F04B1A39F37D66CB73E0B0CD4BBF36E0D0E7_CustomAttributesCacheGenerator_audioSourcesHolder,
	SoundsPlayerManager_tE090F04B1A39F37D66CB73E0B0CD4BBF36E0D0E7_CustomAttributesCacheGenerator_U3CManagerLoadedU3Ek__BackingField,
	SoundsPlayerManager_tE090F04B1A39F37D66CB73E0B0CD4BBF36E0D0E7_CustomAttributesCacheGenerator_U3CIsMusicOnU3Ek__BackingField,
	SoundsPlayerManager_tE090F04B1A39F37D66CB73E0B0CD4BBF36E0D0E7_CustomAttributesCacheGenerator_U3CIsSfxOnU3Ek__BackingField,
	PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_dieEffect,
	PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_collectEffect,
	PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_playerBody,
	PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_hats,
	PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_HatColors,
	PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_moveSpeed,
	PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_rotationSlowDown,
	PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_anim,
	PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_rotationLasting,
	PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_helloFrequency,
	ColorExtensions_t72892A69207663404B47720DD90667E8A6A4A22F_CustomAttributesCacheGenerator_ColorExtensions_ChangeAlpha_mC3CC7B4AE0E805D7101B9A31F686BA42DB3AF9FC,
	CoroutineExtensions_t2E926427EDAB1AE4124D7B4C7ECA80C2C62E3336_CustomAttributesCacheGenerator_CoroutineExtensions_End_m517E0787B1E601B3B2E985FA47A1690FB9C57202,
	DateTimeExtensions_t6A5D20B8382B0B207849FC576D23AA2B9E8D228A_CustomAttributesCacheGenerator_DateTimeExtensions_TimeStamp_mC3066A49B5AF458185559BBC314A323D68923E34,
	DictionaryExtensions_t8809E869D7D42CA740EA25A4F8EB9280D1C0A94D_CustomAttributesCacheGenerator_DictionaryExtensions_exKeysList_m15799EFECB4BB73F219417A85E43085B18A88794,
	DictionaryExtensions_t8809E869D7D42CA740EA25A4F8EB9280D1C0A94D_CustomAttributesCacheGenerator_DictionaryExtensions_exValsList_m6DE4210697AA6ECFCCBDC8CAB8C52C46AA32ABA1,
	DictionaryExtensions_t8809E869D7D42CA740EA25A4F8EB9280D1C0A94D_CustomAttributesCacheGenerator_DictionaryExtensions_exFirstPair_m94016A9AB7A82AD9D26EB70F0B24EC28802C4A42,
	DictionaryExtensions_t8809E869D7D42CA740EA25A4F8EB9280D1C0A94D_CustomAttributesCacheGenerator_DictionaryExtensions_exFirstKey_mFEAC204CBF5794658954941F95FB92CA6FC5A5AA,
	DictionaryExtensions_t8809E869D7D42CA740EA25A4F8EB9280D1C0A94D_CustomAttributesCacheGenerator_DictionaryExtensions_exFirstValue_m60711462BAAB56DE869364146093251B6943CC07,
	DictionaryExtensions_t8809E869D7D42CA740EA25A4F8EB9280D1C0A94D_CustomAttributesCacheGenerator_DictionaryExtensions_exRandomItem_mA929CF2A15196D188AE6E43DC81EE125017C0445,
	DictionaryExtensions_t8809E869D7D42CA740EA25A4F8EB9280D1C0A94D_CustomAttributesCacheGenerator_DictionaryExtensions_exRandomValue_m5DA4982DCD1ACAD359E8801F391C5406892D36D9,
	DictionaryExtensions_t8809E869D7D42CA740EA25A4F8EB9280D1C0A94D_CustomAttributesCacheGenerator_DictionaryExtensions_exRandomKey_m31739BD3C89EBB1FD87D227EBC5F97092EC68F21,
	FloatExtensions_tC1D69ACC51899B26B456B8D4B9EBD59D1FDB69D6_CustomAttributesCacheGenerator_FloatExtensions_Sign_m4FCF92814F87B927385F0B7C06A5253CBE787BFF,
	FloatExtensions_tC1D69ACC51899B26B456B8D4B9EBD59D1FDB69D6_CustomAttributesCacheGenerator_FloatExtensions_IsPositive_m56E710C581B36D7E5BB1C87D5B93B554B6E144B9,
	FloatExtensions_tC1D69ACC51899B26B456B8D4B9EBD59D1FDB69D6_CustomAttributesCacheGenerator_FloatExtensions_ToTimeString_m7476A4FFB42E9C3E1CC9FBB461948769AFA00897,
	GameObjectExtensions_t9CAB6FA68B5E68D39CC079EF641BB6D922DE6F22_CustomAttributesCacheGenerator_GameObjectExtensions_DestroyIfSingletonExist_m2057689DFF6902B1D0E38402274E19371EDA74CF,
	IntExtensions_tA94D6C4CF640582935395B9F371352AEDB8F1318_CustomAttributesCacheGenerator_IntExtensions_Sign_mEB374FC99F30A2BC1ACD044FD33B8DC406F9175A,
	IntExtensions_tA94D6C4CF640582935395B9F371352AEDB8F1318_CustomAttributesCacheGenerator_IntExtensions_IsPositive_mC99FA2A59F97A83B5636EE374566A000C3954F8B,
	ListExtensions_t1AE27B309243C7540DD8185472EC958FC2953B29_CustomAttributesCacheGenerator_ListExtensions_GetRndElement_m36ADAF78794F42C4A234EA803EA85E5A29FEBC63,
	ListExtensions_t1AE27B309243C7540DD8185472EC958FC2953B29_CustomAttributesCacheGenerator_ListExtensions_GetRandomItemAndRemove_m3006365AEA77521A687383AF79771696D880134A,
	ListExtensions_t1AE27B309243C7540DD8185472EC958FC2953B29_CustomAttributesCacheGenerator_ListExtensions_exDistinct_m6480814E7853C469D672251687AA5FA39669C219,
	ParticleSystemExtensions_t06BEB0CB36800B29681FCEB5B231786B1531E43B_CustomAttributesCacheGenerator_ParticleSystemExtensions_exFullStop_m611C3ADD0DD46034D10439A26365D53CF195FE0C,
	ParticleSystemExtensions_t06BEB0CB36800B29681FCEB5B231786B1531E43B_CustomAttributesCacheGenerator_ParticleSystemExtensions_exFullReset_m8CC35A22CD32BF1AC5FCA09D6D9990C2733E3035,
	ParticleSystemExtensions_t06BEB0CB36800B29681FCEB5B231786B1531E43B_CustomAttributesCacheGenerator_ParticleSystemExtensions_exFullClear_mC09E34569DC27BABFB2EE01E34A4E5D9761570F8,
	ParticleSystemExtensions_t06BEB0CB36800B29681FCEB5B231786B1531E43B_CustomAttributesCacheGenerator_ParticleSystemExtensions_exSetColor_m1711396E1D9B438D40258E2C02801EB5FE7DA7F9,
	ParticleSystemExtensions_t06BEB0CB36800B29681FCEB5B231786B1531E43B_CustomAttributesCacheGenerator_ParticleSystemExtensions_ResetAndPlay_m5E0F57B158F49C0DFFFDC6A09AA42723B675718B,
	ParticleSystemExtensions_t06BEB0CB36800B29681FCEB5B231786B1531E43B_CustomAttributesCacheGenerator_ParticleSystemExtensions_exFullClear_m8DE597433DED9D59E7AB1FA06B1BDA0371C2D71D,
	SpriteExtensions_t51D56CB1D5836CADD7C7317E6630B570AB79C44A_CustomAttributesCacheGenerator_SpriteExtensions_getUvOffset_mDCB6E2B56CB261598C275D2EEBF5A3CCB56712F3,
	StringExtensions_tC367E6F67B4C112531BF21341C582782806E4F69_CustomAttributesCacheGenerator_StringExtensions_Reverse_m921801DC6F50A3C8D3A5C50E2BD3E56A964D6806,
	TimeExtensions_t86985F861458B7EC3F5B38A4810F388D5380D1BE_CustomAttributesCacheGenerator_TimeExtensions_ToShortForm_m495F5B791C0C842B7C045D19BD4A28BA256F17DF,
	TimeExtensions_t86985F861458B7EC3F5B38A4810F388D5380D1BE_CustomAttributesCacheGenerator_TimeExtensions_ToMegaShortForm_m0552A35DB7EF0ECDFE80FC12082116AAC9B10180,
	TransformExtensions_t0663C8668959986C21AECDDB309DBF244F5B6014_CustomAttributesCacheGenerator_TransformExtensions_SelfDestroy_m37BF9588254B27591768734A57AE42637B6C2061,
	TransformExtensions_t0663C8668959986C21AECDDB309DBF244F5B6014_CustomAttributesCacheGenerator_TransformExtensions_DestroyChildren_mA936B8276E3B8037E2A0564903923F0039ECD9FE,
	TransformExtensions_t0663C8668959986C21AECDDB309DBF244F5B6014_CustomAttributesCacheGenerator_TransformExtensions_DestroyChildrenNow_m1B49213F608A5FF49CF742D4B2C1FD8C3D8AAD6B,
	TransformExtensions_t0663C8668959986C21AECDDB309DBF244F5B6014_CustomAttributesCacheGenerator_TransformExtensions_exContains_m86563D73F6C59BD08C28DD3D0740AE331E1F8EC6,
	TransformExtensions_t0663C8668959986C21AECDDB309DBF244F5B6014_CustomAttributesCacheGenerator_TransformExtensions_exContains_m35F876DB602247DCAE89E2230E0FAB83919BF0A4,
	TransformExtensions_t0663C8668959986C21AECDDB309DBF244F5B6014_CustomAttributesCacheGenerator_TransformExtensions_exIndex_m29F4E08C5007EDD88E1E04FF545BDA782FBAEAFF,
	VectorExtensions_t317DEEAE473359EDB7F5D249CF83CF8BBD9F9802_CustomAttributesCacheGenerator_VectorExtensions_ToV3_mEEC4EE674FCE50FF782B34554FAD934BA6A25089,
	VectorExtensions_t317DEEAE473359EDB7F5D249CF83CF8BBD9F9802_CustomAttributesCacheGenerator_VectorExtensions_ToV3_m96F471D1DAD72302AD1F22EBF54B52AD1213744F,
	VectorExtensions_t317DEEAE473359EDB7F5D249CF83CF8BBD9F9802_CustomAttributesCacheGenerator_VectorExtensions_Abs_m0C727BD6192E0F080F46BF5E3C17551347FBFF8F,
	VectorExtensions_t317DEEAE473359EDB7F5D249CF83CF8BBD9F9802_CustomAttributesCacheGenerator_VectorExtensions_Div_mA05A365EC50132940174346D7B03E0B09352CCB1,
	VectorExtensions_t317DEEAE473359EDB7F5D249CF83CF8BBD9F9802_CustomAttributesCacheGenerator_VectorExtensions_Mirror_m1C4157865EAACC117CE8503CFC6F1F4B95DF3C4E,
	VectorExtensions_t317DEEAE473359EDB7F5D249CF83CF8BBD9F9802_CustomAttributesCacheGenerator_VectorExtensions_SwapCoord_mB1C58FE21EC7D3F0E297309CAD6BAA5CF5C32FE8,
	VectorExtensions_t317DEEAE473359EDB7F5D249CF83CF8BBD9F9802_CustomAttributesCacheGenerator_VectorExtensions_SwapCoord_m8D37CE7CA45E4AC7833C7FE6B98E1772CD1AD58F,
	VectorExtensions_t317DEEAE473359EDB7F5D249CF83CF8BBD9F9802_CustomAttributesCacheGenerator_VectorExtensions_AddCoord_m03E0EF7A6F887BBE97A986B702C88256FC8FE184,
	VectorExtensions_t317DEEAE473359EDB7F5D249CF83CF8BBD9F9802_CustomAttributesCacheGenerator_VectorExtensions_Zero_m9FA2283836BE1DD565A1A3441E018E34F041D1FB,
	VectorExtensions_t317DEEAE473359EDB7F5D249CF83CF8BBD9F9802_CustomAttributesCacheGenerator_VectorExtensions_RndShft_m650D9A88BA0DC7D81A2846828B78B4B4741845A9,
	VectorExtensions_t317DEEAE473359EDB7F5D249CF83CF8BBD9F9802_CustomAttributesCacheGenerator_VectorExtensions_RndAbsShift_m54D3E249B07A759147A68FE962C1FF2F8D95C6AE,
	ETFXLoopScript_tF4DFDBD5AF30EE9D60CDB5D8E64685454AD7CA75_CustomAttributesCacheGenerator_ETFXLoopScript_EffectLoop_m728B76DFD989B681D2C93D7A88E9D76C9696CAF4,
	U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_CustomAttributesCacheGenerator_U3CEffectLoopU3Ed__6__ctor_m553F1C586BBB6C68EEB9710F62438A8EDCE136DA,
	U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_CustomAttributesCacheGenerator_U3CEffectLoopU3Ed__6_System_IDisposable_Dispose_m8142A69BF60253C0139648E9350176EB9EFD8BFA,
	U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_CustomAttributesCacheGenerator_U3CEffectLoopU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m93EE11485A4BA42D80F240C1703D673774889545,
	U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_CustomAttributesCacheGenerator_U3CEffectLoopU3Ed__6_System_Collections_IEnumerator_Reset_mB35745D308CDED6CAB69D52DEFC2D84AB6CC5896,
	U3CEffectLoopU3Ed__6_t25BAA433CE780795116A92A22C987A2CE9108F43_CustomAttributesCacheGenerator_U3CEffectLoopU3Ed__6_System_Collections_IEnumerator_get_Current_mB56FD34E205892C6B85E8FD18DAE68F19ECBDE2E,
	ETFXTarget_tF0B3C5AAE0244F70D1701A4B14AED2D0A8F4B02F_CustomAttributesCacheGenerator_ETFXTarget_Respawn_m679D0B0188B3DA281D2BE313E7212FC1D6F2B905,
	U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_CustomAttributesCacheGenerator_U3CRespawnU3Ed__7__ctor_m415513F665DE8F3777A3B21AF9C5B45A90CB1BF3,
	U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_CustomAttributesCacheGenerator_U3CRespawnU3Ed__7_System_IDisposable_Dispose_m63C5D414DB8D33EB86AFF6D5569B6C4E18C32615,
	U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_CustomAttributesCacheGenerator_U3CRespawnU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF70DB558C29B7FFE4A790553093711C3478A2C54,
	U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_CustomAttributesCacheGenerator_U3CRespawnU3Ed__7_System_Collections_IEnumerator_Reset_m37DFF9115E528EA7997B687DD51321AB843D9BE7,
	U3CRespawnU3Ed__7_t2F0FDE3C689F1226C7989D80298592F01E221CFC_CustomAttributesCacheGenerator_U3CRespawnU3Ed__7_System_Collections_IEnumerator_get_Current_mA84FD67FBB5DE784889C67146E66570FC2E5D80E,
	GreedBeh_t161E4AD27111A77E72F5EB4F6087221B35167544_CustomAttributesCacheGenerator_GreedBeh_GreedController_m9AD3972A3D4EDF41FE9B6BB5AAC75AF6071E14AD,
	GreedBeh_t161E4AD27111A77E72F5EB4F6087221B35167544_CustomAttributesCacheGenerator_GreedBeh_DifficultyApper_mD7A702FA04D913E24905B2B1082A39093F270381,
	U3CGreedControllerU3Ed__23_tD199267145EA542A06DB4BE74C527EB42673B3D8_CustomAttributesCacheGenerator_U3CGreedControllerU3Ed__23__ctor_mED24E9A036599767030BB969BE03320D52DE3BB8,
	U3CGreedControllerU3Ed__23_tD199267145EA542A06DB4BE74C527EB42673B3D8_CustomAttributesCacheGenerator_U3CGreedControllerU3Ed__23_System_IDisposable_Dispose_m778A80884DF8544DB4F7E374C965D73586794490,
	U3CGreedControllerU3Ed__23_tD199267145EA542A06DB4BE74C527EB42673B3D8_CustomAttributesCacheGenerator_U3CGreedControllerU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8C0F4A03686C2661E36FC7B7A700315ADC72348F,
	U3CGreedControllerU3Ed__23_tD199267145EA542A06DB4BE74C527EB42673B3D8_CustomAttributesCacheGenerator_U3CGreedControllerU3Ed__23_System_Collections_IEnumerator_Reset_m2114CC24AB04A2370B6F76FD9AE1FEB45F236F55,
	U3CGreedControllerU3Ed__23_tD199267145EA542A06DB4BE74C527EB42673B3D8_CustomAttributesCacheGenerator_U3CGreedControllerU3Ed__23_System_Collections_IEnumerator_get_Current_mACA549512D9667723F550D555D208986BF10C8AD,
	U3CDifficultyApperU3Ed__24_t904C0FB3DB6C190E3DB3FCE00435363F1E101970_CustomAttributesCacheGenerator_U3CDifficultyApperU3Ed__24__ctor_mBA4F037F96B2F476A714F6DBDC8B7A2CA90287BC,
	U3CDifficultyApperU3Ed__24_t904C0FB3DB6C190E3DB3FCE00435363F1E101970_CustomAttributesCacheGenerator_U3CDifficultyApperU3Ed__24_System_IDisposable_Dispose_m134F71DF2B1632B26105A2275EABFABCE0E632F3,
	U3CDifficultyApperU3Ed__24_t904C0FB3DB6C190E3DB3FCE00435363F1E101970_CustomAttributesCacheGenerator_U3CDifficultyApperU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C742F57DD6CF120F66DD900101944356EC7B002,
	U3CDifficultyApperU3Ed__24_t904C0FB3DB6C190E3DB3FCE00435363F1E101970_CustomAttributesCacheGenerator_U3CDifficultyApperU3Ed__24_System_Collections_IEnumerator_Reset_m3A9261AFA76DEFAEF6A962E88D9F46E5ACBEC7E1,
	U3CDifficultyApperU3Ed__24_t904C0FB3DB6C190E3DB3FCE00435363F1E101970_CustomAttributesCacheGenerator_U3CDifficultyApperU3Ed__24_System_Collections_IEnumerator_get_Current_m8690A70EEAC79F6399EE311D2B93DC5F7AA85198,
	CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_CoinBeh_Rotator_m40AA8668E10BFADA112DA289E78E735E46B7E4D5,
	CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_CoinBeh_Fall_m4200C6B1973602768A994122AFBAF516C305FDC7,
	CoinBeh_tDD249ECAAC4F636478EB2C97C487A04FD8983963_CustomAttributesCacheGenerator_CoinBeh_DelayedDie_mF36E21BA2FF53B40C20F22FAC0460B9972D72EAA,
	U3CRotatorU3Ed__17_t51FCCC2D459A39620C3DC1DF763DAB8F5A256B6A_CustomAttributesCacheGenerator_U3CRotatorU3Ed__17__ctor_m114D28EEABEDC896272B845F7A91892F68306FFE,
	U3CRotatorU3Ed__17_t51FCCC2D459A39620C3DC1DF763DAB8F5A256B6A_CustomAttributesCacheGenerator_U3CRotatorU3Ed__17_System_IDisposable_Dispose_m6315BB8051C0C7EE5D1625FC4E03031B2219CD05,
	U3CRotatorU3Ed__17_t51FCCC2D459A39620C3DC1DF763DAB8F5A256B6A_CustomAttributesCacheGenerator_U3CRotatorU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8A3D8CB7F45697A23FC98BAB9AA05B83C4A447CC,
	U3CRotatorU3Ed__17_t51FCCC2D459A39620C3DC1DF763DAB8F5A256B6A_CustomAttributesCacheGenerator_U3CRotatorU3Ed__17_System_Collections_IEnumerator_Reset_m81DCA7E77FC0A97396961845CDD5DCBA2F9C5154,
	U3CRotatorU3Ed__17_t51FCCC2D459A39620C3DC1DF763DAB8F5A256B6A_CustomAttributesCacheGenerator_U3CRotatorU3Ed__17_System_Collections_IEnumerator_get_Current_m9CE4E05E678854976D2A6843AE0B6262F41E8693,
	U3CFallU3Ed__18_t165EC7F06440CB52F1578139076D6BA440E3C330_CustomAttributesCacheGenerator_U3CFallU3Ed__18__ctor_mA1BD6BEE887A2EE42B90B2F75315E7E1FB4EC535,
	U3CFallU3Ed__18_t165EC7F06440CB52F1578139076D6BA440E3C330_CustomAttributesCacheGenerator_U3CFallU3Ed__18_System_IDisposable_Dispose_mF309B4C364F44A922728EA07BDA6CF9635CFA32F,
	U3CFallU3Ed__18_t165EC7F06440CB52F1578139076D6BA440E3C330_CustomAttributesCacheGenerator_U3CFallU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAC3583E0B3BBC249538DAFF1FC5D2A1D0F69BC0F,
	U3CFallU3Ed__18_t165EC7F06440CB52F1578139076D6BA440E3C330_CustomAttributesCacheGenerator_U3CFallU3Ed__18_System_Collections_IEnumerator_Reset_m9D7DEE6CEADC090BFB3D63C3937B4341E28F3A8D,
	U3CFallU3Ed__18_t165EC7F06440CB52F1578139076D6BA440E3C330_CustomAttributesCacheGenerator_U3CFallU3Ed__18_System_Collections_IEnumerator_get_Current_m1211A1CDA0C02D5916843A755AE7A032E0954ADE,
	U3CDelayedDieU3Ed__22_t13921E7880F014290B349242E2F41F1A80227638_CustomAttributesCacheGenerator_U3CDelayedDieU3Ed__22__ctor_m83D599148B986FE4A385354A2414B1B1F207E131,
	U3CDelayedDieU3Ed__22_t13921E7880F014290B349242E2F41F1A80227638_CustomAttributesCacheGenerator_U3CDelayedDieU3Ed__22_System_IDisposable_Dispose_mC5D8224A1F4D3929672FDDE8AD96BE060E09CAFA,
	U3CDelayedDieU3Ed__22_t13921E7880F014290B349242E2F41F1A80227638_CustomAttributesCacheGenerator_U3CDelayedDieU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAD32D4B0BCD495AC64F7C0E2B6B380AA6D349240,
	U3CDelayedDieU3Ed__22_t13921E7880F014290B349242E2F41F1A80227638_CustomAttributesCacheGenerator_U3CDelayedDieU3Ed__22_System_Collections_IEnumerator_Reset_m787F6488D83FA9BC2AD52E672155FEB4EA8658C7,
	U3CDelayedDieU3Ed__22_t13921E7880F014290B349242E2F41F1A80227638_CustomAttributesCacheGenerator_U3CDelayedDieU3Ed__22_System_Collections_IEnumerator_get_Current_mA3ED1713C218EF84A4DC28FB92BA6E398DF2C36C,
	CounterBeh_t9616A7B7C30413FA3FEE01EC6752F51630E2490B_CustomAttributesCacheGenerator_CounterBeh_LerperNumber_m599BE2B23127C037AA97382BA0BA024FFF2D3D2F,
	U3CLerperNumberU3Ed__11_t28CF8F99BC897162AE9B411D48413A6B85662D50_CustomAttributesCacheGenerator_U3CLerperNumberU3Ed__11__ctor_mD9FD4C2897BEC16FC1A9C2B18F6FE69619970BE8,
	U3CLerperNumberU3Ed__11_t28CF8F99BC897162AE9B411D48413A6B85662D50_CustomAttributesCacheGenerator_U3CLerperNumberU3Ed__11_System_IDisposable_Dispose_m6660869AC0EA25A327B028E9F253931B2072A96B,
	U3CLerperNumberU3Ed__11_t28CF8F99BC897162AE9B411D48413A6B85662D50_CustomAttributesCacheGenerator_U3CLerperNumberU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB9730D565D67363A535F32E2EA770B2950AF17ED,
	U3CLerperNumberU3Ed__11_t28CF8F99BC897162AE9B411D48413A6B85662D50_CustomAttributesCacheGenerator_U3CLerperNumberU3Ed__11_System_Collections_IEnumerator_Reset_m868765C8474E200FCC40548774D832A1BB02DD86,
	U3CLerperNumberU3Ed__11_t28CF8F99BC897162AE9B411D48413A6B85662D50_CustomAttributesCacheGenerator_U3CLerperNumberU3Ed__11_System_Collections_IEnumerator_get_Current_mB0792E3850208F01A09783A4C819C80F9558CF92,
	RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_SetDefaultScale_m756116B110805E0750BD50CFB5AC9EAD5FEB0E0C,
	RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_SetPivotAndAnchors_m3739A3FDBB6ECF890D2794F3A0D140C241EAD927,
	RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_GetSize_m5E15B6433CDE84F88039436390CC83E1D87EC4D9,
	RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_GetWidth_m65D963AF477887BF495B2F9CB66E81B921CA32F3,
	RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_GetHeight_m061AB7A3F469AD5BB5C432F69063FB29E198E3E0,
	RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_SetPositionOfPivot_mB94D62C0DA9123D1A5237B7F9352F36C40ECDCE9,
	RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_SetLeftBottomPosition_m9A969464222C09A18146A52B02C4A16646B12A54,
	RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_SetLeftTopPosition_m262E8022AE28161E22ED3442CF58FBB35909667B,
	RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_SetRightBottomPosition_m0E6A4FE54AD6CF0F22617AA9E942207757C1D64C,
	RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_SetRightTopPosition_mED1168AD12631EECFD0C863BD87E7430B5CF183C,
	RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_SetSize_mECA4608AA6FAE6B4F3955D38542102448BF50C7C,
	RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_SetWidth_mF6725AB65F1C2BB7355870EAE41A5A3BF5E16CF5,
	RectTransformExtensions_t397FD48CAF86AED6E3933755EC09B31FA83B5145_CustomAttributesCacheGenerator_RectTransformExtensions_SetHeight_m23A05DB16E19D8931FC02B61AD32D22B52ECEE83,
	BattleManager_tBA2D4B3875E580ACB0E65449760F24CECAE8FC40_CustomAttributesCacheGenerator_BattleManager_CoinsDropper_m394A526819130312751C5AB1A3730B358709802F,
	BattleManager_tBA2D4B3875E580ACB0E65449760F24CECAE8FC40_CustomAttributesCacheGenerator_BattleManager_PropsSpawner_mC5067D1A2AFFABEC2BFDD6FEBD7F92E0A275DB29,
	U3CCoinsDropperU3Ed__16_tB255480E26513BDB1C6E8741FD8348B2C44B83EE_CustomAttributesCacheGenerator_U3CCoinsDropperU3Ed__16__ctor_m5B20A37FCEFC5792A1ECBBE76109AAA212EDFE78,
	U3CCoinsDropperU3Ed__16_tB255480E26513BDB1C6E8741FD8348B2C44B83EE_CustomAttributesCacheGenerator_U3CCoinsDropperU3Ed__16_System_IDisposable_Dispose_m2513921149CA83BF9D4ED0949FB0A1CB476A73C4,
	U3CCoinsDropperU3Ed__16_tB255480E26513BDB1C6E8741FD8348B2C44B83EE_CustomAttributesCacheGenerator_U3CCoinsDropperU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mED52FFF71BA21C416E7500AE61BDD67E9BA46538,
	U3CCoinsDropperU3Ed__16_tB255480E26513BDB1C6E8741FD8348B2C44B83EE_CustomAttributesCacheGenerator_U3CCoinsDropperU3Ed__16_System_Collections_IEnumerator_Reset_m0D4F891AA49D23F2917BBE2D2C97BB9318997040,
	U3CCoinsDropperU3Ed__16_tB255480E26513BDB1C6E8741FD8348B2C44B83EE_CustomAttributesCacheGenerator_U3CCoinsDropperU3Ed__16_System_Collections_IEnumerator_get_Current_mF4B5A889FECEAB4450FCD66D115160011B90FF50,
	U3CPropsSpawnerU3Ed__17_tFF2684BDB6250DFA569D66790A7747F407DE4815_CustomAttributesCacheGenerator_U3CPropsSpawnerU3Ed__17__ctor_m0C53E3153219B6888EFF50E1FBEBDA78A1E9FE2B,
	U3CPropsSpawnerU3Ed__17_tFF2684BDB6250DFA569D66790A7747F407DE4815_CustomAttributesCacheGenerator_U3CPropsSpawnerU3Ed__17_System_IDisposable_Dispose_m5F2BA115EA1588C1E45604A9E7EE633291E6D742,
	U3CPropsSpawnerU3Ed__17_tFF2684BDB6250DFA569D66790A7747F407DE4815_CustomAttributesCacheGenerator_U3CPropsSpawnerU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD2BCDF6BD7EA85A558C6090E32A48DAFF313AB19,
	U3CPropsSpawnerU3Ed__17_tFF2684BDB6250DFA569D66790A7747F407DE4815_CustomAttributesCacheGenerator_U3CPropsSpawnerU3Ed__17_System_Collections_IEnumerator_Reset_m8670F90638804486CC750EBA42D27124A4A4F4D1,
	U3CPropsSpawnerU3Ed__17_tFF2684BDB6250DFA569D66790A7747F407DE4815_CustomAttributesCacheGenerator_U3CPropsSpawnerU3Ed__17_System_Collections_IEnumerator_get_Current_m54019008A86128E9F048DF85037C0518527FEB58,
	BattleUiManager_t7E7C0CF864A65C1ABFD94A4A5FA962D7AB6C1700_CustomAttributesCacheGenerator_BattleUiManager_RectLerper_m0381C4B0C57388EBA10CA3C1C8EDEAA217AB3EF8,
	U3CRectLerperU3Ed__22_t1529E126C3ABC63AAA4C8A3C18CB37944708E770_CustomAttributesCacheGenerator_U3CRectLerperU3Ed__22__ctor_mFE4BC35956BB1F5CAEB16BB2382C1C7BAB5FA335,
	U3CRectLerperU3Ed__22_t1529E126C3ABC63AAA4C8A3C18CB37944708E770_CustomAttributesCacheGenerator_U3CRectLerperU3Ed__22_System_IDisposable_Dispose_m3BA1315708803BD83C616C77D7A255BC598135F1,
	U3CRectLerperU3Ed__22_t1529E126C3ABC63AAA4C8A3C18CB37944708E770_CustomAttributesCacheGenerator_U3CRectLerperU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9E6376E59EBF96B900D203887FE32F6EE74A3AC7,
	U3CRectLerperU3Ed__22_t1529E126C3ABC63AAA4C8A3C18CB37944708E770_CustomAttributesCacheGenerator_U3CRectLerperU3Ed__22_System_Collections_IEnumerator_Reset_m0395461C746FA32266CEAB64C0CAF0B1F5D14219,
	U3CRectLerperU3Ed__22_t1529E126C3ABC63AAA4C8A3C18CB37944708E770_CustomAttributesCacheGenerator_U3CRectLerperU3Ed__22_System_Collections_IEnumerator_get_Current_m2F39E8D4723F7EA8CE96441AB4D337E0956F07A5,
	EventManager_t979564A250C00C4A14D6D8CD2B21B4EE72AD62E3_CustomAttributesCacheGenerator_EventManager_get_ManagerLoaded_m0CDF1ABA9D71D9FDF98A55DED0580ABE7BB09119,
	EventManager_t979564A250C00C4A14D6D8CD2B21B4EE72AD62E3_CustomAttributesCacheGenerator_EventManager_set_ManagerLoaded_mF465E0FC890BAFCD46E08A7B45C341CCFF51180C,
	GameCore_tBDD99C41346679D5795D34FAA7EA0D1916B358CA_CustomAttributesCacheGenerator_GameCore_InitCore_m8B8D858F49C6159AC07980638C0F6155DCFF0B3C,
	GameCore_tBDD99C41346679D5795D34FAA7EA0D1916B358CA_CustomAttributesCacheGenerator_GameCore_ManagersInit_m628635156AB71C4AB20CD389F9FA13204DA224C6,
	U3CInitCoreU3Ed__3_tDFD7C3A276A8679CE027A37454FC646A663895B5_CustomAttributesCacheGenerator_U3CInitCoreU3Ed__3__ctor_mCCAD943136895CF43DF40DAD28D6D05461041FA7,
	U3CInitCoreU3Ed__3_tDFD7C3A276A8679CE027A37454FC646A663895B5_CustomAttributesCacheGenerator_U3CInitCoreU3Ed__3_System_IDisposable_Dispose_m214566DFB3B6CC50D2C9CC9AD7B65452E2FDBCF9,
	U3CInitCoreU3Ed__3_tDFD7C3A276A8679CE027A37454FC646A663895B5_CustomAttributesCacheGenerator_U3CInitCoreU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF66EBC679A96E1B1F5BFF2DD05003DFA1A3C8BF7,
	U3CInitCoreU3Ed__3_tDFD7C3A276A8679CE027A37454FC646A663895B5_CustomAttributesCacheGenerator_U3CInitCoreU3Ed__3_System_Collections_IEnumerator_Reset_mE2AC84D59ACB0435151AC467DBC64F928500E0EA,
	U3CInitCoreU3Ed__3_tDFD7C3A276A8679CE027A37454FC646A663895B5_CustomAttributesCacheGenerator_U3CInitCoreU3Ed__3_System_Collections_IEnumerator_get_Current_mAAA0F1C1AC6138B672DC1A1CD62FC68E5CE1ED33,
	U3CManagersInitU3Ed__4_tFA7811EED864BC0384726559E3D01C81BFC920AA_CustomAttributesCacheGenerator_U3CManagersInitU3Ed__4__ctor_m9D17335FFA81B6E19CFAC5F36D195B5B6465EA49,
	U3CManagersInitU3Ed__4_tFA7811EED864BC0384726559E3D01C81BFC920AA_CustomAttributesCacheGenerator_U3CManagersInitU3Ed__4_System_IDisposable_Dispose_m3BC8E51F2662DD30EA6D7F7F5ECF3D3DA7599584,
	U3CManagersInitU3Ed__4_tFA7811EED864BC0384726559E3D01C81BFC920AA_CustomAttributesCacheGenerator_U3CManagersInitU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDBAA898997E6235197484255E05B6FF5E23EDAE8,
	U3CManagersInitU3Ed__4_tFA7811EED864BC0384726559E3D01C81BFC920AA_CustomAttributesCacheGenerator_U3CManagersInitU3Ed__4_System_Collections_IEnumerator_Reset_m2FBF32B1158983877657F90DEFDA128510E8FCA9,
	U3CManagersInitU3Ed__4_tFA7811EED864BC0384726559E3D01C81BFC920AA_CustomAttributesCacheGenerator_U3CManagersInitU3Ed__4_System_Collections_IEnumerator_get_Current_m00C0BEFE87BD041DAEEDC27CA297B07357197B3D,
	MenuManager_t4D94FC94EBCD4BC8FC4248EBC26B768A175333F1_CustomAttributesCacheGenerator_MenuManager_RectLerper_m673FD921A02A52C9AEEA52EBBB0BA25822612C27,
	U3CRectLerperU3Ed__7_t627D2B2D7285C0085962BF78194CD59D57F049EA_CustomAttributesCacheGenerator_U3CRectLerperU3Ed__7__ctor_m30B56C1B8F94B603CDD28B9094630DBCC58DBB37,
	U3CRectLerperU3Ed__7_t627D2B2D7285C0085962BF78194CD59D57F049EA_CustomAttributesCacheGenerator_U3CRectLerperU3Ed__7_System_IDisposable_Dispose_m07B7C2EF1F8B0D6485FC4D0AC507D3047424491C,
	U3CRectLerperU3Ed__7_t627D2B2D7285C0085962BF78194CD59D57F049EA_CustomAttributesCacheGenerator_U3CRectLerperU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0DE627B04D8C907BA9536B54F2DDCCA4E18CB2D5,
	U3CRectLerperU3Ed__7_t627D2B2D7285C0085962BF78194CD59D57F049EA_CustomAttributesCacheGenerator_U3CRectLerperU3Ed__7_System_Collections_IEnumerator_Reset_m087D959A04E82FA45CF5482C60AE988A0E886B50,
	U3CRectLerperU3Ed__7_t627D2B2D7285C0085962BF78194CD59D57F049EA_CustomAttributesCacheGenerator_U3CRectLerperU3Ed__7_System_Collections_IEnumerator_get_Current_mD7CE3F95967A694D0005D7CD0A7792718586875E,
	PoolManager_t884433F1D81180C1DFD89A0ED0F37F5746FED153_CustomAttributesCacheGenerator_PoolManager_get_ManagerLoaded_mEA8DC0C106DAFF98B4E44BE29BD05D25B898CFD6,
	PoolManager_t884433F1D81180C1DFD89A0ED0F37F5746FED153_CustomAttributesCacheGenerator_PoolManager_set_ManagerLoaded_mB1934AED08E82FA5E05C24928872827F339EB026,
	PoolManager_t884433F1D81180C1DFD89A0ED0F37F5746FED153_CustomAttributesCacheGenerator_PoolManager_DelayedExecuteRout_m925F863DF8679D4EAD358D9CEF05963B6DA83ADB,
	U3CDelayedExecuteRoutU3Ed__12_tB4B9F1609DA131FA9762A04E9878ED5B781C2144_CustomAttributesCacheGenerator_U3CDelayedExecuteRoutU3Ed__12__ctor_mD9E6417E2A02D10C7437F9EB77CE8D88F83AB8E8,
	U3CDelayedExecuteRoutU3Ed__12_tB4B9F1609DA131FA9762A04E9878ED5B781C2144_CustomAttributesCacheGenerator_U3CDelayedExecuteRoutU3Ed__12_System_IDisposable_Dispose_m9ACD873B22A38611DBA9C4ACBD113A473FE78DB8,
	U3CDelayedExecuteRoutU3Ed__12_tB4B9F1609DA131FA9762A04E9878ED5B781C2144_CustomAttributesCacheGenerator_U3CDelayedExecuteRoutU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDE80C087E891DEAEC694EA8BC3153EEAD3D96953,
	U3CDelayedExecuteRoutU3Ed__12_tB4B9F1609DA131FA9762A04E9878ED5B781C2144_CustomAttributesCacheGenerator_U3CDelayedExecuteRoutU3Ed__12_System_Collections_IEnumerator_Reset_mC12C650FE8CF3A3377E0CB43B7C950128D2666F0,
	U3CDelayedExecuteRoutU3Ed__12_tB4B9F1609DA131FA9762A04E9878ED5B781C2144_CustomAttributesCacheGenerator_U3CDelayedExecuteRoutU3Ed__12_System_Collections_IEnumerator_get_Current_m9C8E239CAC36693CAB41741151A0461C56FD1137,
	ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_ScenesManager_get_ManagerLoaded_m9853E6F6C20C9A0A8B8AE1CCB175DBC4A6CC1A7B,
	ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_ScenesManager_set_ManagerLoaded_m498FBC0E72C9775D90B33FB968326DDA1AE027BB,
	ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_ScenesManager_LoadMenu_mB5F53C0CBB07D0F8DCCD0582718698BE354350A9,
	ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_ScenesManager_LoadBattleRout_m6E134CC1BC70F1A493C32FE27BDE87D0B9796218,
	ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_ScenesManager_LoadBundle_m6E2FD6AEC587F263C243CE835383630A5C4A840D,
	ScenesManager_tAF4613044B04F1E0EA257ADD874A386E5ED84AEC_CustomAttributesCacheGenerator_ScenesManager_GetBundlesVersionInfo_m877050724519A0516789CF3A91D43C14F5CFC385,
	U3CLoadMenuU3Ed__28_tB58C1122E8D0ED148523461E8CD31F85CDD1D0D4_CustomAttributesCacheGenerator_U3CLoadMenuU3Ed__28__ctor_m83C8D766D0CA9BCE7DEC0052E90A6478B49DF083,
	U3CLoadMenuU3Ed__28_tB58C1122E8D0ED148523461E8CD31F85CDD1D0D4_CustomAttributesCacheGenerator_U3CLoadMenuU3Ed__28_System_IDisposable_Dispose_mCCA53E59ABB7B1BF4C661A5FEE7D434EC29D695A,
	U3CLoadMenuU3Ed__28_tB58C1122E8D0ED148523461E8CD31F85CDD1D0D4_CustomAttributesCacheGenerator_U3CLoadMenuU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD0B1FF8419F91341A03CF453DDF1D7BBA16C16AB,
	U3CLoadMenuU3Ed__28_tB58C1122E8D0ED148523461E8CD31F85CDD1D0D4_CustomAttributesCacheGenerator_U3CLoadMenuU3Ed__28_System_Collections_IEnumerator_Reset_mF54E18EE6E6C2D73AA2A862B2003F8A6FF3873BF,
	U3CLoadMenuU3Ed__28_tB58C1122E8D0ED148523461E8CD31F85CDD1D0D4_CustomAttributesCacheGenerator_U3CLoadMenuU3Ed__28_System_Collections_IEnumerator_get_Current_m8C521593D1D04B269A3554CC7C18CEB8E5C17F28,
	U3CLoadBattleRoutU3Ed__29_tF474E0A73CD058CD0A109CD9822D8B23B25D88BC_CustomAttributesCacheGenerator_U3CLoadBattleRoutU3Ed__29__ctor_m49F6DDD14DC52A8E2123C7F81BED86B23CB4A997,
	U3CLoadBattleRoutU3Ed__29_tF474E0A73CD058CD0A109CD9822D8B23B25D88BC_CustomAttributesCacheGenerator_U3CLoadBattleRoutU3Ed__29_System_IDisposable_Dispose_m75BE0F49721F852BAEEBCEC8EE7B1B74CBF57D76,
	U3CLoadBattleRoutU3Ed__29_tF474E0A73CD058CD0A109CD9822D8B23B25D88BC_CustomAttributesCacheGenerator_U3CLoadBattleRoutU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1BF2ABDA675CFC41535E484C711B664B0E3FE2F3,
	U3CLoadBattleRoutU3Ed__29_tF474E0A73CD058CD0A109CD9822D8B23B25D88BC_CustomAttributesCacheGenerator_U3CLoadBattleRoutU3Ed__29_System_Collections_IEnumerator_Reset_mEA0F3257C44B1B2EE12C96B014A43D9F5C06BA3C,
	U3CLoadBattleRoutU3Ed__29_tF474E0A73CD058CD0A109CD9822D8B23B25D88BC_CustomAttributesCacheGenerator_U3CLoadBattleRoutU3Ed__29_System_Collections_IEnumerator_get_Current_mC09B797D42E91CDDD03F19717E8BD5EFBD03573C,
	U3CLoadBundleU3Ed__34_t5883FB0ED07D57BAA2813F454550FAEFB16158C4_CustomAttributesCacheGenerator_U3CLoadBundleU3Ed__34__ctor_mA0E45B9A1CAC6DF82370452648B4E06B8C8D4AB2,
	U3CLoadBundleU3Ed__34_t5883FB0ED07D57BAA2813F454550FAEFB16158C4_CustomAttributesCacheGenerator_U3CLoadBundleU3Ed__34_System_IDisposable_Dispose_mD7EDC1DDC94917E540440B118748896313146141,
	U3CLoadBundleU3Ed__34_t5883FB0ED07D57BAA2813F454550FAEFB16158C4_CustomAttributesCacheGenerator_U3CLoadBundleU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC471A946990DDDC262A7554F9921392ED0015CB,
	U3CLoadBundleU3Ed__34_t5883FB0ED07D57BAA2813F454550FAEFB16158C4_CustomAttributesCacheGenerator_U3CLoadBundleU3Ed__34_System_Collections_IEnumerator_Reset_mC3E16195AF7E2154763F4D93431FE337E2BCF4F7,
	U3CLoadBundleU3Ed__34_t5883FB0ED07D57BAA2813F454550FAEFB16158C4_CustomAttributesCacheGenerator_U3CLoadBundleU3Ed__34_System_Collections_IEnumerator_get_Current_mDFF6DFD397416B0B367B626FC77730FB584084D0,
	U3CGetBundlesVersionInfoU3Ed__35_t723284E5DB9205EE817B2FFA5A0711AC94FD84B0_CustomAttributesCacheGenerator_U3CGetBundlesVersionInfoU3Ed__35__ctor_mDDE8FD2C504647E80E61EA74994A1088842B0AFB,
	U3CGetBundlesVersionInfoU3Ed__35_t723284E5DB9205EE817B2FFA5A0711AC94FD84B0_CustomAttributesCacheGenerator_U3CGetBundlesVersionInfoU3Ed__35_System_IDisposable_Dispose_m51692A5E82E73E57C977E6505ACFE136F76DDCEE,
	U3CGetBundlesVersionInfoU3Ed__35_t723284E5DB9205EE817B2FFA5A0711AC94FD84B0_CustomAttributesCacheGenerator_U3CGetBundlesVersionInfoU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m57156DE3AB27AB6D386535BCB919FFB23D356260,
	U3CGetBundlesVersionInfoU3Ed__35_t723284E5DB9205EE817B2FFA5A0711AC94FD84B0_CustomAttributesCacheGenerator_U3CGetBundlesVersionInfoU3Ed__35_System_Collections_IEnumerator_Reset_m09D18BFEF78670C3C13CD082C67704B45BB9B75D,
	U3CGetBundlesVersionInfoU3Ed__35_t723284E5DB9205EE817B2FFA5A0711AC94FD84B0_CustomAttributesCacheGenerator_U3CGetBundlesVersionInfoU3Ed__35_System_Collections_IEnumerator_get_Current_m5C57BB4F73F450C9DA8370F4F95A126E7ADF487D,
	SoundsPlayerManager_tE090F04B1A39F37D66CB73E0B0CD4BBF36E0D0E7_CustomAttributesCacheGenerator_SoundsPlayerManager_get_ManagerLoaded_m9414366FE26AB2E3DA2A51F41BF130FB941BD6DD,
	SoundsPlayerManager_tE090F04B1A39F37D66CB73E0B0CD4BBF36E0D0E7_CustomAttributesCacheGenerator_SoundsPlayerManager_set_ManagerLoaded_m19ECE5D36E48CADC44A635B447A3118F27CEACB2,
	SoundsPlayerManager_tE090F04B1A39F37D66CB73E0B0CD4BBF36E0D0E7_CustomAttributesCacheGenerator_SoundsPlayerManager_get_IsMusicOn_m1B4AE98BA33C0B42882EAE5DA37784ADDD1A85C7,
	SoundsPlayerManager_tE090F04B1A39F37D66CB73E0B0CD4BBF36E0D0E7_CustomAttributesCacheGenerator_SoundsPlayerManager_set_IsMusicOn_m76345AC43E617E1DF6892232BFE151BE108EF821,
	SoundsPlayerManager_tE090F04B1A39F37D66CB73E0B0CD4BBF36E0D0E7_CustomAttributesCacheGenerator_SoundsPlayerManager_get_IsSfxOn_m34E199685D320CE232180EAC17708945FB22ED31,
	SoundsPlayerManager_tE090F04B1A39F37D66CB73E0B0CD4BBF36E0D0E7_CustomAttributesCacheGenerator_SoundsPlayerManager_set_IsSfxOn_m454038EABCA58D8C1169C251EB29032758180BE4,
	PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_PlayerController_RunRout_mA22B2A74036B92E6C4B3B76B212981F7F44AE3D4,
	PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_PlayerController_SayHello_mA637BCB7DB14816CCBC35803E77F9CF1B5385C26,
	PlayerController_t188AC802CF20B9160D5464745B4FEF9832CFD915_CustomAttributesCacheGenerator_PlayerController_Rotator_m441BB00CC5F6329CAF42EE05CD76279368F9ED09,
	U3CRunRoutU3Ed__30_tD0905ED021E61471FA6F363886A33A1AC7330457_CustomAttributesCacheGenerator_U3CRunRoutU3Ed__30__ctor_mCA94540393D6FF23BFAEE95F33AC7F75D69CDAF9,
	U3CRunRoutU3Ed__30_tD0905ED021E61471FA6F363886A33A1AC7330457_CustomAttributesCacheGenerator_U3CRunRoutU3Ed__30_System_IDisposable_Dispose_mD9B16A132D4A399D11929531A6CE9AFBC63EA4EE,
	U3CRunRoutU3Ed__30_tD0905ED021E61471FA6F363886A33A1AC7330457_CustomAttributesCacheGenerator_U3CRunRoutU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m205313068F1B73E8EA33743E806702693E22766D,
	U3CRunRoutU3Ed__30_tD0905ED021E61471FA6F363886A33A1AC7330457_CustomAttributesCacheGenerator_U3CRunRoutU3Ed__30_System_Collections_IEnumerator_Reset_m5B8DA141B9C088A61F3302CB6C7F279A7E475A88,
	U3CRunRoutU3Ed__30_tD0905ED021E61471FA6F363886A33A1AC7330457_CustomAttributesCacheGenerator_U3CRunRoutU3Ed__30_System_Collections_IEnumerator_get_Current_m7EEC891E87CA1E9B2C39B4355D7F4553F0C9D0A8,
	U3CSayHelloU3Ed__31_tE0422E39EEC3F88FBDEE3F3891BA2FA0933F935E_CustomAttributesCacheGenerator_U3CSayHelloU3Ed__31__ctor_mA603E85A8DB0B4D7B67E720F7530EC6BBA9DDF7E,
	U3CSayHelloU3Ed__31_tE0422E39EEC3F88FBDEE3F3891BA2FA0933F935E_CustomAttributesCacheGenerator_U3CSayHelloU3Ed__31_System_IDisposable_Dispose_m23FC1D002C29FCD2BF7E5430B56A3D92BF2C3816,
	U3CSayHelloU3Ed__31_tE0422E39EEC3F88FBDEE3F3891BA2FA0933F935E_CustomAttributesCacheGenerator_U3CSayHelloU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m788DCE676AD27D2E45809A95C2D721FF39525713,
	U3CSayHelloU3Ed__31_tE0422E39EEC3F88FBDEE3F3891BA2FA0933F935E_CustomAttributesCacheGenerator_U3CSayHelloU3Ed__31_System_Collections_IEnumerator_Reset_m0CE46B566DA780F05F8FEC06127AB405182FA7E2,
	U3CSayHelloU3Ed__31_tE0422E39EEC3F88FBDEE3F3891BA2FA0933F935E_CustomAttributesCacheGenerator_U3CSayHelloU3Ed__31_System_Collections_IEnumerator_get_Current_mACA149E6143CEEC49B09738A94AEC131886D2F80,
	U3CRotatorU3Ed__32_t42629F9D4CE344E0CEE7196E0647CB2BDC4369A5_CustomAttributesCacheGenerator_U3CRotatorU3Ed__32__ctor_m48279ADDC8D8C49C9C9B8B818F1CA7CBAF0C1057,
	U3CRotatorU3Ed__32_t42629F9D4CE344E0CEE7196E0647CB2BDC4369A5_CustomAttributesCacheGenerator_U3CRotatorU3Ed__32_System_IDisposable_Dispose_mB23FB46D08F56888E6913A5D78182521004F0F81,
	U3CRotatorU3Ed__32_t42629F9D4CE344E0CEE7196E0647CB2BDC4369A5_CustomAttributesCacheGenerator_U3CRotatorU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0114479E05754C49ECF28B18186E2E2C8682AD31,
	U3CRotatorU3Ed__32_t42629F9D4CE344E0CEE7196E0647CB2BDC4369A5_CustomAttributesCacheGenerator_U3CRotatorU3Ed__32_System_Collections_IEnumerator_Reset_m1FF085920A616461360A3512B2185E8CE39381A3,
	U3CRotatorU3Ed__32_t42629F9D4CE344E0CEE7196E0647CB2BDC4369A5_CustomAttributesCacheGenerator_U3CRotatorU3Ed__32_System_Collections_IEnumerator_get_Current_mB4B770B1451A7086036EFAB0EC8056DFE025FAB5,
	EventManager_t979564A250C00C4A14D6D8CD2B21B4EE72AD62E3_CustomAttributesCacheGenerator_EventManager_ClearAllEventsBut_mDE1909B896E1E3A4B6F71B021849CB2A01EEA723____except0,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
