﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.Color ColorExtensions::ChangeAlpha(UnityEngine.Color,System.Single)
extern void ColorExtensions_ChangeAlpha_mC3CC7B4AE0E805D7101B9A31F686BA42DB3AF9FC (void);
// 0x00000002 System.Void CoroutineExtensions::End(UnityEngine.Coroutine,UnityEngine.Coroutine&,UnityEngine.MonoBehaviour)
extern void CoroutineExtensions_End_m517E0787B1E601B3B2E985FA47A1690FB9C57202 (void);
// 0x00000003 System.Int32 DateTimeExtensions::TimeStamp(System.DateTime)
extern void DateTimeExtensions_TimeStamp_mC3066A49B5AF458185559BBC314A323D68923E34 (void);
// 0x00000004 System.Collections.Generic.List`1<K> DictionaryExtensions::exKeysList(System.Collections.Generic.Dictionary`2<K,V>)
// 0x00000005 System.Collections.Generic.List`1<V> DictionaryExtensions::exValsList(System.Collections.Generic.Dictionary`2<K,V>)
// 0x00000006 System.Collections.Generic.KeyValuePair`2<K,V> DictionaryExtensions::exFirstPair(System.Collections.Generic.Dictionary`2<K,V>)
// 0x00000007 K DictionaryExtensions::exFirstKey(System.Collections.Generic.Dictionary`2<K,V>)
// 0x00000008 V DictionaryExtensions::exFirstValue(System.Collections.Generic.Dictionary`2<K,V>)
// 0x00000009 System.Collections.Generic.KeyValuePair`2<K,V> DictionaryExtensions::exRandomItem(System.Collections.Generic.Dictionary`2<K,V>)
// 0x0000000A V DictionaryExtensions::exRandomValue(System.Collections.Generic.Dictionary`2<K,V>)
// 0x0000000B K DictionaryExtensions::exRandomKey(System.Collections.Generic.Dictionary`2<K,V>)
// 0x0000000C System.Int32 FloatExtensions::Sign(System.Single)
extern void FloatExtensions_Sign_m4FCF92814F87B927385F0B7C06A5253CBE787BFF (void);
// 0x0000000D System.Boolean FloatExtensions::IsPositive(System.Single)
extern void FloatExtensions_IsPositive_m56E710C581B36D7E5BB1C87D5B93B554B6E144B9 (void);
// 0x0000000E System.String FloatExtensions::ToTimeString(System.Single)
extern void FloatExtensions_ToTimeString_m7476A4FFB42E9C3E1CC9FBB461948769AFA00897 (void);
// 0x0000000F System.Boolean GameObjectExtensions::DestroyIfSingletonExist(UnityEngine.MonoBehaviour)
extern void GameObjectExtensions_DestroyIfSingletonExist_m2057689DFF6902B1D0E38402274E19371EDA74CF (void);
// 0x00000010 System.Int32 IntExtensions::Sign(System.Int32)
extern void IntExtensions_Sign_mEB374FC99F30A2BC1ACD044FD33B8DC406F9175A (void);
// 0x00000011 System.Boolean IntExtensions::IsPositive(System.Int32)
extern void IntExtensions_IsPositive_mC99FA2A59F97A83B5636EE374566A000C3954F8B (void);
// 0x00000012 T ListExtensions::GetRndElement(System.Collections.Generic.List`1<T>)
// 0x00000013 T ListExtensions::GetRandomItemAndRemove(System.Collections.Generic.List`1<T>)
// 0x00000014 System.Collections.Generic.List`1<T> ListExtensions::exDistinct(System.Collections.Generic.List`1<T>)
// 0x00000015 System.Boolean ParticleSystemExtensions::exFullStop(UnityEngine.ParticleSystem[],System.Boolean)
extern void ParticleSystemExtensions_exFullStop_m611C3ADD0DD46034D10439A26365D53CF195FE0C (void);
// 0x00000016 System.Boolean ParticleSystemExtensions::exFullReset(UnityEngine.ParticleSystem[],System.Boolean)
extern void ParticleSystemExtensions_exFullReset_m8CC35A22CD32BF1AC5FCA09D6D9990C2733E3035 (void);
// 0x00000017 System.Boolean ParticleSystemExtensions::exFullClear(UnityEngine.ParticleSystem[],System.Boolean)
extern void ParticleSystemExtensions_exFullClear_mC09E34569DC27BABFB2EE01E34A4E5D9761570F8 (void);
// 0x00000018 System.Boolean ParticleSystemExtensions::exSetColor(UnityEngine.ParticleSystem[],UnityEngine.Color)
extern void ParticleSystemExtensions_exSetColor_m1711396E1D9B438D40258E2C02801EB5FE7DA7F9 (void);
// 0x00000019 System.Boolean ParticleSystemExtensions::ResetAndPlay(UnityEngine.ParticleSystem,System.Boolean)
extern void ParticleSystemExtensions_ResetAndPlay_m5E0F57B158F49C0DFFFDC6A09AA42723B675718B (void);
// 0x0000001A System.Boolean ParticleSystemExtensions::exFullClear(UnityEngine.ParticleSystem,System.Boolean)
extern void ParticleSystemExtensions_exFullClear_m8DE597433DED9D59E7AB1FA06B1BDA0371C2D71D (void);
// 0x0000001B UnityEngine.Vector4 SpriteExtensions::getUvOffset(UnityEngine.Sprite)
extern void SpriteExtensions_getUvOffset_mDCB6E2B56CB261598C275D2EEBF5A3CCB56712F3 (void);
// 0x0000001C System.String StringExtensions::Reverse(System.String)
extern void StringExtensions_Reverse_m921801DC6F50A3C8D3A5C50E2BD3E56A964D6806 (void);
// 0x0000001D System.String TimeExtensions::ToShortForm(System.TimeSpan)
extern void TimeExtensions_ToShortForm_m495F5B791C0C842B7C045D19BD4A28BA256F17DF (void);
// 0x0000001E System.String TimeExtensions::ToMegaShortForm(System.TimeSpan)
extern void TimeExtensions_ToMegaShortForm_m0552A35DB7EF0ECDFE80FC12082116AAC9B10180 (void);
// 0x0000001F System.Void TransformExtensions::SelfDestroy(UnityEngine.Transform)
extern void TransformExtensions_SelfDestroy_m37BF9588254B27591768734A57AE42637B6C2061 (void);
// 0x00000020 System.Void TransformExtensions::DestroyChildren(UnityEngine.Transform)
extern void TransformExtensions_DestroyChildren_mA936B8276E3B8037E2A0564903923F0039ECD9FE (void);
// 0x00000021 System.Void TransformExtensions::DestroyChildrenNow(UnityEngine.Transform)
extern void TransformExtensions_DestroyChildrenNow_m1B49213F608A5FF49CF742D4B2C1FD8C3D8AAD6B (void);
// 0x00000022 System.Boolean TransformExtensions::exContains(UnityEngine.Transform[],System.String)
extern void TransformExtensions_exContains_m86563D73F6C59BD08C28DD3D0740AE331E1F8EC6 (void);
// 0x00000023 System.Boolean TransformExtensions::exContains(System.Collections.Generic.List`1<UnityEngine.Transform>,System.String)
extern void TransformExtensions_exContains_m35F876DB602247DCAE89E2230E0FAB83919BF0A4 (void);
// 0x00000024 System.Int32 TransformExtensions::exIndex(System.Collections.Generic.List`1<UnityEngine.Transform>,System.String,System.Boolean)
extern void TransformExtensions_exIndex_m29F4E08C5007EDD88E1E04FF545BDA782FBAEAFF (void);
// 0x00000025 System.Single VectorExtensions::Distance(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void VectorExtensions_Distance_mE7B73E7D665A6C4EF406964DDF122A5C6D406F82 (void);
// 0x00000026 UnityEngine.Vector3 VectorExtensions::ToV3(UnityEngine.Vector2)
extern void VectorExtensions_ToV3_mEEC4EE674FCE50FF782B34554FAD934BA6A25089 (void);
// 0x00000027 UnityEngine.Vector3 VectorExtensions::ToV3(UnityEngine.Vector2Int)
extern void VectorExtensions_ToV3_m96F471D1DAD72302AD1F22EBF54B52AD1213744F (void);
// 0x00000028 UnityEngine.Vector3 VectorExtensions::Abs(UnityEngine.Vector3)
extern void VectorExtensions_Abs_m0C727BD6192E0F080F46BF5E3C17551347FBFF8F (void);
// 0x00000029 UnityEngine.Vector3 VectorExtensions::Div(UnityEngine.Vector3,UnityEngine.Vector3)
extern void VectorExtensions_Div_mA05A365EC50132940174346D7B03E0B09352CCB1 (void);
// 0x0000002A UnityEngine.Vector3 VectorExtensions::RndDirection(System.Single,System.Int32,System.Single)
extern void VectorExtensions_RndDirection_m0E294BA5C43072D8FC38230852E58506B2C2BC37 (void);
// 0x0000002B UnityEngine.Vector3 VectorExtensions::RndDir()
extern void VectorExtensions_RndDir_mF1C0701C304D402D59BBDD060591B2E2FF3A5316 (void);
// 0x0000002C UnityEngine.Vector3 VectorExtensions::Mirror(UnityEngine.Vector3,System.Boolean,System.Boolean,System.Boolean)
extern void VectorExtensions_Mirror_m1C4157865EAACC117CE8503CFC6F1F4B95DF3C4E (void);
// 0x0000002D UnityEngine.Vector3 VectorExtensions::SwapCoord(UnityEngine.Vector3,UnityEngine.Vector3,System.Boolean,System.Boolean,System.Boolean)
extern void VectorExtensions_SwapCoord_mB1C58FE21EC7D3F0E297309CAD6BAA5CF5C32FE8 (void);
// 0x0000002E UnityEngine.Vector3 VectorExtensions::SwapCoord(UnityEngine.Vector3,System.Single,System.Single,System.Single)
extern void VectorExtensions_SwapCoord_m8D37CE7CA45E4AC7833C7FE6B98E1772CD1AD58F (void);
// 0x0000002F UnityEngine.Vector3 VectorExtensions::AddCoord(UnityEngine.Vector3,System.Single,System.Single,System.Single)
extern void VectorExtensions_AddCoord_m03E0EF7A6F887BBE97A986B702C88256FC8FE184 (void);
// 0x00000030 UnityEngine.Vector3 VectorExtensions::Zero(UnityEngine.Vector3,System.Boolean,System.Boolean,System.Boolean)
extern void VectorExtensions_Zero_m9FA2283836BE1DD565A1A3441E018E34F041D1FB (void);
// 0x00000031 UnityEngine.Vector3 VectorExtensions::RndShft(UnityEngine.Vector3,System.Single)
extern void VectorExtensions_RndShft_m650D9A88BA0DC7D81A2846828B78B4B4741845A9 (void);
// 0x00000032 UnityEngine.Vector3 VectorExtensions::RndAbsShift(UnityEngine.Vector3,System.Single)
extern void VectorExtensions_RndAbsShift_m54D3E249B07A759147A68FE962C1FF2F8D95C6AE (void);
// 0x00000033 System.Void ShadowRotator::Start()
extern void ShadowRotator_Start_mCA1719DD579ED8AA668E58EFC5E88700B671DC75 (void);
// 0x00000034 System.Void ShadowRotator::Update()
extern void ShadowRotator_Update_m4B05DFCDA3699F109B4961F1EBE636B5FEBC8287 (void);
// 0x00000035 System.Void ShadowRotator::.ctor()
extern void ShadowRotator__ctor_m65C7AEB3D8CB910461C5CEB05968F4891A7BA63C (void);
// 0x00000036 System.Void GameHandler::Start()
extern void GameHandler_Start_m78B61BE18B716B2039EAE293F99DFD5CC0219FF2 (void);
// 0x00000037 System.Void GameHandler::Update()
extern void GameHandler_Update_m5966CB0A43519F7047D8E110AF3599540F44620B (void);
// 0x00000038 System.Void GameHandler::.ctor()
extern void GameHandler__ctor_m99B62501A237CFBFE117E822D074F35D036E79A2 (void);
// 0x00000039 System.Void ScreenshotHandler::Awake()
extern void ScreenshotHandler_Awake_m481871FBCF725B56BF967CCD0C40AD1E1B06AE95 (void);
// 0x0000003A System.Void ScreenshotHandler::OnPostRender()
extern void ScreenshotHandler_OnPostRender_m5A70CDB1F59DCE3A932BACE11D997CF8A0E653F0 (void);
// 0x0000003B System.Void ScreenshotHandler::TakeScreenshot(System.Int32,System.Int32)
extern void ScreenshotHandler_TakeScreenshot_m659DD76068C15BF7DAB31C562476BBC5E57966B2 (void);
// 0x0000003C System.Void ScreenshotHandler::TakeScreenshot_Static(System.Int32,System.Int32)
extern void ScreenshotHandler_TakeScreenshot_Static_m965ADA18BD02B605FF093ADE55C48E2092B7DFA3 (void);
// 0x0000003D System.Void ScreenshotHandler::.ctor()
extern void ScreenshotHandler__ctor_mCA9D5ABB96208972B55127BB416A1B30A5775D97 (void);
// 0x0000003E System.Void examplescene::Start()
extern void examplescene_Start_mF772F5209BBADE7B1564528A598B9AE0E8521E56 (void);
// 0x0000003F System.Void examplescene::Update()
extern void examplescene_Update_m80C52A0676D6A51B393E4B51CE620AE1C568CEA6 (void);
// 0x00000040 System.Void examplescene::OnGUI()
extern void examplescene_OnGUI_m451FA489D413D0A793B8227D22D5D9A10C3E9669 (void);
// 0x00000041 System.Void examplescene::.ctor()
extern void examplescene__ctor_m8813A9F768BE489C957DE34046DCA616E4F7B1C9 (void);
// 0x00000042 System.Void ETFXProjectileScript::Start()
extern void ETFXProjectileScript_Start_mE8756011C883DDCF287343C6BF881B36B7ADB59C (void);
// 0x00000043 System.Void ETFXProjectileScript::FixedUpdate()
extern void ETFXProjectileScript_FixedUpdate_m959BFE684CC8CF77305ABF0C54256007023E2FFD (void);
// 0x00000044 System.Void ETFXProjectileScript::.ctor()
extern void ETFXProjectileScript__ctor_mDB3BFAED1E91A0DC99930E80169BF2F1EE26CFB5 (void);
// 0x00000045 System.Void ETFXSceneManager::LoadScene2DDemo()
extern void ETFXSceneManager_LoadScene2DDemo_m11E37C513C423B288C88C336A3F77E846AA6B358 (void);
// 0x00000046 System.Void ETFXSceneManager::LoadSceneCards()
extern void ETFXSceneManager_LoadSceneCards_m2374101144F9C56EB968B3A3B7BEACA75BB21C03 (void);
// 0x00000047 System.Void ETFXSceneManager::LoadSceneCombat()
extern void ETFXSceneManager_LoadSceneCombat_mDA5B85860C1746C93E4D81CE992E7C4887D89E0C (void);
// 0x00000048 System.Void ETFXSceneManager::LoadSceneDecals()
extern void ETFXSceneManager_LoadSceneDecals_m0D919CCFC190AAB40C20D990A8DEBBDF79075F2E (void);
// 0x00000049 System.Void ETFXSceneManager::LoadSceneDecals2()
extern void ETFXSceneManager_LoadSceneDecals2_mEFA6A5777999D5F3D87E1E8226A361880433DCF6 (void);
// 0x0000004A System.Void ETFXSceneManager::LoadSceneEmojis()
extern void ETFXSceneManager_LoadSceneEmojis_m01E3A7533C8A4D8CF6115E0EE1DC687455AB4D4E (void);
// 0x0000004B System.Void ETFXSceneManager::LoadSceneEmojis2()
extern void ETFXSceneManager_LoadSceneEmojis2_m6005349247D781727A558BA01D2085C893AF6438 (void);
// 0x0000004C System.Void ETFXSceneManager::LoadSceneExplosions()
extern void ETFXSceneManager_LoadSceneExplosions_m1984686B3E5CE79E49BDDA11BDD09596F8E7C42E (void);
// 0x0000004D System.Void ETFXSceneManager::LoadSceneExplosions2()
extern void ETFXSceneManager_LoadSceneExplosions2_mB04546364A96740B6CA14B50B2DCC3E80C28CF90 (void);
// 0x0000004E System.Void ETFXSceneManager::LoadSceneFire()
extern void ETFXSceneManager_LoadSceneFire_m9E31C2B57B979A46E2C6F727C858947CF9C0EA19 (void);
// 0x0000004F System.Void ETFXSceneManager::LoadSceneFire2()
extern void ETFXSceneManager_LoadSceneFire2_mEAE89BF11E50292B18F69A327923ED3A8B652256 (void);
// 0x00000050 System.Void ETFXSceneManager::LoadSceneFire3()
extern void ETFXSceneManager_LoadSceneFire3_mD9919F751EC9CFEF0D95691414A042A22B55C831 (void);
// 0x00000051 System.Void ETFXSceneManager::LoadSceneFireworks()
extern void ETFXSceneManager_LoadSceneFireworks_m2827DAD7FCC3996841900263F223F5201E8A3C43 (void);
// 0x00000052 System.Void ETFXSceneManager::LoadSceneFlares()
extern void ETFXSceneManager_LoadSceneFlares_mD740C35453DD10F374658441ED58411126845D38 (void);
// 0x00000053 System.Void ETFXSceneManager::LoadSceneMagic()
extern void ETFXSceneManager_LoadSceneMagic_m225C27B1375FCB1B4C215A3D67BDF33C01BDE04E (void);
// 0x00000054 System.Void ETFXSceneManager::LoadSceneMagic2()
extern void ETFXSceneManager_LoadSceneMagic2_m08E7D2EF64DD9F9534B7525E3905324264DCAAC7 (void);
// 0x00000055 System.Void ETFXSceneManager::LoadSceneMagic3()
extern void ETFXSceneManager_LoadSceneMagic3_m58FC3634AA94E283420B22E080D9E91A2A52A737 (void);
// 0x00000056 System.Void ETFXSceneManager::LoadSceneMainDemo()
extern void ETFXSceneManager_LoadSceneMainDemo_m030D3019FECBBAF948E3FAC03B7229FAAF0839C4 (void);
// 0x00000057 System.Void ETFXSceneManager::LoadSceneMissiles()
extern void ETFXSceneManager_LoadSceneMissiles_mEE0267B462F61EC2B7BD88460F69CEE7139A6E8D (void);
// 0x00000058 System.Void ETFXSceneManager::LoadScenePortals()
extern void ETFXSceneManager_LoadScenePortals_m4932DCDC59F52F6B75F7E00FE384C15CAACD4371 (void);
// 0x00000059 System.Void ETFXSceneManager::LoadScenePortals2()
extern void ETFXSceneManager_LoadScenePortals2_mE5701BAD231A3926A5F3DD95E7FB029E9378C89C (void);
// 0x0000005A System.Void ETFXSceneManager::LoadScenePowerups()
extern void ETFXSceneManager_LoadScenePowerups_m49AE6A1640136507D16DFF611DC78B4CDA28FEC2 (void);
// 0x0000005B System.Void ETFXSceneManager::LoadScenePowerups2()
extern void ETFXSceneManager_LoadScenePowerups2_m548B7BFF09B8A133EF48FE2B130A91D796EC6A33 (void);
// 0x0000005C System.Void ETFXSceneManager::LoadSceneSparkles()
extern void ETFXSceneManager_LoadSceneSparkles_m9463E60A6ED35D9897EEEF18D7B94880359B75A5 (void);
// 0x0000005D System.Void ETFXSceneManager::LoadSceneSwordCombat()
extern void ETFXSceneManager_LoadSceneSwordCombat_m2040FBE4FE83D2256F5208F20D5A0A817472052E (void);
// 0x0000005E System.Void ETFXSceneManager::LoadSceneSwordCombat2()
extern void ETFXSceneManager_LoadSceneSwordCombat2_m3C8D0447A800B6C4616D4CCE669711CC6236C13A (void);
// 0x0000005F System.Void ETFXSceneManager::LoadSceneMoney()
extern void ETFXSceneManager_LoadSceneMoney_mE3B3B3433BE6C2E37B56CCF7CF0ACA0AF274B795 (void);
// 0x00000060 System.Void ETFXSceneManager::LoadSceneHealing()
extern void ETFXSceneManager_LoadSceneHealing_m73F368066D2E63E359E36079EB20A4B51006BA56 (void);
// 0x00000061 System.Void ETFXSceneManager::LoadSceneWind()
extern void ETFXSceneManager_LoadSceneWind_mB2FF2390377FF263B5ACAEE93DD389C9D666BE2E (void);
// 0x00000062 System.Void ETFXSceneManager::Update()
extern void ETFXSceneManager_Update_mB2FB561ADC4764627A10DECF88F6C3F17891B9C1 (void);
// 0x00000063 System.Void ETFXSceneManager::.ctor()
extern void ETFXSceneManager__ctor_mC6363CFDCD6B6646152AA5FEC471088DFC20367A (void);
// 0x00000064 System.Void PEButtonScript::Start()
extern void PEButtonScript_Start_mED60465004404B45D9A5BE8129015C39394C9DC0 (void);
// 0x00000065 System.Void PEButtonScript::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void PEButtonScript_OnPointerEnter_m89340DB88B2D0EF0CDD762F27C1EB455A6876CB6 (void);
// 0x00000066 System.Void PEButtonScript::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void PEButtonScript_OnPointerExit_m8F731E714619F43F056DD2D4E27A24B5B045774C (void);
// 0x00000067 System.Void PEButtonScript::OnButtonClicked()
extern void PEButtonScript_OnButtonClicked_m8D7DB17EB5C69D54E2F95931AB6C90A7490159B2 (void);
// 0x00000068 System.Void PEButtonScript::.ctor()
extern void PEButtonScript__ctor_mBFA614FD383BF4104DE7ADEE7AD88A91AC8083D6 (void);
// 0x00000069 System.Void ParticleEffectsLibrary::Awake()
extern void ParticleEffectsLibrary_Awake_mDC6AFDB7CD4E3DA4C5163BFDDCC04A76EED147CE (void);
// 0x0000006A System.Void ParticleEffectsLibrary::Start()
extern void ParticleEffectsLibrary_Start_m82B0B1F30A8C0E6B1060FEEC21C5280500D7F771 (void);
// 0x0000006B System.String ParticleEffectsLibrary::GetCurrentPENameString()
extern void ParticleEffectsLibrary_GetCurrentPENameString_m067B9C792580A9DFD2DE2B4318AEC99254BF5285 (void);
// 0x0000006C System.Void ParticleEffectsLibrary::PreviousParticleEffect()
extern void ParticleEffectsLibrary_PreviousParticleEffect_m4A6BB63833B9C4D91B2F2801741C731C8174836E (void);
// 0x0000006D System.Void ParticleEffectsLibrary::NextParticleEffect()
extern void ParticleEffectsLibrary_NextParticleEffect_mD095F239D4054E69278390CA3A404D033F8A658E (void);
// 0x0000006E System.Void ParticleEffectsLibrary::SpawnParticleEffect(UnityEngine.Vector3)
extern void ParticleEffectsLibrary_SpawnParticleEffect_m17AE1DA6F799AFF7DE1C64DC0ECF3D716E22355A (void);
// 0x0000006F System.Void ParticleEffectsLibrary::.ctor()
extern void ParticleEffectsLibrary__ctor_m777070C2175441BEB93AFF11748BC74588651C7B (void);
// 0x00000070 System.Void UICanvasManager::Awake()
extern void UICanvasManager_Awake_m8D84AB004D6FEEBB3BDC5FBB9C5101B75ACDC396 (void);
// 0x00000071 System.Void UICanvasManager::Start()
extern void UICanvasManager_Start_m9652DFED3C5A6CD686ABFB2D77A474270D5F7656 (void);
// 0x00000072 System.Void UICanvasManager::Update()
extern void UICanvasManager_Update_mA2771AB72770EC84D9D24FA819E57BB122AD7FC0 (void);
// 0x00000073 System.Void UICanvasManager::UpdateToolTip(ButtonTypes)
extern void UICanvasManager_UpdateToolTip_m13510C3590867DC2E5B2F8D45E0BA522479103D7 (void);
// 0x00000074 System.Void UICanvasManager::ClearToolTip()
extern void UICanvasManager_ClearToolTip_mFA69FDBDB5CADD0F18A31D1AC407D38D81F2B3BF (void);
// 0x00000075 System.Void UICanvasManager::SelectPreviousPE()
extern void UICanvasManager_SelectPreviousPE_m88ABF3CB63D07DBBEFCAE908864103FF09C21E30 (void);
// 0x00000076 System.Void UICanvasManager::SelectNextPE()
extern void UICanvasManager_SelectNextPE_m5838FA28DBEEC3DFF2A84E671842B86C366A2367 (void);
// 0x00000077 System.Void UICanvasManager::SpawnCurrentParticleEffect()
extern void UICanvasManager_SpawnCurrentParticleEffect_m8E4B86D6B40EBB353D2EA0D33B22785FB3A55B1B (void);
// 0x00000078 System.Void UICanvasManager::UIButtonClick(ButtonTypes)
extern void UICanvasManager_UIButtonClick_m0BE04EA2C0BA8E35CE5B22BFF4CBD4E9128B3CE4 (void);
// 0x00000079 System.Void UICanvasManager::.ctor()
extern void UICanvasManager__ctor_m8BDA3D4D6FD64344E6CCBEF6F7B314DEE7180E22 (void);
// 0x0000007A System.Void FPSWalkerEnhanced::Start()
extern void FPSWalkerEnhanced_Start_m252E88E0B24056C673BB14447377688734E7B8E2 (void);
// 0x0000007B System.Void FPSWalkerEnhanced::FixedUpdate()
extern void FPSWalkerEnhanced_FixedUpdate_m1473D52C9F5E268E25165FBE5F518E164748B3B1 (void);
// 0x0000007C System.Void FPSWalkerEnhanced::Update()
extern void FPSWalkerEnhanced_Update_mEA0CA073B5D3809A37EAB7C2281CF734F5531DC6 (void);
// 0x0000007D System.Void FPSWalkerEnhanced::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern void FPSWalkerEnhanced_OnControllerColliderHit_m19912E41ADB24963D0B31F1CC7DA6478C303073A (void);
// 0x0000007E System.Void FPSWalkerEnhanced::FallingDamageAlert(System.Single)
extern void FPSWalkerEnhanced_FallingDamageAlert_mB445332F8682AFBDEBCF35666278BD745299E23F (void);
// 0x0000007F System.Void FPSWalkerEnhanced::.ctor()
extern void FPSWalkerEnhanced__ctor_m1D5E7200BD8BEDDC09FDE08D0BB6B91D51BC7AFB (void);
// 0x00000080 System.Void SmoothMouseLook::Update()
extern void SmoothMouseLook_Update_m981BA5E8AB3B079E3B340A5051950642F3E1A109 (void);
// 0x00000081 System.Void SmoothMouseLook::Start()
extern void SmoothMouseLook_Start_m94730D34E9DBB8830F51F6E3DE68A50B80C40DC8 (void);
// 0x00000082 System.Single SmoothMouseLook::ClampAngle(System.Single,System.Single,System.Single)
extern void SmoothMouseLook_ClampAngle_mEE917D4C3904EB31D4DDDD84D0FF1F216990C0FC (void);
// 0x00000083 System.Void SmoothMouseLook::.ctor()
extern void SmoothMouseLook__ctor_m41C510FC83836FF572D9767E8F8433B8BAB341D7 (void);
// 0x00000084 System.Void SkyboxChanger::Awake()
extern void SkyboxChanger_Awake_m277CAAE5694E0F626BC667F43F17F9937A5CE0F8 (void);
// 0x00000085 System.Void SkyboxChanger::ChangeSkybox()
extern void SkyboxChanger_ChangeSkybox_mDDFA30D0CA245B27A82D0E3C499725B0C4A2A7A8 (void);
// 0x00000086 System.Void SkyboxChanger::.ctor()
extern void SkyboxChanger__ctor_mF2042E40C757632172F960BB139550F779D36080 (void);
// 0x00000087 System.Void SkyboxRotator::Update()
extern void SkyboxRotator_Update_m2E2891AA030A8BD0F61C1FDEF10D1E1C161C6DDE (void);
// 0x00000088 System.Void SkyboxRotator::ToggleSkyboxRotation()
extern void SkyboxRotator_ToggleSkyboxRotation_mCB6825B6E7E9FE18DF218AC2306523E747BCBC5F (void);
// 0x00000089 System.Void SkyboxRotator::.ctor()
extern void SkyboxRotator__ctor_m7BFD040FB7F3C18ABA7AED90C283AC89B46D7912 (void);
// 0x0000008A System.Void EpicToonFX.ETFXButtonScript::Start()
extern void ETFXButtonScript_Start_m25A1DDC0BEB25CBCB20EB6FBE9E71F006138B3C1 (void);
// 0x0000008B System.Void EpicToonFX.ETFXButtonScript::Update()
extern void ETFXButtonScript_Update_m22634A284FB8C975D9929F15988F09FA1ACDF332 (void);
// 0x0000008C System.Void EpicToonFX.ETFXButtonScript::getProjectileNames()
extern void ETFXButtonScript_getProjectileNames_mA13342862E48B4A8B6BC0666BC7EFE2BB5625D3E (void);
// 0x0000008D System.Boolean EpicToonFX.ETFXButtonScript::overButton()
extern void ETFXButtonScript_overButton_mEFE1C511D8EEA91AE9E54B97CD0A155E542290B0 (void);
// 0x0000008E System.Void EpicToonFX.ETFXButtonScript::.ctor()
extern void ETFXButtonScript__ctor_m65F38109C2CBF445E711A2C6646B34C2483E0455 (void);
// 0x0000008F System.Void EpicToonFX.ETFXFireProjectile::Start()
extern void ETFXFireProjectile_Start_m6586051FBDDC4104ED997E585ABEA089694B805D (void);
// 0x00000090 System.Void EpicToonFX.ETFXFireProjectile::Update()
extern void ETFXFireProjectile_Update_m4F39FFB37EDCE10D6037FDE13FF274C027F60990 (void);
// 0x00000091 System.Void EpicToonFX.ETFXFireProjectile::nextEffect()
extern void ETFXFireProjectile_nextEffect_m406DBBEAF4133E7C3660AB0F5BC32CD6B3B0C7C5 (void);
// 0x00000092 System.Void EpicToonFX.ETFXFireProjectile::previousEffect()
extern void ETFXFireProjectile_previousEffect_m33C82A976F62A20D43E2FF8C57E3066DCB00AE5F (void);
// 0x00000093 System.Void EpicToonFX.ETFXFireProjectile::AdjustSpeed(System.Single)
extern void ETFXFireProjectile_AdjustSpeed_m0A76F61B5320E509827716B5B302AE41B028288A (void);
// 0x00000094 System.Void EpicToonFX.ETFXFireProjectile::.ctor()
extern void ETFXFireProjectile__ctor_mB48108DD5AB58E5907180ED79690E217D8EB1D65 (void);
// 0x00000095 System.Void EpicToonFX.ETFXLoopScript::Start()
extern void ETFXLoopScript_Start_mE67C5E09B7AB2A78688B1877DAC3766A0868CCA8 (void);
// 0x00000096 System.Void EpicToonFX.ETFXLoopScript::PlayEffect()
extern void ETFXLoopScript_PlayEffect_m2EAE66A60E00C863C15BE46D10AC4522789A218E (void);
// 0x00000097 System.Collections.IEnumerator EpicToonFX.ETFXLoopScript::EffectLoop()
extern void ETFXLoopScript_EffectLoop_m728B76DFD989B681D2C93D7A88E9D76C9696CAF4 (void);
// 0x00000098 System.Void EpicToonFX.ETFXLoopScript::.ctor()
extern void ETFXLoopScript__ctor_mC0B4A260353A71EC89B2E1ADA7834286E0F60E5B (void);
// 0x00000099 System.Void EpicToonFX.ETFXLoopScript/<EffectLoop>d__6::.ctor(System.Int32)
extern void U3CEffectLoopU3Ed__6__ctor_m553F1C586BBB6C68EEB9710F62438A8EDCE136DA (void);
// 0x0000009A System.Void EpicToonFX.ETFXLoopScript/<EffectLoop>d__6::System.IDisposable.Dispose()
extern void U3CEffectLoopU3Ed__6_System_IDisposable_Dispose_m8142A69BF60253C0139648E9350176EB9EFD8BFA (void);
// 0x0000009B System.Boolean EpicToonFX.ETFXLoopScript/<EffectLoop>d__6::MoveNext()
extern void U3CEffectLoopU3Ed__6_MoveNext_mE11D3E615622E410D2DAF1960011C76F8ABC322D (void);
// 0x0000009C System.Object EpicToonFX.ETFXLoopScript/<EffectLoop>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEffectLoopU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m93EE11485A4BA42D80F240C1703D673774889545 (void);
// 0x0000009D System.Void EpicToonFX.ETFXLoopScript/<EffectLoop>d__6::System.Collections.IEnumerator.Reset()
extern void U3CEffectLoopU3Ed__6_System_Collections_IEnumerator_Reset_mB35745D308CDED6CAB69D52DEFC2D84AB6CC5896 (void);
// 0x0000009E System.Object EpicToonFX.ETFXLoopScript/<EffectLoop>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CEffectLoopU3Ed__6_System_Collections_IEnumerator_get_Current_mB56FD34E205892C6B85E8FD18DAE68F19ECBDE2E (void);
// 0x0000009F System.Void EpicToonFX.ETFXMouseOrbit::Start()
extern void ETFXMouseOrbit_Start_m4FB8182859BA3A3D8139156DFFFF58A6001F9297 (void);
// 0x000000A0 System.Void EpicToonFX.ETFXMouseOrbit::LateUpdate()
extern void ETFXMouseOrbit_LateUpdate_m27A95AC3E473534D0DAD68B5B76579A0811D8CEC (void);
// 0x000000A1 System.Single EpicToonFX.ETFXMouseOrbit::ClampAngle(System.Single,System.Single,System.Single)
extern void ETFXMouseOrbit_ClampAngle_mA204BF97BDDC49A732917C75BDC934D4CF56B314 (void);
// 0x000000A2 System.Void EpicToonFX.ETFXMouseOrbit::.ctor()
extern void ETFXMouseOrbit__ctor_mB2E0C52CC44B7FE5D064C3183F9DED7714DDCBDC (void);
// 0x000000A3 System.Void EpicToonFX.ETFXTarget::Start()
extern void ETFXTarget_Start_m60CA8E1079E17A7C9A47D10C9FA8AE9220DB1E9F (void);
// 0x000000A4 System.Void EpicToonFX.ETFXTarget::SpawnTarget()
extern void ETFXTarget_SpawnTarget_m00485209171AF5E1A8DB572C256C99B0E821BF96 (void);
// 0x000000A5 System.Void EpicToonFX.ETFXTarget::OnTriggerEnter(UnityEngine.Collider)
extern void ETFXTarget_OnTriggerEnter_m057AF200E64829505145549ED629FAA79935B1F5 (void);
// 0x000000A6 System.Collections.IEnumerator EpicToonFX.ETFXTarget::Respawn()
extern void ETFXTarget_Respawn_m679D0B0188B3DA281D2BE313E7212FC1D6F2B905 (void);
// 0x000000A7 System.Void EpicToonFX.ETFXTarget::.ctor()
extern void ETFXTarget__ctor_mD0F9BFFB2CC402DC5C6FF274EEDD58EB891A22C6 (void);
// 0x000000A8 System.Void EpicToonFX.ETFXTarget/<Respawn>d__7::.ctor(System.Int32)
extern void U3CRespawnU3Ed__7__ctor_m415513F665DE8F3777A3B21AF9C5B45A90CB1BF3 (void);
// 0x000000A9 System.Void EpicToonFX.ETFXTarget/<Respawn>d__7::System.IDisposable.Dispose()
extern void U3CRespawnU3Ed__7_System_IDisposable_Dispose_m63C5D414DB8D33EB86AFF6D5569B6C4E18C32615 (void);
// 0x000000AA System.Boolean EpicToonFX.ETFXTarget/<Respawn>d__7::MoveNext()
extern void U3CRespawnU3Ed__7_MoveNext_m58D6E6D0CFF4964FDE4083115A10119C4FB6FEE3 (void);
// 0x000000AB System.Object EpicToonFX.ETFXTarget/<Respawn>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRespawnU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF70DB558C29B7FFE4A790553093711C3478A2C54 (void);
// 0x000000AC System.Void EpicToonFX.ETFXTarget/<Respawn>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRespawnU3Ed__7_System_Collections_IEnumerator_Reset_m37DFF9115E528EA7997B687DD51321AB843D9BE7 (void);
// 0x000000AD System.Object EpicToonFX.ETFXTarget/<Respawn>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRespawnU3Ed__7_System_Collections_IEnumerator_get_Current_mA84FD67FBB5DE784889C67146E66570FC2E5D80E (void);
// 0x000000AE System.Void EpicToonFX.ETFXLightFade::Start()
extern void ETFXLightFade_Start_m80EEDD11315E813676DA8E550C806F8C96374733 (void);
// 0x000000AF System.Void EpicToonFX.ETFXLightFade::Update()
extern void ETFXLightFade_Update_m3705B92646C8612BC70987DF39BC94B8F88EC49D (void);
// 0x000000B0 System.Void EpicToonFX.ETFXLightFade::.ctor()
extern void ETFXLightFade__ctor_mCDF41DC7334BC08F86DD3EF815FC1F3EA4C44F11 (void);
// 0x000000B1 System.Void EpicToonFX.ETFXPitchRandomizer::Start()
extern void ETFXPitchRandomizer_Start_m64BE3B77C622908C2E324C6D3DFD17CB9092D21E (void);
// 0x000000B2 System.Void EpicToonFX.ETFXPitchRandomizer::.ctor()
extern void ETFXPitchRandomizer__ctor_m8493E0829884B225C1FC98134FA1EFCC44CDD238 (void);
// 0x000000B3 System.Void EpicToonFX.ETFXRotation::Start()
extern void ETFXRotation_Start_m3DC21DA9C26DBA1E14C27B9B997F46BEBBFB22C7 (void);
// 0x000000B4 System.Void EpicToonFX.ETFXRotation::Update()
extern void ETFXRotation_Update_mED37E3F435A6166C540A1DA30B49354F05279100 (void);
// 0x000000B5 System.Void EpicToonFX.ETFXRotation::.ctor()
extern void ETFXRotation__ctor_mC1C7849D64611953B026F069EE879C10B1399ABA (void);
// 0x000000B6 System.Void Mainspace.GreedBeh::Start()
extern void GreedBeh_Start_m37A027DE7AB581CEACE80F5AD2CCF9BD700AA225 (void);
// 0x000000B7 System.Void Mainspace.GreedBeh::OnDestroy()
extern void GreedBeh_OnDestroy_mE81CC0941EDFE81287AB889554662A121745412A (void);
// 0x000000B8 System.Void Mainspace.GreedBeh::StartGame(GameEvents.Battle.StartGame)
extern void GreedBeh_StartGame_m0B7D5597408C27ED8834B4B3CE96C2234B64C9EF (void);
// 0x000000B9 System.Void Mainspace.GreedBeh::StopGame(GameEvents.Battle.PlayerDie)
extern void GreedBeh_StopGame_m794D4D2CA4AB1525FC98400D15A1AD29B1820F5F (void);
// 0x000000BA System.Void Mainspace.GreedBeh::Pause(GameEvents.Battle.Pause)
extern void GreedBeh_Pause_mD45FF1DE015FFE2500C7F8EFB5A2DCDFEAB175A0 (void);
// 0x000000BB System.Void Mainspace.GreedBeh::Unpause(GameEvents.Battle.Unpause)
extern void GreedBeh_Unpause_m9CA7600C6AE26BD969EFA8ECAD208CCA3EF65C07 (void);
// 0x000000BC System.Void Mainspace.GreedBeh::StopRoutines()
extern void GreedBeh_StopRoutines_m33ECD4E704E9CAE7B0B704C33F7C3CE4A785D3A4 (void);
// 0x000000BD System.Collections.IEnumerator Mainspace.GreedBeh::GreedController()
extern void GreedBeh_GreedController_m9AD3972A3D4EDF41FE9B6BB5AAC75AF6071E14AD (void);
// 0x000000BE System.Collections.IEnumerator Mainspace.GreedBeh::DifficultyApper()
extern void GreedBeh_DifficultyApper_mD7A702FA04D913E24905B2B1082A39093F270381 (void);
// 0x000000BF System.Void Mainspace.GreedBeh::SomethingCollected(GameEvents.Battle.CoinCollected)
extern void GreedBeh_SomethingCollected_m5A4C00B6CB8EC9CB3FF336A7425169D95BDC53C7 (void);
// 0x000000C0 System.Void Mainspace.GreedBeh::Punch(System.Single)
extern void GreedBeh_Punch_m5FE8B4C7B5606A61603938B72C205996FD28EC5F (void);
// 0x000000C1 System.Void Mainspace.GreedBeh::.ctor()
extern void GreedBeh__ctor_mE4C9FFDC5DEDBB9368AD549BCFAD2E17328FD3C5 (void);
// 0x000000C2 System.Void Mainspace.GreedBeh/<GreedController>d__23::.ctor(System.Int32)
extern void U3CGreedControllerU3Ed__23__ctor_mED24E9A036599767030BB969BE03320D52DE3BB8 (void);
// 0x000000C3 System.Void Mainspace.GreedBeh/<GreedController>d__23::System.IDisposable.Dispose()
extern void U3CGreedControllerU3Ed__23_System_IDisposable_Dispose_m778A80884DF8544DB4F7E374C965D73586794490 (void);
// 0x000000C4 System.Boolean Mainspace.GreedBeh/<GreedController>d__23::MoveNext()
extern void U3CGreedControllerU3Ed__23_MoveNext_mBF26CA0FE56398652BC8B53E190FA535CA4C924A (void);
// 0x000000C5 System.Object Mainspace.GreedBeh/<GreedController>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGreedControllerU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8C0F4A03686C2661E36FC7B7A700315ADC72348F (void);
// 0x000000C6 System.Void Mainspace.GreedBeh/<GreedController>d__23::System.Collections.IEnumerator.Reset()
extern void U3CGreedControllerU3Ed__23_System_Collections_IEnumerator_Reset_m2114CC24AB04A2370B6F76FD9AE1FEB45F236F55 (void);
// 0x000000C7 System.Object Mainspace.GreedBeh/<GreedController>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CGreedControllerU3Ed__23_System_Collections_IEnumerator_get_Current_mACA549512D9667723F550D555D208986BF10C8AD (void);
// 0x000000C8 System.Void Mainspace.GreedBeh/<DifficultyApper>d__24::.ctor(System.Int32)
extern void U3CDifficultyApperU3Ed__24__ctor_mBA4F037F96B2F476A714F6DBDC8B7A2CA90287BC (void);
// 0x000000C9 System.Void Mainspace.GreedBeh/<DifficultyApper>d__24::System.IDisposable.Dispose()
extern void U3CDifficultyApperU3Ed__24_System_IDisposable_Dispose_m134F71DF2B1632B26105A2275EABFABCE0E632F3 (void);
// 0x000000CA System.Boolean Mainspace.GreedBeh/<DifficultyApper>d__24::MoveNext()
extern void U3CDifficultyApperU3Ed__24_MoveNext_mF693A32CB9519D25CB8D6F97CE7124E9F1FED735 (void);
// 0x000000CB System.Object Mainspace.GreedBeh/<DifficultyApper>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDifficultyApperU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C742F57DD6CF120F66DD900101944356EC7B002 (void);
// 0x000000CC System.Void Mainspace.GreedBeh/<DifficultyApper>d__24::System.Collections.IEnumerator.Reset()
extern void U3CDifficultyApperU3Ed__24_System_Collections_IEnumerator_Reset_m3A9261AFA76DEFAEF6A962E88D9F46E5ACBEC7E1 (void);
// 0x000000CD System.Object Mainspace.GreedBeh/<DifficultyApper>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CDifficultyApperU3Ed__24_System_Collections_IEnumerator_get_Current_m8690A70EEAC79F6399EE311D2B93DC5F7AA85198 (void);
// 0x000000CE System.Void Mainspace.CameraBeh::Start()
extern void CameraBeh_Start_m2D4A3857C1AED278AE858524A1EE52DAC86CD43D (void);
// 0x000000CF System.Void Mainspace.CameraBeh::LateUpdate()
extern void CameraBeh_LateUpdate_m0E7FFAAA766F5F77BDCD43B0E3E8ED8197F1BF7B (void);
// 0x000000D0 System.Void Mainspace.CameraBeh::.ctor()
extern void CameraBeh__ctor_m65480E2C59A6682B181A1D7C2F5207945DC3C29B (void);
// 0x000000D1 System.Void Mainspace.CoinBeh::Awake()
extern void CoinBeh_Awake_m01D2140F8F9DE8E80FD63358896F320793C432C8 (void);
// 0x000000D2 System.Void Mainspace.CoinBeh::OnEnable()
extern void CoinBeh_OnEnable_m884DE0E8AC5A790C61FF43764EF267E934C11A42 (void);
// 0x000000D3 System.Collections.IEnumerator Mainspace.CoinBeh::Rotator()
extern void CoinBeh_Rotator_m40AA8668E10BFADA112DA289E78E735E46B7E4D5 (void);
// 0x000000D4 System.Collections.IEnumerator Mainspace.CoinBeh::Fall()
extern void CoinBeh_Fall_m4200C6B1973602768A994122AFBAF516C305FDC7 (void);
// 0x000000D5 System.Void Mainspace.CoinBeh::PlayOnCollionVFX(System.Single)
extern void CoinBeh_PlayOnCollionVFX_m6E170B523E220A38C3A3B7BD8D7EA44935C93CF5 (void);
// 0x000000D6 System.Void Mainspace.CoinBeh::PlayStaticVFX()
extern void CoinBeh_PlayStaticVFX_m7E71482DE74F9B07E65E5E3FE20EDFC9C12EDCAF (void);
// 0x000000D7 System.Void Mainspace.CoinBeh::CollectVFX()
extern void CoinBeh_CollectVFX_m556BDCB482694FA8BEE78E1C9DB25F4F6D58738E (void);
// 0x000000D8 System.Collections.IEnumerator Mainspace.CoinBeh::DelayedDie()
extern void CoinBeh_DelayedDie_mF36E21BA2FF53B40C20F22FAC0460B9972D72EAA (void);
// 0x000000D9 System.Void Mainspace.CoinBeh::Colected()
extern void CoinBeh_Colected_mBE61F5482F2316C16268A479E508214A8DF81D6B (void);
// 0x000000DA System.Void Mainspace.CoinBeh::Die()
extern void CoinBeh_Die_m46CCF4DEB2F99B8DAAC8813DE86104E7066CF98A (void);
// 0x000000DB System.Void Mainspace.CoinBeh::OnTriggerEnter(UnityEngine.Collider)
extern void CoinBeh_OnTriggerEnter_mF0AC5F3DC88438F55BBE1133DCC9BDB3A97BCE99 (void);
// 0x000000DC System.Void Mainspace.CoinBeh::.ctor()
extern void CoinBeh__ctor_m38F4E15B9888DAA8FAE1EB626F4C314EE2FFA6B3 (void);
// 0x000000DD System.Void Mainspace.CoinBeh/<Rotator>d__17::.ctor(System.Int32)
extern void U3CRotatorU3Ed__17__ctor_m114D28EEABEDC896272B845F7A91892F68306FFE (void);
// 0x000000DE System.Void Mainspace.CoinBeh/<Rotator>d__17::System.IDisposable.Dispose()
extern void U3CRotatorU3Ed__17_System_IDisposable_Dispose_m6315BB8051C0C7EE5D1625FC4E03031B2219CD05 (void);
// 0x000000DF System.Boolean Mainspace.CoinBeh/<Rotator>d__17::MoveNext()
extern void U3CRotatorU3Ed__17_MoveNext_mD068A7E198C1F87088ACCFBE4336AC0F57F74F20 (void);
// 0x000000E0 System.Object Mainspace.CoinBeh/<Rotator>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRotatorU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8A3D8CB7F45697A23FC98BAB9AA05B83C4A447CC (void);
// 0x000000E1 System.Void Mainspace.CoinBeh/<Rotator>d__17::System.Collections.IEnumerator.Reset()
extern void U3CRotatorU3Ed__17_System_Collections_IEnumerator_Reset_m81DCA7E77FC0A97396961845CDD5DCBA2F9C5154 (void);
// 0x000000E2 System.Object Mainspace.CoinBeh/<Rotator>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CRotatorU3Ed__17_System_Collections_IEnumerator_get_Current_m9CE4E05E678854976D2A6843AE0B6262F41E8693 (void);
// 0x000000E3 System.Void Mainspace.CoinBeh/<Fall>d__18::.ctor(System.Int32)
extern void U3CFallU3Ed__18__ctor_mA1BD6BEE887A2EE42B90B2F75315E7E1FB4EC535 (void);
// 0x000000E4 System.Void Mainspace.CoinBeh/<Fall>d__18::System.IDisposable.Dispose()
extern void U3CFallU3Ed__18_System_IDisposable_Dispose_mF309B4C364F44A922728EA07BDA6CF9635CFA32F (void);
// 0x000000E5 System.Boolean Mainspace.CoinBeh/<Fall>d__18::MoveNext()
extern void U3CFallU3Ed__18_MoveNext_m02659C42F0AFF67C3FA36961639FF26B776EAAFE (void);
// 0x000000E6 System.Object Mainspace.CoinBeh/<Fall>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFallU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAC3583E0B3BBC249538DAFF1FC5D2A1D0F69BC0F (void);
// 0x000000E7 System.Void Mainspace.CoinBeh/<Fall>d__18::System.Collections.IEnumerator.Reset()
extern void U3CFallU3Ed__18_System_Collections_IEnumerator_Reset_m9D7DEE6CEADC090BFB3D63C3937B4341E28F3A8D (void);
// 0x000000E8 System.Object Mainspace.CoinBeh/<Fall>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CFallU3Ed__18_System_Collections_IEnumerator_get_Current_m1211A1CDA0C02D5916843A755AE7A032E0954ADE (void);
// 0x000000E9 System.Void Mainspace.CoinBeh/<DelayedDie>d__22::.ctor(System.Int32)
extern void U3CDelayedDieU3Ed__22__ctor_m83D599148B986FE4A385354A2414B1B1F207E131 (void);
// 0x000000EA System.Void Mainspace.CoinBeh/<DelayedDie>d__22::System.IDisposable.Dispose()
extern void U3CDelayedDieU3Ed__22_System_IDisposable_Dispose_mC5D8224A1F4D3929672FDDE8AD96BE060E09CAFA (void);
// 0x000000EB System.Boolean Mainspace.CoinBeh/<DelayedDie>d__22::MoveNext()
extern void U3CDelayedDieU3Ed__22_MoveNext_mB4213CBB5B8B28DED206A67A468446769290F729 (void);
// 0x000000EC System.Object Mainspace.CoinBeh/<DelayedDie>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayedDieU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAD32D4B0BCD495AC64F7C0E2B6B380AA6D349240 (void);
// 0x000000ED System.Void Mainspace.CoinBeh/<DelayedDie>d__22::System.Collections.IEnumerator.Reset()
extern void U3CDelayedDieU3Ed__22_System_Collections_IEnumerator_Reset_m787F6488D83FA9BC2AD52E672155FEB4EA8658C7 (void);
// 0x000000EE System.Object Mainspace.CoinBeh/<DelayedDie>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CDelayedDieU3Ed__22_System_Collections_IEnumerator_get_Current_mA3ED1713C218EF84A4DC28FB92BA6E398DF2C36C (void);
// 0x000000EF System.Void Mainspace.CounterBeh::SetValue(System.Int32)
extern void CounterBeh_SetValue_m45E2A922029303B7A0C1D58EB7E57F53D12CB3EC (void);
// 0x000000F0 System.Void Mainspace.CounterBeh::Start()
extern void CounterBeh_Start_mCCB2E0A232D8A997BCD9C99DEC0548CDBEBC76DE (void);
// 0x000000F1 System.Void Mainspace.CounterBeh::OnDestroy()
extern void CounterBeh_OnDestroy_m6409272BFA70967C280983417813B4DD29F8266C (void);
// 0x000000F2 System.Void Mainspace.CounterBeh::UpdateCounter(GameEvents.Battle.CoinCollected)
extern void CounterBeh_UpdateCounter_m9BCEC3577B12A30D4BD5947AA7A569D28D137DD0 (void);
// 0x000000F3 System.Collections.IEnumerator Mainspace.CounterBeh::LerperNumber(System.Int32,System.Int32)
extern void CounterBeh_LerperNumber_m599BE2B23127C037AA97382BA0BA024FFF2D3D2F (void);
// 0x000000F4 System.Void Mainspace.CounterBeh::Punch(System.Single)
extern void CounterBeh_Punch_m5F37EA170C05200CDFDAA54C5989632817422342 (void);
// 0x000000F5 System.Void Mainspace.CounterBeh::SendResult(GameEvents.Battle.PlayerDie)
extern void CounterBeh_SendResult_mA7E11FC19354244D39AB5B57BA95A7DEB883D8A0 (void);
// 0x000000F6 System.Void Mainspace.CounterBeh::.ctor()
extern void CounterBeh__ctor_m3D022E9113F17CECD96158366DAEF25962E8768E (void);
// 0x000000F7 System.Void Mainspace.CounterBeh/<LerperNumber>d__11::.ctor(System.Int32)
extern void U3CLerperNumberU3Ed__11__ctor_mD9FD4C2897BEC16FC1A9C2B18F6FE69619970BE8 (void);
// 0x000000F8 System.Void Mainspace.CounterBeh/<LerperNumber>d__11::System.IDisposable.Dispose()
extern void U3CLerperNumberU3Ed__11_System_IDisposable_Dispose_m6660869AC0EA25A327B028E9F253931B2072A96B (void);
// 0x000000F9 System.Boolean Mainspace.CounterBeh/<LerperNumber>d__11::MoveNext()
extern void U3CLerperNumberU3Ed__11_MoveNext_m438653738F78C50FE8C47AC3A54E8320AF9A1A99 (void);
// 0x000000FA System.Object Mainspace.CounterBeh/<LerperNumber>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLerperNumberU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB9730D565D67363A535F32E2EA770B2950AF17ED (void);
// 0x000000FB System.Void Mainspace.CounterBeh/<LerperNumber>d__11::System.Collections.IEnumerator.Reset()
extern void U3CLerperNumberU3Ed__11_System_Collections_IEnumerator_Reset_m868765C8474E200FCC40548774D832A1BB02DD86 (void);
// 0x000000FC System.Object Mainspace.CounterBeh/<LerperNumber>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CLerperNumberU3Ed__11_System_Collections_IEnumerator_get_Current_mB0792E3850208F01A09783A4C819C80F9558CF92 (void);
// 0x000000FD System.Void Mainspace.PropBeh::OnEnable()
extern void PropBeh_OnEnable_mED626DF56D3F705182C190FBEB02647E36B1D126 (void);
// 0x000000FE System.Void Mainspace.PropBeh::Update()
extern void PropBeh_Update_mBBBD0FF57DF4C56231B0F42B3AA5677A38F85D0A (void);
// 0x000000FF System.Void Mainspace.PropBeh::.ctor()
extern void PropBeh__ctor_m1DEB1BF0FF2117AAFF4F636D859F3954B3470BAE (void);
// 0x00000100 System.Void Mainspace.ShadowBeh::Init()
extern void ShadowBeh_Init_mFA0429FB0CB028F969261909A1ACEB03702D5446 (void);
// 0x00000101 System.Void Mainspace.ShadowBeh::SetPos(UnityEngine.Vector3)
extern void ShadowBeh_SetPos_m2D863ED8A0A771CC93804803BD4D31868BA0A157 (void);
// 0x00000102 System.Void Mainspace.ShadowBeh::SetSize(System.Single)
extern void ShadowBeh_SetSize_mD2F466F246495E05354166D65B4AA6A1CB4E3D22 (void);
// 0x00000103 System.Void Mainspace.ShadowBeh::Store()
extern void ShadowBeh_Store_mCFCE63BA58298796A4E5BD6E82BCEAF6F7E9C368 (void);
// 0x00000104 System.Void Mainspace.ShadowBeh::.ctor()
extern void ShadowBeh__ctor_mA9D1C7071FB406D3BA4C8087F055C36F2B0C4F42 (void);
// 0x00000105 System.Void Mainspace.BundleInfo::.ctor(System.Int32,System.String)
extern void BundleInfo__ctor_mF4DB7CACD68EBEE05A80861D744354744816008F (void);
// 0x00000106 System.Void Mainspace.ButtonBehaviour::Start()
extern void ButtonBehaviour_Start_m049E3E93A130B44EA85CCFE19A6434A18A95EB63 (void);
// 0x00000107 System.Void Mainspace.ButtonBehaviour::OnDestroy()
extern void ButtonBehaviour_OnDestroy_mD6D9A99C2B5C795E055E6E001D12EDF5CDB28DEC (void);
// 0x00000108 System.Void Mainspace.ButtonBehaviour::ButtonOff(GameEvents.Menu.CancasMoveStart)
extern void ButtonBehaviour_ButtonOff_m6EEBD45A64D5C12A9909F299F4FAE53A6EDF0CC3 (void);
// 0x00000109 System.Void Mainspace.ButtonBehaviour::ButtonOn(GameEvents.Menu.CancasMoveEnd)
extern void ButtonBehaviour_ButtonOn_m6C3208E028821C06A24D81E29A835BE2D20B0F3F (void);
// 0x0000010A System.Void Mainspace.ButtonBehaviour::SetButtunInteractibility(System.Boolean)
extern void ButtonBehaviour_SetButtunInteractibility_mE489AE585AAC4814F04ADFB1F16C3EB6F29C78E3 (void);
// 0x0000010B System.Void Mainspace.ButtonBehaviour::Punch()
extern void ButtonBehaviour_Punch_m6B84826B8C58B2751DAB82FB4D025C38EDB42EB2 (void);
// 0x0000010C System.Void Mainspace.ButtonBehaviour::PingPong()
extern void ButtonBehaviour_PingPong_m8803139DB310C3BAAECD057565E16FE8AB9B429F (void);
// 0x0000010D System.Void Mainspace.ButtonBehaviour::.ctor()
extern void ButtonBehaviour__ctor_m8D8AACDAA926C024974434FE7B399320F8C2A79B (void);
// 0x0000010E System.Void Mainspace.ButtonBehaviour/<>c::.cctor()
extern void U3CU3Ec__cctor_m1D8E1CE79E22EC1DADB8C6B130CF56CF4F0FD392 (void);
// 0x0000010F System.Void Mainspace.ButtonBehaviour/<>c::.ctor()
extern void U3CU3Ec__ctor_m7E06856CE747177228F8B1A739141903D0CA8702 (void);
// 0x00000110 System.Void Mainspace.ButtonBehaviour/<>c::<Start>b__3_0()
extern void U3CU3Ec_U3CStartU3Eb__3_0_m2BDB301B5FA71EE500D1024242D28F3C7BBFD611 (void);
// 0x00000111 System.Void Mainspace.CheckBoxBeh::Start()
extern void CheckBoxBeh_Start_m0767B99C3D7E1600C6CDE0C039D9CA09E19D6D78 (void);
// 0x00000112 System.Void Mainspace.CheckBoxBeh::Init()
extern void CheckBoxBeh_Init_m0997F07C4A65D9684661F19C98DD9B0637ACCE96 (void);
// 0x00000113 System.Void Mainspace.CheckBoxBeh::Switcher()
extern void CheckBoxBeh_Switcher_mE2A5DF844CB50826A4CE2F65B28AF838CF37BAE3 (void);
// 0x00000114 System.Void Mainspace.CheckBoxBeh::Anim(UnityEngine.GameObject,UnityEngine.GameObject)
extern void CheckBoxBeh_Anim_m4C11517FDC91F8A84F257392FE8BCA18ECF90BAF (void);
// 0x00000115 System.Void Mainspace.CheckBoxBeh::.ctor()
extern void CheckBoxBeh__ctor_mA5200223D1EEB9D8275D69D7FE26E64EC6D6C332 (void);
// 0x00000116 System.Void Mainspace.CheckBoxBeh/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_mA68A37A4E847EC8F295CF885D8DDD78C197FA0C1 (void);
// 0x00000117 System.Void Mainspace.CheckBoxBeh/<>c__DisplayClass9_0::<Anim>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CAnimU3Eb__0_m59F45D3050DFE99BEC9E9CD5726787197FFDC0E0 (void);
// 0x00000118 System.Void Mainspace.CheckBoxBeh/<>c__DisplayClass9_0::<Anim>b__1()
extern void U3CU3Ec__DisplayClass9_0_U3CAnimU3Eb__1_m6532E359BF3C3101D79AC4493F95A5DF132CCDBC (void);
// 0x00000119 System.Void Mainspace.CheckBoxBehMus::Init()
extern void CheckBoxBehMus_Init_m9FEB18592EAB7F4C47D06F4A7E872ECEE0054DAB (void);
// 0x0000011A System.Void Mainspace.CheckBoxBehMus::Switcher()
extern void CheckBoxBehMus_Switcher_mD530956C8B67C1FF6E7BF482E9D05FC39E009D06 (void);
// 0x0000011B System.Void Mainspace.CheckBoxBehMus::.ctor()
extern void CheckBoxBehMus__ctor_m4E1275BF1BFEB8DF53225C103BDA1137BE2FFB27 (void);
// 0x0000011C System.Void Mainspace.CheckBoxBehSfx::Init()
extern void CheckBoxBehSfx_Init_mCA8A2397D813A10CC3529027BEB8E9578DF390C3 (void);
// 0x0000011D System.Void Mainspace.CheckBoxBehSfx::Switcher()
extern void CheckBoxBehSfx_Switcher_mE5709A22FA91FF8EF7ED1E686EF1A29483AC283D (void);
// 0x0000011E System.Void Mainspace.CheckBoxBehSfx::Anim(UnityEngine.GameObject,UnityEngine.GameObject)
extern void CheckBoxBehSfx_Anim_mC8E49AD557D54FBB51E970814E5AD23E02EEE667 (void);
// 0x0000011F System.Void Mainspace.CheckBoxBehSfx::.ctor()
extern void CheckBoxBehSfx__ctor_m74DE136E09B3BFAE1A3175AA4668012431E25922 (void);
// 0x00000120 System.Void Mainspace.CheckBoxBehSfx/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m299FDC95A04148C28B0D683290F7234A8E27FDB4 (void);
// 0x00000121 System.Void Mainspace.CheckBoxBehSfx/<>c__DisplayClass2_0::<Anim>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CAnimU3Eb__0_m2019EAE8BB86AE4290DB80C54B9A5DA695158B04 (void);
// 0x00000122 System.Void Mainspace.CheckBoxBehSfx/<>c__DisplayClass2_0::<Anim>b__1()
extern void U3CU3Ec__DisplayClass2_0_U3CAnimU3Eb__1_m6ADFA764B62E0DCC3CC3AFC91EF3442ED3DD8FE6 (void);
// 0x00000123 System.Void Mainspace.LogoClorsChanger::ColorChange()
extern void LogoClorsChanger_ColorChange_m99C81E7CCB1FCA3BDE8BC27565980302D1CFB37F (void);
// 0x00000124 System.Void Mainspace.LogoClorsChanger::.ctor()
extern void LogoClorsChanger__ctor_m4368AFF6E4603BFC4B14F4BD321F3516D5D20FE9 (void);
// 0x00000125 System.Void Mainspace.RectTransformExtensions::SetDefaultScale(UnityEngine.RectTransform)
extern void RectTransformExtensions_SetDefaultScale_m756116B110805E0750BD50CFB5AC9EAD5FEB0E0C (void);
// 0x00000126 System.Void Mainspace.RectTransformExtensions::SetPivotAndAnchors(UnityEngine.RectTransform,UnityEngine.Vector2)
extern void RectTransformExtensions_SetPivotAndAnchors_m3739A3FDBB6ECF890D2794F3A0D140C241EAD927 (void);
// 0x00000127 UnityEngine.Vector2 Mainspace.RectTransformExtensions::GetSize(UnityEngine.RectTransform)
extern void RectTransformExtensions_GetSize_m5E15B6433CDE84F88039436390CC83E1D87EC4D9 (void);
// 0x00000128 System.Single Mainspace.RectTransformExtensions::GetWidth(UnityEngine.RectTransform)
extern void RectTransformExtensions_GetWidth_m65D963AF477887BF495B2F9CB66E81B921CA32F3 (void);
// 0x00000129 System.Single Mainspace.RectTransformExtensions::GetHeight(UnityEngine.RectTransform)
extern void RectTransformExtensions_GetHeight_m061AB7A3F469AD5BB5C432F69063FB29E198E3E0 (void);
// 0x0000012A System.Void Mainspace.RectTransformExtensions::SetPositionOfPivot(UnityEngine.RectTransform,UnityEngine.Vector2)
extern void RectTransformExtensions_SetPositionOfPivot_mB94D62C0DA9123D1A5237B7F9352F36C40ECDCE9 (void);
// 0x0000012B System.Void Mainspace.RectTransformExtensions::SetLeftBottomPosition(UnityEngine.RectTransform,UnityEngine.Vector2)
extern void RectTransformExtensions_SetLeftBottomPosition_m9A969464222C09A18146A52B02C4A16646B12A54 (void);
// 0x0000012C System.Void Mainspace.RectTransformExtensions::SetLeftTopPosition(UnityEngine.RectTransform,UnityEngine.Vector2)
extern void RectTransformExtensions_SetLeftTopPosition_m262E8022AE28161E22ED3442CF58FBB35909667B (void);
// 0x0000012D System.Void Mainspace.RectTransformExtensions::SetRightBottomPosition(UnityEngine.RectTransform,UnityEngine.Vector2)
extern void RectTransformExtensions_SetRightBottomPosition_m0E6A4FE54AD6CF0F22617AA9E942207757C1D64C (void);
// 0x0000012E System.Void Mainspace.RectTransformExtensions::SetRightTopPosition(UnityEngine.RectTransform,UnityEngine.Vector2)
extern void RectTransformExtensions_SetRightTopPosition_mED1168AD12631EECFD0C863BD87E7430B5CF183C (void);
// 0x0000012F System.Void Mainspace.RectTransformExtensions::SetSize(UnityEngine.RectTransform,UnityEngine.Vector2)
extern void RectTransformExtensions_SetSize_mECA4608AA6FAE6B4F3955D38542102448BF50C7C (void);
// 0x00000130 System.Void Mainspace.RectTransformExtensions::SetWidth(UnityEngine.RectTransform,System.Single)
extern void RectTransformExtensions_SetWidth_mF6725AB65F1C2BB7355870EAE41A5A3BF5E16CF5 (void);
// 0x00000131 System.Void Mainspace.RectTransformExtensions::SetHeight(UnityEngine.RectTransform,System.Single)
extern void RectTransformExtensions_SetHeight_m23A05DB16E19D8931FC02B61AD32D22B52ECEE83 (void);
// 0x00000132 UnityEngine.Transform Mainspace.BattleManager::get_PlayerTrans()
extern void BattleManager_get_PlayerTrans_m22A007F333B019DBD0E3337B1E903F380A5B8005 (void);
// 0x00000133 System.Void Mainspace.BattleManager::Start()
extern void BattleManager_Start_m87E7EFEA0928318604248D3ADC75A0DC153CB808 (void);
// 0x00000134 System.Void Mainspace.BattleManager::OnDestroy()
extern void BattleManager_OnDestroy_m5E2DCE2EFA52F203521771E7715377876D7D2607 (void);
// 0x00000135 System.Void Mainspace.BattleManager::StartRouts()
extern void BattleManager_StartRouts_mEFB6D66B0BA36B6B0B0713C1AE5B074E1CE7549A (void);
// 0x00000136 System.Collections.IEnumerator Mainspace.BattleManager::CoinsDropper()
extern void BattleManager_CoinsDropper_m394A526819130312751C5AB1A3730B358709802F (void);
// 0x00000137 System.Collections.IEnumerator Mainspace.BattleManager::PropsSpawner()
extern void BattleManager_PropsSpawner_mC5067D1A2AFFABEC2BFDD6FEBD7F92E0A275DB29 (void);
// 0x00000138 System.Void Mainspace.BattleManager::Pause(GameEvents.Battle.Pause)
extern void BattleManager_Pause_m64FC78FA91FD6E0704399C0739E22B9BD74550F4 (void);
// 0x00000139 System.Void Mainspace.BattleManager::Unpause(GameEvents.Battle.Unpause)
extern void BattleManager_Unpause_m9C8D46FF5745AFC37C7C1216C7D19615EAB965DD (void);
// 0x0000013A System.Void Mainspace.BattleManager::StopGame(GameEvents.Battle.PlayerDie)
extern void BattleManager_StopGame_m0F809FBAC640CB843CD737805DF8F7C8F4FCDE5D (void);
// 0x0000013B System.Void Mainspace.BattleManager::StartGame(GameEvents.Battle.StartGame)
extern void BattleManager_StartGame_m9E74E827DAC7F35D451C9996DB58092859300BB2 (void);
// 0x0000013C System.Void Mainspace.BattleManager::StopRoutines()
extern void BattleManager_StopRoutines_mE7D861E5B095596F007992919DC94A1FBED9A04A (void);
// 0x0000013D System.Void Mainspace.BattleManager::.ctor()
extern void BattleManager__ctor_m7C2C690C92F16BD4782694DE18929CD3E7FFE577 (void);
// 0x0000013E System.Void Mainspace.BattleManager/<CoinsDropper>d__16::.ctor(System.Int32)
extern void U3CCoinsDropperU3Ed__16__ctor_m5B20A37FCEFC5792A1ECBBE76109AAA212EDFE78 (void);
// 0x0000013F System.Void Mainspace.BattleManager/<CoinsDropper>d__16::System.IDisposable.Dispose()
extern void U3CCoinsDropperU3Ed__16_System_IDisposable_Dispose_m2513921149CA83BF9D4ED0949FB0A1CB476A73C4 (void);
// 0x00000140 System.Boolean Mainspace.BattleManager/<CoinsDropper>d__16::MoveNext()
extern void U3CCoinsDropperU3Ed__16_MoveNext_mBC651FF96409D2420A3FC4C4803541F3BDFA190A (void);
// 0x00000141 System.Object Mainspace.BattleManager/<CoinsDropper>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCoinsDropperU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mED52FFF71BA21C416E7500AE61BDD67E9BA46538 (void);
// 0x00000142 System.Void Mainspace.BattleManager/<CoinsDropper>d__16::System.Collections.IEnumerator.Reset()
extern void U3CCoinsDropperU3Ed__16_System_Collections_IEnumerator_Reset_m0D4F891AA49D23F2917BBE2D2C97BB9318997040 (void);
// 0x00000143 System.Object Mainspace.BattleManager/<CoinsDropper>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CCoinsDropperU3Ed__16_System_Collections_IEnumerator_get_Current_mF4B5A889FECEAB4450FCD66D115160011B90FF50 (void);
// 0x00000144 System.Void Mainspace.BattleManager/<PropsSpawner>d__17::.ctor(System.Int32)
extern void U3CPropsSpawnerU3Ed__17__ctor_m0C53E3153219B6888EFF50E1FBEBDA78A1E9FE2B (void);
// 0x00000145 System.Void Mainspace.BattleManager/<PropsSpawner>d__17::System.IDisposable.Dispose()
extern void U3CPropsSpawnerU3Ed__17_System_IDisposable_Dispose_m5F2BA115EA1588C1E45604A9E7EE633291E6D742 (void);
// 0x00000146 System.Boolean Mainspace.BattleManager/<PropsSpawner>d__17::MoveNext()
extern void U3CPropsSpawnerU3Ed__17_MoveNext_mF95422EEC4543954729ABC45C8812FC2012F7408 (void);
// 0x00000147 System.Object Mainspace.BattleManager/<PropsSpawner>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPropsSpawnerU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD2BCDF6BD7EA85A558C6090E32A48DAFF313AB19 (void);
// 0x00000148 System.Void Mainspace.BattleManager/<PropsSpawner>d__17::System.Collections.IEnumerator.Reset()
extern void U3CPropsSpawnerU3Ed__17_System_Collections_IEnumerator_Reset_m8670F90638804486CC750EBA42D27124A4A4F4D1 (void);
// 0x00000149 System.Object Mainspace.BattleManager/<PropsSpawner>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CPropsSpawnerU3Ed__17_System_Collections_IEnumerator_get_Current_m54019008A86128E9F048DF85037C0518527FEB58 (void);
// 0x0000014A System.Int32 Mainspace.BattleUiManager::get_screenWidth()
extern void BattleUiManager_get_screenWidth_m6D85E238C793826ADA714401A92A81B03599C655 (void);
// 0x0000014B System.Void Mainspace.BattleUiManager::Start()
extern void BattleUiManager_Start_mE843B18FE823ECE79AE644F71F21AF52A121F267 (void);
// 0x0000014C System.Void Mainspace.BattleUiManager::OnDestroy()
extern void BattleUiManager_OnDestroy_m0D8E7DB1A7A9441EE4B68D394882F354617ED917 (void);
// 0x0000014D System.Collections.IEnumerator Mainspace.BattleUiManager::RectLerper(UnityEngine.Vector2,UnityEngine.Vector2)
extern void BattleUiManager_RectLerper_m0381C4B0C57388EBA10CA3C1C8EDEAA217AB3EF8 (void);
// 0x0000014E System.Void Mainspace.BattleUiManager::GoToMenuScene()
extern void BattleUiManager_GoToMenuScene_mB382857A123BEA1A6918A9ACE06877837E96C75E (void);
// 0x0000014F System.Void Mainspace.BattleUiManager::GoToBattleScene()
extern void BattleUiManager_GoToBattleScene_m93DA5B4C04D3D935FCE859564B3F4A366A88B81A (void);
// 0x00000150 System.Void Mainspace.BattleUiManager::GoToSettings()
extern void BattleUiManager_GoToSettings_mC91798FAB041A48A6CEAFF5BD92EE7F56E2A6685 (void);
// 0x00000151 System.Void Mainspace.BattleUiManager::BackFromSettings()
extern void BattleUiManager_BackFromSettings_mA439894FAB4873C9BD53CE9BA8E786054B4E7217 (void);
// 0x00000152 System.Void Mainspace.BattleUiManager::StartGame()
extern void BattleUiManager_StartGame_m184774B8BEACE8A21933D26FC36AC2E02FA6CF68 (void);
// 0x00000153 System.Void Mainspace.BattleUiManager::SaveScore(GameEvents.Battle.NewScoreResult)
extern void BattleUiManager_SaveScore_mADA825146532AF5C7B083959AE4854B75D87C832 (void);
// 0x00000154 System.Void Mainspace.BattleUiManager::ShowResultWIndow(System.Int32,System.Boolean)
extern void BattleUiManager_ShowResultWIndow_m28BF86BD732209A7BACDAFDE378B1ABB4B6D5D5C (void);
// 0x00000155 System.Void Mainspace.BattleUiManager::.ctor()
extern void BattleUiManager__ctor_mFEB5665F3BFA8264611702E7EB164AA7C0BD694E (void);
// 0x00000156 System.Void Mainspace.BattleUiManager/<RectLerper>d__22::.ctor(System.Int32)
extern void U3CRectLerperU3Ed__22__ctor_mFE4BC35956BB1F5CAEB16BB2382C1C7BAB5FA335 (void);
// 0x00000157 System.Void Mainspace.BattleUiManager/<RectLerper>d__22::System.IDisposable.Dispose()
extern void U3CRectLerperU3Ed__22_System_IDisposable_Dispose_m3BA1315708803BD83C616C77D7A255BC598135F1 (void);
// 0x00000158 System.Boolean Mainspace.BattleUiManager/<RectLerper>d__22::MoveNext()
extern void U3CRectLerperU3Ed__22_MoveNext_m644DB85F89D46040A80F948208A4E1F92C61184B (void);
// 0x00000159 System.Object Mainspace.BattleUiManager/<RectLerper>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRectLerperU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9E6376E59EBF96B900D203887FE32F6EE74A3AC7 (void);
// 0x0000015A System.Void Mainspace.BattleUiManager/<RectLerper>d__22::System.Collections.IEnumerator.Reset()
extern void U3CRectLerperU3Ed__22_System_Collections_IEnumerator_Reset_m0395461C746FA32266CEAB64C0CAF0B1F5D14219 (void);
// 0x0000015B System.Object Mainspace.BattleUiManager/<RectLerper>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CRectLerperU3Ed__22_System_Collections_IEnumerator_get_Current_m2F39E8D4723F7EA8CE96441AB4D337E0956F07A5 (void);
// 0x0000015C System.Boolean Mainspace.EventManager::get_ManagerLoaded()
extern void EventManager_get_ManagerLoaded_m0CDF1ABA9D71D9FDF98A55DED0580ABE7BB09119 (void);
// 0x0000015D System.Void Mainspace.EventManager::set_ManagerLoaded(System.Boolean)
extern void EventManager_set_ManagerLoaded_mF465E0FC890BAFCD46E08A7B45C341CCFF51180C (void);
// 0x0000015E System.Void Mainspace.EventManager::InitManager()
extern void EventManager_InitManager_mA82C80579104290B0B3F9EACEB60A7338D84BECD (void);
// 0x0000015F System.Void Mainspace.EventManager::Subscribe(System.Action`1<T>)
// 0x00000160 System.Void Mainspace.EventManager::Unsubscribe(System.Action`1<T>)
// 0x00000161 System.Void Mainspace.EventManager::Trigger(T)
// 0x00000162 System.Void Mainspace.EventManager::ClearEventsOf()
// 0x00000163 System.Void Mainspace.EventManager::ClearAllEvents()
extern void EventManager_ClearAllEvents_m21D2168AAA4F9D43DB4CD2E52AEA1E332F3C9824 (void);
// 0x00000164 System.Void Mainspace.EventManager::ClearAllEventsBut(System.Type[])
extern void EventManager_ClearAllEventsBut_mDE1909B896E1E3A4B6F71B021849CB2A01EEA723 (void);
// 0x00000165 System.Void Mainspace.EventManager::ReloadManager()
extern void EventManager_ReloadManager_m1C639031EEB26BA2A292EC97245F4E1047E218EE (void);
// 0x00000166 System.Void Mainspace.EventManager::OnDisable()
extern void EventManager_OnDisable_m331BD5C8ECB71257C3FD01D38ED36AB6ACB0F517 (void);
// 0x00000167 System.Void Mainspace.EventManager::.ctor()
extern void EventManager__ctor_m5C11E791A8B96426FB471901E5907CC78EB362E4 (void);
// 0x00000168 System.Void Mainspace.GameCore::Awake()
extern void GameCore_Awake_m55A860B8AE16BA8FD8CAE492C12B607227F5211F (void);
// 0x00000169 System.Collections.IEnumerator Mainspace.GameCore::InitCore()
extern void GameCore_InitCore_m8B8D858F49C6159AC07980638C0F6155DCFF0B3C (void);
// 0x0000016A System.Collections.IEnumerator Mainspace.GameCore::ManagersInit()
extern void GameCore_ManagersInit_m628635156AB71C4AB20CD389F9FA13204DA224C6 (void);
// 0x0000016B System.Void Mainspace.GameCore::.ctor()
extern void GameCore__ctor_m5B2B882D40F3C2F7BE008F519B0038B1F04AAE48 (void);
// 0x0000016C System.Void Mainspace.GameCore::.cctor()
extern void GameCore__cctor_mCEB86C2ED43FB983B668205684D7E21580516DB6 (void);
// 0x0000016D System.Boolean Mainspace.GameCore/Info::get_GameCoreIsLoaded()
extern void Info_get_GameCoreIsLoaded_m9CDDD2913BDC5E60498E144D567CF787E7790F44 (void);
// 0x0000016E Mainspace.ScenesManager Mainspace.GameCore/Managers::get_ScenesManager()
extern void Managers_get_ScenesManager_m9CBDA8A26D4E496A22AD25136F595882923E5473 (void);
// 0x0000016F Mainspace.EventManager Mainspace.GameCore/Managers::get_EventManager()
extern void Managers_get_EventManager_m26C1B23AB798F474FC2DC701D8E41BA71FAFB22B (void);
// 0x00000170 Mainspace.PoolManager Mainspace.GameCore/Managers::get_PoolManager()
extern void Managers_get_PoolManager_m8C6B883E85E3B151FA32350DC2882F27DA9BE6A2 (void);
// 0x00000171 Mainspace.MenuManager Mainspace.GameCore/Managers::get_MenuManager()
extern void Managers_get_MenuManager_m810D98BC12C0B35E1C3561C74E1B400261E9F85C (void);
// 0x00000172 Mainspace.SoundsPlayerManager Mainspace.GameCore/Managers::get_SoundsPlayerManager()
extern void Managers_get_SoundsPlayerManager_mEE039E8C9E04636A04B19251E3B3B028B9C4F8C2 (void);
// 0x00000173 System.Void Mainspace.GameCore/<InitCore>d__3::.ctor(System.Int32)
extern void U3CInitCoreU3Ed__3__ctor_mCCAD943136895CF43DF40DAD28D6D05461041FA7 (void);
// 0x00000174 System.Void Mainspace.GameCore/<InitCore>d__3::System.IDisposable.Dispose()
extern void U3CInitCoreU3Ed__3_System_IDisposable_Dispose_m214566DFB3B6CC50D2C9CC9AD7B65452E2FDBCF9 (void);
// 0x00000175 System.Boolean Mainspace.GameCore/<InitCore>d__3::MoveNext()
extern void U3CInitCoreU3Ed__3_MoveNext_m703072F3DBF5336A53ED0B4EE02D0889BC9539DE (void);
// 0x00000176 System.Object Mainspace.GameCore/<InitCore>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitCoreU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF66EBC679A96E1B1F5BFF2DD05003DFA1A3C8BF7 (void);
// 0x00000177 System.Void Mainspace.GameCore/<InitCore>d__3::System.Collections.IEnumerator.Reset()
extern void U3CInitCoreU3Ed__3_System_Collections_IEnumerator_Reset_mE2AC84D59ACB0435151AC467DBC64F928500E0EA (void);
// 0x00000178 System.Object Mainspace.GameCore/<InitCore>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CInitCoreU3Ed__3_System_Collections_IEnumerator_get_Current_mAAA0F1C1AC6138B672DC1A1CD62FC68E5CE1ED33 (void);
// 0x00000179 System.Void Mainspace.GameCore/<ManagersInit>d__4::.ctor(System.Int32)
extern void U3CManagersInitU3Ed__4__ctor_m9D17335FFA81B6E19CFAC5F36D195B5B6465EA49 (void);
// 0x0000017A System.Void Mainspace.GameCore/<ManagersInit>d__4::System.IDisposable.Dispose()
extern void U3CManagersInitU3Ed__4_System_IDisposable_Dispose_m3BC8E51F2662DD30EA6D7F7F5ECF3D3DA7599584 (void);
// 0x0000017B System.Boolean Mainspace.GameCore/<ManagersInit>d__4::MoveNext()
extern void U3CManagersInitU3Ed__4_MoveNext_mF2C1957685E71DFB25732A4EF16A53180A7B947A (void);
// 0x0000017C System.Object Mainspace.GameCore/<ManagersInit>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CManagersInitU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDBAA898997E6235197484255E05B6FF5E23EDAE8 (void);
// 0x0000017D System.Void Mainspace.GameCore/<ManagersInit>d__4::System.Collections.IEnumerator.Reset()
extern void U3CManagersInitU3Ed__4_System_Collections_IEnumerator_Reset_m2FBF32B1158983877657F90DEFDA128510E8FCA9 (void);
// 0x0000017E System.Object Mainspace.GameCore/<ManagersInit>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CManagersInitU3Ed__4_System_Collections_IEnumerator_get_Current_m00C0BEFE87BD041DAEEDC27CA297B07357197B3D (void);
// 0x0000017F System.Int32 Mainspace.MenuManager::get_screenWidth()
extern void MenuManager_get_screenWidth_mF99F0B9631E3B0727CF01F63B56275D9440561EA (void);
// 0x00000180 System.Void Mainspace.MenuManager::GoToSettings()
extern void MenuManager_GoToSettings_m5214A257804B21F7C46111349B17E50869C82949 (void);
// 0x00000181 System.Void Mainspace.MenuManager::BackToMainWindow()
extern void MenuManager_BackToMainWindow_mDBB195CCACFA95FA20B98951391086D80D901B4A (void);
// 0x00000182 System.Collections.IEnumerator Mainspace.MenuManager::RectLerper(UnityEngine.Vector2,UnityEngine.Vector2)
extern void MenuManager_RectLerper_m673FD921A02A52C9AEEA52EBBB0BA25822612C27 (void);
// 0x00000183 System.Void Mainspace.MenuManager::PlayClicked()
extern void MenuManager_PlayClicked_m79AE20784456C671AFD84F7467AE17827654360A (void);
// 0x00000184 System.Void Mainspace.MenuManager::.ctor()
extern void MenuManager__ctor_m987520DA540050F86F6FC46014A7C2CE617B0BB7 (void);
// 0x00000185 System.Void Mainspace.MenuManager/<RectLerper>d__7::.ctor(System.Int32)
extern void U3CRectLerperU3Ed__7__ctor_m30B56C1B8F94B603CDD28B9094630DBCC58DBB37 (void);
// 0x00000186 System.Void Mainspace.MenuManager/<RectLerper>d__7::System.IDisposable.Dispose()
extern void U3CRectLerperU3Ed__7_System_IDisposable_Dispose_m07B7C2EF1F8B0D6485FC4D0AC507D3047424491C (void);
// 0x00000187 System.Boolean Mainspace.MenuManager/<RectLerper>d__7::MoveNext()
extern void U3CRectLerperU3Ed__7_MoveNext_mA07A95F0A6BCF91EA95A49E130BB8B505DDFB0DD (void);
// 0x00000188 System.Object Mainspace.MenuManager/<RectLerper>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRectLerperU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0DE627B04D8C907BA9536B54F2DDCCA4E18CB2D5 (void);
// 0x00000189 System.Void Mainspace.MenuManager/<RectLerper>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRectLerperU3Ed__7_System_Collections_IEnumerator_Reset_m087D959A04E82FA45CF5482C60AE988A0E886B50 (void);
// 0x0000018A System.Object Mainspace.MenuManager/<RectLerper>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRectLerperU3Ed__7_System_Collections_IEnumerator_get_Current_mD7CE3F95967A694D0005D7CD0A7792718586875E (void);
// 0x0000018B System.Boolean Mainspace.PoolManager::get_ManagerLoaded()
extern void PoolManager_get_ManagerLoaded_mEA8DC0C106DAFF98B4E44BE29BD05D25B898CFD6 (void);
// 0x0000018C System.Void Mainspace.PoolManager::set_ManagerLoaded(System.Boolean)
extern void PoolManager_set_ManagerLoaded_mB1934AED08E82FA5E05C24928872827F339EB026 (void);
// 0x0000018D System.Void Mainspace.PoolManager::InitManager()
extern void PoolManager_InitManager_m8BA3F7B828A2EF8415AAE0F4643131F37FC47AF6 (void);
// 0x0000018E System.Void Mainspace.PoolManager::ReloadManager()
extern void PoolManager_ReloadManager_m321BBC86F78D1B498F13D4D25C926BDD0702F3C5 (void);
// 0x0000018F UnityEngine.GameObject Mainspace.PoolManager::GetObj(UnityEngine.GameObject,System.Boolean)
extern void PoolManager_GetObj_mEABC8FEE015CACFF56AAF14609FED4B0F31C6712 (void);
// 0x00000190 UnityEngine.GameObject Mainspace.PoolManager::GetObjectWithTimer(UnityEngine.GameObject,System.Single,System.Boolean)
extern void PoolManager_GetObjectWithTimer_m71EFD815C24FB9EEFEAC32C1FB9524CDD52E4E92 (void);
// 0x00000191 System.Void Mainspace.PoolManager::StoreObj(UnityEngine.GameObject)
extern void PoolManager_StoreObj_m3693648A1673A06264CDBEC6B5826960BC8F59BF (void);
// 0x00000192 System.Void Mainspace.PoolManager::DelayedStore(UnityEngine.GameObject,System.Single)
extern void PoolManager_DelayedStore_m5EDF6D0D777DAC9E6D302B544C76ACEE2A2CCF17 (void);
// 0x00000193 System.Collections.IEnumerator Mainspace.PoolManager::DelayedExecuteRout(System.Single,System.Action)
extern void PoolManager_DelayedExecuteRout_m925F863DF8679D4EAD358D9CEF05963B6DA83ADB (void);
// 0x00000194 System.Void Mainspace.PoolManager::StoreEverithing()
extern void PoolManager_StoreEverithing_m54F97D7594D5B675AF76E3ED800992D7EAF4CB61 (void);
// 0x00000195 System.Void Mainspace.PoolManager::.ctor()
extern void PoolManager__ctor_mAB5AC56BD81688C8EA329ADD3CEFA86FA0D12060 (void);
// 0x00000196 System.Void Mainspace.PoolManager/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_mA71F5A03F602E54EB53C989C91721E74E3BF1196 (void);
// 0x00000197 System.Void Mainspace.PoolManager/<>c__DisplayClass11_0::<DelayedStore>b__0()
extern void U3CU3Ec__DisplayClass11_0_U3CDelayedStoreU3Eb__0_m1D5193886E722EFC5BFA01DCD3140E973837E10F (void);
// 0x00000198 System.Void Mainspace.PoolManager/<DelayedExecuteRout>d__12::.ctor(System.Int32)
extern void U3CDelayedExecuteRoutU3Ed__12__ctor_mD9E6417E2A02D10C7437F9EB77CE8D88F83AB8E8 (void);
// 0x00000199 System.Void Mainspace.PoolManager/<DelayedExecuteRout>d__12::System.IDisposable.Dispose()
extern void U3CDelayedExecuteRoutU3Ed__12_System_IDisposable_Dispose_m9ACD873B22A38611DBA9C4ACBD113A473FE78DB8 (void);
// 0x0000019A System.Boolean Mainspace.PoolManager/<DelayedExecuteRout>d__12::MoveNext()
extern void U3CDelayedExecuteRoutU3Ed__12_MoveNext_m16D259BE12B3167513F74AB5B2AA659E7E927918 (void);
// 0x0000019B System.Object Mainspace.PoolManager/<DelayedExecuteRout>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayedExecuteRoutU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDE80C087E891DEAEC694EA8BC3153EEAD3D96953 (void);
// 0x0000019C System.Void Mainspace.PoolManager/<DelayedExecuteRout>d__12::System.Collections.IEnumerator.Reset()
extern void U3CDelayedExecuteRoutU3Ed__12_System_Collections_IEnumerator_Reset_mC12C650FE8CF3A3377E0CB43B7C950128D2666F0 (void);
// 0x0000019D System.Object Mainspace.PoolManager/<DelayedExecuteRout>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CDelayedExecuteRoutU3Ed__12_System_Collections_IEnumerator_get_Current_m9C8E239CAC36693CAB41741151A0461C56FD1137 (void);
// 0x0000019E System.Boolean Mainspace.ScenesManager::get_ManagerLoaded()
extern void ScenesManager_get_ManagerLoaded_m9853E6F6C20C9A0A8B8AE1CCB175DBC4A6CC1A7B (void);
// 0x0000019F System.Void Mainspace.ScenesManager::set_ManagerLoaded(System.Boolean)
extern void ScenesManager_set_ManagerLoaded_m498FBC0E72C9775D90B33FB968326DDA1AE027BB (void);
// 0x000001A0 System.Void Mainspace.ScenesManager::InitManager()
extern void ScenesManager_InitManager_m431D0F1C232148EBC0660BB89F8F3A7FB7598002 (void);
// 0x000001A1 System.Void Mainspace.ScenesManager::ReloadManager()
extern void ScenesManager_ReloadManager_m47F9946E7108DC995F8BF282907394A348008B21 (void);
// 0x000001A2 System.Void Mainspace.ScenesManager::LoadBattle(GameEvents.Menu.GoToBattleSceneClicked)
extern void ScenesManager_LoadBattle_m4B99735109F3E5E7B4D8562392ABBB6F2B8B88B2 (void);
// 0x000001A3 System.Void Mainspace.ScenesManager::LoadMenu(GameEvents.Menu.GoToMenuSceneClicked)
extern void ScenesManager_LoadMenu_mBCB6C19089DB732CC8872BC0236F36676F8F5EB7 (void);
// 0x000001A4 System.Collections.IEnumerator Mainspace.ScenesManager::LoadMenu()
extern void ScenesManager_LoadMenu_mB5F53C0CBB07D0F8DCCD0582718698BE354350A9 (void);
// 0x000001A5 System.Collections.IEnumerator Mainspace.ScenesManager::LoadBattleRout()
extern void ScenesManager_LoadBattleRout_m6E134CC1BC70F1A493C32FE27BDE87D0B9796218 (void);
// 0x000001A6 System.Void Mainspace.ScenesManager::FadeOut()
extern void ScenesManager_FadeOut_mEBB374EF89EF0B3E0877D98781086296C31A243B (void);
// 0x000001A7 System.Void Mainspace.ScenesManager::FadeIn()
extern void ScenesManager_FadeIn_m87C3AEC0086B75BBFDA66725C0E3F6621222D69B (void);
// 0x000001A8 System.Void Mainspace.ScenesManager::DisableImage(System.Boolean)
extern void ScenesManager_DisableImage_m632A6EEEA32B9CAB3D63D714F91C777BF0B1176E (void);
// 0x000001A9 System.Void Mainspace.ScenesManager::Update()
extern void ScenesManager_Update_m7E98163D9637E6DF78D365658DFAF1DACBD7B258 (void);
// 0x000001AA System.Collections.IEnumerator Mainspace.ScenesManager::LoadBundle()
extern void ScenesManager_LoadBundle_m6E2FD6AEC587F263C243CE835383630A5C4A840D (void);
// 0x000001AB System.Collections.IEnumerator Mainspace.ScenesManager::GetBundlesVersionInfo()
extern void ScenesManager_GetBundlesVersionInfo_m877050724519A0516789CF3A91D43C14F5CFC385 (void);
// 0x000001AC System.Void Mainspace.ScenesManager::.ctor()
extern void ScenesManager__ctor_mB5A34F3B7CB07104B2A46DD948E0C75793F29A17 (void);
// 0x000001AD System.Void Mainspace.ScenesManager/<LoadMenu>d__28::.ctor(System.Int32)
extern void U3CLoadMenuU3Ed__28__ctor_m83C8D766D0CA9BCE7DEC0052E90A6478B49DF083 (void);
// 0x000001AE System.Void Mainspace.ScenesManager/<LoadMenu>d__28::System.IDisposable.Dispose()
extern void U3CLoadMenuU3Ed__28_System_IDisposable_Dispose_mCCA53E59ABB7B1BF4C661A5FEE7D434EC29D695A (void);
// 0x000001AF System.Boolean Mainspace.ScenesManager/<LoadMenu>d__28::MoveNext()
extern void U3CLoadMenuU3Ed__28_MoveNext_m79ADB24A9F941386C16E91D2E7C7C7B5D071D476 (void);
// 0x000001B0 System.Object Mainspace.ScenesManager/<LoadMenu>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadMenuU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD0B1FF8419F91341A03CF453DDF1D7BBA16C16AB (void);
// 0x000001B1 System.Void Mainspace.ScenesManager/<LoadMenu>d__28::System.Collections.IEnumerator.Reset()
extern void U3CLoadMenuU3Ed__28_System_Collections_IEnumerator_Reset_mF54E18EE6E6C2D73AA2A862B2003F8A6FF3873BF (void);
// 0x000001B2 System.Object Mainspace.ScenesManager/<LoadMenu>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CLoadMenuU3Ed__28_System_Collections_IEnumerator_get_Current_m8C521593D1D04B269A3554CC7C18CEB8E5C17F28 (void);
// 0x000001B3 System.Void Mainspace.ScenesManager/<LoadBattleRout>d__29::.ctor(System.Int32)
extern void U3CLoadBattleRoutU3Ed__29__ctor_m49F6DDD14DC52A8E2123C7F81BED86B23CB4A997 (void);
// 0x000001B4 System.Void Mainspace.ScenesManager/<LoadBattleRout>d__29::System.IDisposable.Dispose()
extern void U3CLoadBattleRoutU3Ed__29_System_IDisposable_Dispose_m75BE0F49721F852BAEEBCEC8EE7B1B74CBF57D76 (void);
// 0x000001B5 System.Boolean Mainspace.ScenesManager/<LoadBattleRout>d__29::MoveNext()
extern void U3CLoadBattleRoutU3Ed__29_MoveNext_m8E9B6A2FF17CD5874E0DD1ED52B97F63E2A242F4 (void);
// 0x000001B6 System.Object Mainspace.ScenesManager/<LoadBattleRout>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadBattleRoutU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1BF2ABDA675CFC41535E484C711B664B0E3FE2F3 (void);
// 0x000001B7 System.Void Mainspace.ScenesManager/<LoadBattleRout>d__29::System.Collections.IEnumerator.Reset()
extern void U3CLoadBattleRoutU3Ed__29_System_Collections_IEnumerator_Reset_mEA0F3257C44B1B2EE12C96B014A43D9F5C06BA3C (void);
// 0x000001B8 System.Object Mainspace.ScenesManager/<LoadBattleRout>d__29::System.Collections.IEnumerator.get_Current()
extern void U3CLoadBattleRoutU3Ed__29_System_Collections_IEnumerator_get_Current_mC09B797D42E91CDDD03F19717E8BD5EFBD03573C (void);
// 0x000001B9 System.Void Mainspace.ScenesManager/<LoadBundle>d__34::.ctor(System.Int32)
extern void U3CLoadBundleU3Ed__34__ctor_mA0E45B9A1CAC6DF82370452648B4E06B8C8D4AB2 (void);
// 0x000001BA System.Void Mainspace.ScenesManager/<LoadBundle>d__34::System.IDisposable.Dispose()
extern void U3CLoadBundleU3Ed__34_System_IDisposable_Dispose_mD7EDC1DDC94917E540440B118748896313146141 (void);
// 0x000001BB System.Boolean Mainspace.ScenesManager/<LoadBundle>d__34::MoveNext()
extern void U3CLoadBundleU3Ed__34_MoveNext_m4C9D6EADA30FDA46E21C9B01D3DBB4C359A0859C (void);
// 0x000001BC System.Void Mainspace.ScenesManager/<LoadBundle>d__34::<>m__Finally1()
extern void U3CLoadBundleU3Ed__34_U3CU3Em__Finally1_m3ADE189D4915A6B08129BF8C6E2B9B00C4DB5ADE (void);
// 0x000001BD System.Object Mainspace.ScenesManager/<LoadBundle>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadBundleU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC471A946990DDDC262A7554F9921392ED0015CB (void);
// 0x000001BE System.Void Mainspace.ScenesManager/<LoadBundle>d__34::System.Collections.IEnumerator.Reset()
extern void U3CLoadBundleU3Ed__34_System_Collections_IEnumerator_Reset_mC3E16195AF7E2154763F4D93431FE337E2BCF4F7 (void);
// 0x000001BF System.Object Mainspace.ScenesManager/<LoadBundle>d__34::System.Collections.IEnumerator.get_Current()
extern void U3CLoadBundleU3Ed__34_System_Collections_IEnumerator_get_Current_mDFF6DFD397416B0B367B626FC77730FB584084D0 (void);
// 0x000001C0 System.Void Mainspace.ScenesManager/<GetBundlesVersionInfo>d__35::.ctor(System.Int32)
extern void U3CGetBundlesVersionInfoU3Ed__35__ctor_mDDE8FD2C504647E80E61EA74994A1088842B0AFB (void);
// 0x000001C1 System.Void Mainspace.ScenesManager/<GetBundlesVersionInfo>d__35::System.IDisposable.Dispose()
extern void U3CGetBundlesVersionInfoU3Ed__35_System_IDisposable_Dispose_m51692A5E82E73E57C977E6505ACFE136F76DDCEE (void);
// 0x000001C2 System.Boolean Mainspace.ScenesManager/<GetBundlesVersionInfo>d__35::MoveNext()
extern void U3CGetBundlesVersionInfoU3Ed__35_MoveNext_m80A91D071D4E6588465CDA28B26BF666AEE49EC3 (void);
// 0x000001C3 System.Void Mainspace.ScenesManager/<GetBundlesVersionInfo>d__35::<>m__Finally1()
extern void U3CGetBundlesVersionInfoU3Ed__35_U3CU3Em__Finally1_m06621A51E45F0E9A2C740AA696CBAAD2B69A9573 (void);
// 0x000001C4 System.Object Mainspace.ScenesManager/<GetBundlesVersionInfo>d__35::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetBundlesVersionInfoU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m57156DE3AB27AB6D386535BCB919FFB23D356260 (void);
// 0x000001C5 System.Void Mainspace.ScenesManager/<GetBundlesVersionInfo>d__35::System.Collections.IEnumerator.Reset()
extern void U3CGetBundlesVersionInfoU3Ed__35_System_Collections_IEnumerator_Reset_m09D18BFEF78670C3C13CD082C67704B45BB9B75D (void);
// 0x000001C6 System.Object Mainspace.ScenesManager/<GetBundlesVersionInfo>d__35::System.Collections.IEnumerator.get_Current()
extern void U3CGetBundlesVersionInfoU3Ed__35_System_Collections_IEnumerator_get_Current_m5C57BB4F73F450C9DA8370F4F95A126E7ADF487D (void);
// 0x000001C7 System.Boolean Mainspace.SoundsPlayerManager::get_ManagerLoaded()
extern void SoundsPlayerManager_get_ManagerLoaded_m9414366FE26AB2E3DA2A51F41BF130FB941BD6DD (void);
// 0x000001C8 System.Void Mainspace.SoundsPlayerManager::set_ManagerLoaded(System.Boolean)
extern void SoundsPlayerManager_set_ManagerLoaded_m19ECE5D36E48CADC44A635B447A3118F27CEACB2 (void);
// 0x000001C9 System.Boolean Mainspace.SoundsPlayerManager::get_IsMusicOn()
extern void SoundsPlayerManager_get_IsMusicOn_m1B4AE98BA33C0B42882EAE5DA37784ADDD1A85C7 (void);
// 0x000001CA System.Void Mainspace.SoundsPlayerManager::set_IsMusicOn(System.Boolean)
extern void SoundsPlayerManager_set_IsMusicOn_m76345AC43E617E1DF6892232BFE151BE108EF821 (void);
// 0x000001CB System.Boolean Mainspace.SoundsPlayerManager::get_IsSfxOn()
extern void SoundsPlayerManager_get_IsSfxOn_m34E199685D320CE232180EAC17708945FB22ED31 (void);
// 0x000001CC System.Void Mainspace.SoundsPlayerManager::set_IsSfxOn(System.Boolean)
extern void SoundsPlayerManager_set_IsSfxOn_m454038EABCA58D8C1169C251EB29032758180BE4 (void);
// 0x000001CD System.Void Mainspace.SoundsPlayerManager::InitManager()
extern void SoundsPlayerManager_InitManager_mA490057E1979AA812171B450CE5429451BF48303 (void);
// 0x000001CE System.Void Mainspace.SoundsPlayerManager::ReloadManager()
extern void SoundsPlayerManager_ReloadManager_m8E4D9167DC61190551DF48B66DCFBE0510539CB3 (void);
// 0x000001CF System.Void Mainspace.SoundsPlayerManager::PlaySound(Mainspace.SoundName)
extern void SoundsPlayerManager_PlaySound_m73CCAA90E9ACBD4D9E9524084012898F787997DB (void);
// 0x000001D0 System.Void Mainspace.SoundsPlayerManager::StopPlayingSound(Mainspace.SoundName)
extern void SoundsPlayerManager_StopPlayingSound_m955084EFC869E3A3D1CA1542141B368C6710C94C (void);
// 0x000001D1 System.Void Mainspace.SoundsPlayerManager::PlayLoopingSound(Mainspace.SoundName)
extern void SoundsPlayerManager_PlayLoopingSound_m0A2F74BF5F943C93E6F775CFEBE5C20F39BB347C (void);
// 0x000001D2 System.Void Mainspace.SoundsPlayerManager::MusicSettingsChanged(GameEvents.Menu.MusicSettingsChanged)
extern void SoundsPlayerManager_MusicSettingsChanged_mA14729B6936FD71170E6A2AF47E3312318450C54 (void);
// 0x000001D3 System.Void Mainspace.SoundsPlayerManager::SfxSettingsChanged(GameEvents.Menu.SfxSettingsChanged)
extern void SoundsPlayerManager_SfxSettingsChanged_m2DBFE84BC53DFAF30EB0269C77792C9E75938801 (void);
// 0x000001D4 System.Void Mainspace.SoundsPlayerManager::TryPlayMenuMus(GameEvents.Menu.MenuSeceneLoaded)
extern void SoundsPlayerManager_TryPlayMenuMus_mB09CBE076C0C8F4CCC872697703AA3E830235E6F (void);
// 0x000001D5 System.Void Mainspace.SoundsPlayerManager::TryPlayBattleMus(GameEvents.Battle.BattleSceneLoaded)
extern void SoundsPlayerManager_TryPlayBattleMus_mD64963AB53388E8A8FB1395FD85129EE2A88B51B (void);
// 0x000001D6 UnityEngine.AudioSource Mainspace.SoundsPlayerManager::GetSource()
extern void SoundsPlayerManager_GetSource_mBB708DB1715892EDB160E436AD74E3F92F7223A0 (void);
// 0x000001D7 System.Void Mainspace.SoundsPlayerManager::.ctor()
extern void SoundsPlayerManager__ctor_m700A9BC79469C4848662333E4EC231D13754C6F4 (void);
// 0x000001D8 System.Boolean Mainspace.IManager::get_ManagerLoaded()
// 0x000001D9 System.Void Mainspace.IManager::ReloadManager()
// 0x000001DA System.Void Mainspace.IManager::InitManager()
// 0x000001DB T Mainspace.Singleton`1::get_Instance()
// 0x000001DC System.Void Mainspace.Singleton`1::KillThis()
// 0x000001DD System.Void Mainspace.Singleton`1::.ctor()
// 0x000001DE System.Void Mainspace.PlayerController::Start()
extern void PlayerController_Start_mF64B77CC329DE34A8045AE0C940B514587556DB8 (void);
// 0x000001DF System.Void Mainspace.PlayerController::OnDestroy()
extern void PlayerController_OnDestroy_m5985E026FE030644DBEEBDA8C3397D1778B3E739 (void);
// 0x000001E0 System.Void Mainspace.PlayerController::Update()
extern void PlayerController_Update_m648FD430DD4F92DDFB8DF8D6A43B5EB18568CB82 (void);
// 0x000001E1 System.Void Mainspace.PlayerController::DeviceInput()
extern void PlayerController_DeviceInput_m7383D4E09D3E1A24F9E0FEFD8DD9153C9D8E46A1 (void);
// 0x000001E2 System.Void Mainspace.PlayerController::UnityInput()
extern void PlayerController_UnityInput_m5A04F6A17CD494367146B37FFAAF560E143DBFFA (void);
// 0x000001E3 System.Void Mainspace.PlayerController::SetRandoHat()
extern void PlayerController_SetRandoHat_mB20BEDCF3A08A340653C305FA45DEA2D355B1B7F (void);
// 0x000001E4 System.Void Mainspace.PlayerController::OnTriggerEnter(UnityEngine.Collider)
extern void PlayerController_OnTriggerEnter_mB4A82DDBE0722DF3770696CEAAA9430ADCF39BB4 (void);
// 0x000001E5 System.Void Mainspace.PlayerController::PlayerDie()
extern void PlayerController_PlayerDie_m5AF9D3B1B42627E2833BA81423C3ABECEE778480 (void);
// 0x000001E6 System.Void Mainspace.PlayerController::GreedDeath(GameEvents.Battle.GreedHp0)
extern void PlayerController_GreedDeath_mD3FCDA1BB8A23CE9E13D9EF2B0B3F759FF021403 (void);
// 0x000001E7 System.Void Mainspace.PlayerController::StartRun(GameEvents.Battle.StartGame)
extern void PlayerController_StartRun_m538A2496290D2C3CACA749D95E894FF7C17F1A2A (void);
// 0x000001E8 System.Void Mainspace.PlayerController::StopRun(GameEvents.Battle.Pause)
extern void PlayerController_StopRun_m7D55B447F8102133C2EFCFBB17366D61710A5402 (void);
// 0x000001E9 System.Void Mainspace.PlayerController::ContinueRun(GameEvents.Battle.Unpause)
extern void PlayerController_ContinueRun_mCBE1F8739BA551FE2A80673560617F2794AB48AD (void);
// 0x000001EA System.Collections.IEnumerator Mainspace.PlayerController::RunRout()
extern void PlayerController_RunRout_mA22B2A74036B92E6C4B3B76B212981F7F44AE3D4 (void);
// 0x000001EB System.Collections.IEnumerator Mainspace.PlayerController::SayHello()
extern void PlayerController_SayHello_mA637BCB7DB14816CCBC35803E77F9CF1B5385C26 (void);
// 0x000001EC System.Collections.IEnumerator Mainspace.PlayerController::Rotator()
extern void PlayerController_Rotator_m441BB00CC5F6329CAF42EE05CD76279368F9ED09 (void);
// 0x000001ED System.Void Mainspace.PlayerController::.ctor()
extern void PlayerController__ctor_m13056FC83D815319EB5775895D761DA8CC5DF7BE (void);
// 0x000001EE System.Void Mainspace.PlayerController::.cctor()
extern void PlayerController__cctor_m1787AFA429A030E78DE7E659642C31C17D83BF2F (void);
// 0x000001EF System.Void Mainspace.PlayerController/<RunRout>d__30::.ctor(System.Int32)
extern void U3CRunRoutU3Ed__30__ctor_mCA94540393D6FF23BFAEE95F33AC7F75D69CDAF9 (void);
// 0x000001F0 System.Void Mainspace.PlayerController/<RunRout>d__30::System.IDisposable.Dispose()
extern void U3CRunRoutU3Ed__30_System_IDisposable_Dispose_mD9B16A132D4A399D11929531A6CE9AFBC63EA4EE (void);
// 0x000001F1 System.Boolean Mainspace.PlayerController/<RunRout>d__30::MoveNext()
extern void U3CRunRoutU3Ed__30_MoveNext_m0D1027224C343700B1363E500361CEAD8291BBBD (void);
// 0x000001F2 System.Object Mainspace.PlayerController/<RunRout>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRunRoutU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m205313068F1B73E8EA33743E806702693E22766D (void);
// 0x000001F3 System.Void Mainspace.PlayerController/<RunRout>d__30::System.Collections.IEnumerator.Reset()
extern void U3CRunRoutU3Ed__30_System_Collections_IEnumerator_Reset_m5B8DA141B9C088A61F3302CB6C7F279A7E475A88 (void);
// 0x000001F4 System.Object Mainspace.PlayerController/<RunRout>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CRunRoutU3Ed__30_System_Collections_IEnumerator_get_Current_m7EEC891E87CA1E9B2C39B4355D7F4553F0C9D0A8 (void);
// 0x000001F5 System.Void Mainspace.PlayerController/<SayHello>d__31::.ctor(System.Int32)
extern void U3CSayHelloU3Ed__31__ctor_mA603E85A8DB0B4D7B67E720F7530EC6BBA9DDF7E (void);
// 0x000001F6 System.Void Mainspace.PlayerController/<SayHello>d__31::System.IDisposable.Dispose()
extern void U3CSayHelloU3Ed__31_System_IDisposable_Dispose_m23FC1D002C29FCD2BF7E5430B56A3D92BF2C3816 (void);
// 0x000001F7 System.Boolean Mainspace.PlayerController/<SayHello>d__31::MoveNext()
extern void U3CSayHelloU3Ed__31_MoveNext_mF8C6CF4333D756B4EEE05C30FC9A9093B373546B (void);
// 0x000001F8 System.Object Mainspace.PlayerController/<SayHello>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSayHelloU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m788DCE676AD27D2E45809A95C2D721FF39525713 (void);
// 0x000001F9 System.Void Mainspace.PlayerController/<SayHello>d__31::System.Collections.IEnumerator.Reset()
extern void U3CSayHelloU3Ed__31_System_Collections_IEnumerator_Reset_m0CE46B566DA780F05F8FEC06127AB405182FA7E2 (void);
// 0x000001FA System.Object Mainspace.PlayerController/<SayHello>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CSayHelloU3Ed__31_System_Collections_IEnumerator_get_Current_mACA149E6143CEEC49B09738A94AEC131886D2F80 (void);
// 0x000001FB System.Void Mainspace.PlayerController/<Rotator>d__32::.ctor(System.Int32)
extern void U3CRotatorU3Ed__32__ctor_m48279ADDC8D8C49C9C9B8B818F1CA7CBAF0C1057 (void);
// 0x000001FC System.Void Mainspace.PlayerController/<Rotator>d__32::System.IDisposable.Dispose()
extern void U3CRotatorU3Ed__32_System_IDisposable_Dispose_mB23FB46D08F56888E6913A5D78182521004F0F81 (void);
// 0x000001FD System.Boolean Mainspace.PlayerController/<Rotator>d__32::MoveNext()
extern void U3CRotatorU3Ed__32_MoveNext_m91FEBB66B89346CB884EAF429A47B7CB5C16655E (void);
// 0x000001FE System.Object Mainspace.PlayerController/<Rotator>d__32::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRotatorU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0114479E05754C49ECF28B18186E2E2C8682AD31 (void);
// 0x000001FF System.Void Mainspace.PlayerController/<Rotator>d__32::System.Collections.IEnumerator.Reset()
extern void U3CRotatorU3Ed__32_System_Collections_IEnumerator_Reset_m1FF085920A616461360A3512B2185E8CE39381A3 (void);
// 0x00000200 System.Object Mainspace.PlayerController/<Rotator>d__32::System.Collections.IEnumerator.get_Current()
extern void U3CRotatorU3Ed__32_System_Collections_IEnumerator_get_Current_mB4B770B1451A7086036EFAB0EC8056DFE025FAB5 (void);
static Il2CppMethodPointer s_methodPointers[512] = 
{
	ColorExtensions_ChangeAlpha_mC3CC7B4AE0E805D7101B9A31F686BA42DB3AF9FC,
	CoroutineExtensions_End_m517E0787B1E601B3B2E985FA47A1690FB9C57202,
	DateTimeExtensions_TimeStamp_mC3066A49B5AF458185559BBC314A323D68923E34,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	FloatExtensions_Sign_m4FCF92814F87B927385F0B7C06A5253CBE787BFF,
	FloatExtensions_IsPositive_m56E710C581B36D7E5BB1C87D5B93B554B6E144B9,
	FloatExtensions_ToTimeString_m7476A4FFB42E9C3E1CC9FBB461948769AFA00897,
	GameObjectExtensions_DestroyIfSingletonExist_m2057689DFF6902B1D0E38402274E19371EDA74CF,
	IntExtensions_Sign_mEB374FC99F30A2BC1ACD044FD33B8DC406F9175A,
	IntExtensions_IsPositive_mC99FA2A59F97A83B5636EE374566A000C3954F8B,
	NULL,
	NULL,
	NULL,
	ParticleSystemExtensions_exFullStop_m611C3ADD0DD46034D10439A26365D53CF195FE0C,
	ParticleSystemExtensions_exFullReset_m8CC35A22CD32BF1AC5FCA09D6D9990C2733E3035,
	ParticleSystemExtensions_exFullClear_mC09E34569DC27BABFB2EE01E34A4E5D9761570F8,
	ParticleSystemExtensions_exSetColor_m1711396E1D9B438D40258E2C02801EB5FE7DA7F9,
	ParticleSystemExtensions_ResetAndPlay_m5E0F57B158F49C0DFFFDC6A09AA42723B675718B,
	ParticleSystemExtensions_exFullClear_m8DE597433DED9D59E7AB1FA06B1BDA0371C2D71D,
	SpriteExtensions_getUvOffset_mDCB6E2B56CB261598C275D2EEBF5A3CCB56712F3,
	StringExtensions_Reverse_m921801DC6F50A3C8D3A5C50E2BD3E56A964D6806,
	TimeExtensions_ToShortForm_m495F5B791C0C842B7C045D19BD4A28BA256F17DF,
	TimeExtensions_ToMegaShortForm_m0552A35DB7EF0ECDFE80FC12082116AAC9B10180,
	TransformExtensions_SelfDestroy_m37BF9588254B27591768734A57AE42637B6C2061,
	TransformExtensions_DestroyChildren_mA936B8276E3B8037E2A0564903923F0039ECD9FE,
	TransformExtensions_DestroyChildrenNow_m1B49213F608A5FF49CF742D4B2C1FD8C3D8AAD6B,
	TransformExtensions_exContains_m86563D73F6C59BD08C28DD3D0740AE331E1F8EC6,
	TransformExtensions_exContains_m35F876DB602247DCAE89E2230E0FAB83919BF0A4,
	TransformExtensions_exIndex_m29F4E08C5007EDD88E1E04FF545BDA782FBAEAFF,
	VectorExtensions_Distance_mE7B73E7D665A6C4EF406964DDF122A5C6D406F82,
	VectorExtensions_ToV3_mEEC4EE674FCE50FF782B34554FAD934BA6A25089,
	VectorExtensions_ToV3_m96F471D1DAD72302AD1F22EBF54B52AD1213744F,
	VectorExtensions_Abs_m0C727BD6192E0F080F46BF5E3C17551347FBFF8F,
	VectorExtensions_Div_mA05A365EC50132940174346D7B03E0B09352CCB1,
	VectorExtensions_RndDirection_m0E294BA5C43072D8FC38230852E58506B2C2BC37,
	VectorExtensions_RndDir_mF1C0701C304D402D59BBDD060591B2E2FF3A5316,
	VectorExtensions_Mirror_m1C4157865EAACC117CE8503CFC6F1F4B95DF3C4E,
	VectorExtensions_SwapCoord_mB1C58FE21EC7D3F0E297309CAD6BAA5CF5C32FE8,
	VectorExtensions_SwapCoord_m8D37CE7CA45E4AC7833C7FE6B98E1772CD1AD58F,
	VectorExtensions_AddCoord_m03E0EF7A6F887BBE97A986B702C88256FC8FE184,
	VectorExtensions_Zero_m9FA2283836BE1DD565A1A3441E018E34F041D1FB,
	VectorExtensions_RndShft_m650D9A88BA0DC7D81A2846828B78B4B4741845A9,
	VectorExtensions_RndAbsShift_m54D3E249B07A759147A68FE962C1FF2F8D95C6AE,
	ShadowRotator_Start_mCA1719DD579ED8AA668E58EFC5E88700B671DC75,
	ShadowRotator_Update_m4B05DFCDA3699F109B4961F1EBE636B5FEBC8287,
	ShadowRotator__ctor_m65C7AEB3D8CB910461C5CEB05968F4891A7BA63C,
	GameHandler_Start_m78B61BE18B716B2039EAE293F99DFD5CC0219FF2,
	GameHandler_Update_m5966CB0A43519F7047D8E110AF3599540F44620B,
	GameHandler__ctor_m99B62501A237CFBFE117E822D074F35D036E79A2,
	ScreenshotHandler_Awake_m481871FBCF725B56BF967CCD0C40AD1E1B06AE95,
	ScreenshotHandler_OnPostRender_m5A70CDB1F59DCE3A932BACE11D997CF8A0E653F0,
	ScreenshotHandler_TakeScreenshot_m659DD76068C15BF7DAB31C562476BBC5E57966B2,
	ScreenshotHandler_TakeScreenshot_Static_m965ADA18BD02B605FF093ADE55C48E2092B7DFA3,
	ScreenshotHandler__ctor_mCA9D5ABB96208972B55127BB416A1B30A5775D97,
	examplescene_Start_mF772F5209BBADE7B1564528A598B9AE0E8521E56,
	examplescene_Update_m80C52A0676D6A51B393E4B51CE620AE1C568CEA6,
	examplescene_OnGUI_m451FA489D413D0A793B8227D22D5D9A10C3E9669,
	examplescene__ctor_m8813A9F768BE489C957DE34046DCA616E4F7B1C9,
	ETFXProjectileScript_Start_mE8756011C883DDCF287343C6BF881B36B7ADB59C,
	ETFXProjectileScript_FixedUpdate_m959BFE684CC8CF77305ABF0C54256007023E2FFD,
	ETFXProjectileScript__ctor_mDB3BFAED1E91A0DC99930E80169BF2F1EE26CFB5,
	ETFXSceneManager_LoadScene2DDemo_m11E37C513C423B288C88C336A3F77E846AA6B358,
	ETFXSceneManager_LoadSceneCards_m2374101144F9C56EB968B3A3B7BEACA75BB21C03,
	ETFXSceneManager_LoadSceneCombat_mDA5B85860C1746C93E4D81CE992E7C4887D89E0C,
	ETFXSceneManager_LoadSceneDecals_m0D919CCFC190AAB40C20D990A8DEBBDF79075F2E,
	ETFXSceneManager_LoadSceneDecals2_mEFA6A5777999D5F3D87E1E8226A361880433DCF6,
	ETFXSceneManager_LoadSceneEmojis_m01E3A7533C8A4D8CF6115E0EE1DC687455AB4D4E,
	ETFXSceneManager_LoadSceneEmojis2_m6005349247D781727A558BA01D2085C893AF6438,
	ETFXSceneManager_LoadSceneExplosions_m1984686B3E5CE79E49BDDA11BDD09596F8E7C42E,
	ETFXSceneManager_LoadSceneExplosions2_mB04546364A96740B6CA14B50B2DCC3E80C28CF90,
	ETFXSceneManager_LoadSceneFire_m9E31C2B57B979A46E2C6F727C858947CF9C0EA19,
	ETFXSceneManager_LoadSceneFire2_mEAE89BF11E50292B18F69A327923ED3A8B652256,
	ETFXSceneManager_LoadSceneFire3_mD9919F751EC9CFEF0D95691414A042A22B55C831,
	ETFXSceneManager_LoadSceneFireworks_m2827DAD7FCC3996841900263F223F5201E8A3C43,
	ETFXSceneManager_LoadSceneFlares_mD740C35453DD10F374658441ED58411126845D38,
	ETFXSceneManager_LoadSceneMagic_m225C27B1375FCB1B4C215A3D67BDF33C01BDE04E,
	ETFXSceneManager_LoadSceneMagic2_m08E7D2EF64DD9F9534B7525E3905324264DCAAC7,
	ETFXSceneManager_LoadSceneMagic3_m58FC3634AA94E283420B22E080D9E91A2A52A737,
	ETFXSceneManager_LoadSceneMainDemo_m030D3019FECBBAF948E3FAC03B7229FAAF0839C4,
	ETFXSceneManager_LoadSceneMissiles_mEE0267B462F61EC2B7BD88460F69CEE7139A6E8D,
	ETFXSceneManager_LoadScenePortals_m4932DCDC59F52F6B75F7E00FE384C15CAACD4371,
	ETFXSceneManager_LoadScenePortals2_mE5701BAD231A3926A5F3DD95E7FB029E9378C89C,
	ETFXSceneManager_LoadScenePowerups_m49AE6A1640136507D16DFF611DC78B4CDA28FEC2,
	ETFXSceneManager_LoadScenePowerups2_m548B7BFF09B8A133EF48FE2B130A91D796EC6A33,
	ETFXSceneManager_LoadSceneSparkles_m9463E60A6ED35D9897EEEF18D7B94880359B75A5,
	ETFXSceneManager_LoadSceneSwordCombat_m2040FBE4FE83D2256F5208F20D5A0A817472052E,
	ETFXSceneManager_LoadSceneSwordCombat2_m3C8D0447A800B6C4616D4CCE669711CC6236C13A,
	ETFXSceneManager_LoadSceneMoney_mE3B3B3433BE6C2E37B56CCF7CF0ACA0AF274B795,
	ETFXSceneManager_LoadSceneHealing_m73F368066D2E63E359E36079EB20A4B51006BA56,
	ETFXSceneManager_LoadSceneWind_mB2FF2390377FF263B5ACAEE93DD389C9D666BE2E,
	ETFXSceneManager_Update_mB2FB561ADC4764627A10DECF88F6C3F17891B9C1,
	ETFXSceneManager__ctor_mC6363CFDCD6B6646152AA5FEC471088DFC20367A,
	PEButtonScript_Start_mED60465004404B45D9A5BE8129015C39394C9DC0,
	PEButtonScript_OnPointerEnter_m89340DB88B2D0EF0CDD762F27C1EB455A6876CB6,
	PEButtonScript_OnPointerExit_m8F731E714619F43F056DD2D4E27A24B5B045774C,
	PEButtonScript_OnButtonClicked_m8D7DB17EB5C69D54E2F95931AB6C90A7490159B2,
	PEButtonScript__ctor_mBFA614FD383BF4104DE7ADEE7AD88A91AC8083D6,
	ParticleEffectsLibrary_Awake_mDC6AFDB7CD4E3DA4C5163BFDDCC04A76EED147CE,
	ParticleEffectsLibrary_Start_m82B0B1F30A8C0E6B1060FEEC21C5280500D7F771,
	ParticleEffectsLibrary_GetCurrentPENameString_m067B9C792580A9DFD2DE2B4318AEC99254BF5285,
	ParticleEffectsLibrary_PreviousParticleEffect_m4A6BB63833B9C4D91B2F2801741C731C8174836E,
	ParticleEffectsLibrary_NextParticleEffect_mD095F239D4054E69278390CA3A404D033F8A658E,
	ParticleEffectsLibrary_SpawnParticleEffect_m17AE1DA6F799AFF7DE1C64DC0ECF3D716E22355A,
	ParticleEffectsLibrary__ctor_m777070C2175441BEB93AFF11748BC74588651C7B,
	UICanvasManager_Awake_m8D84AB004D6FEEBB3BDC5FBB9C5101B75ACDC396,
	UICanvasManager_Start_m9652DFED3C5A6CD686ABFB2D77A474270D5F7656,
	UICanvasManager_Update_mA2771AB72770EC84D9D24FA819E57BB122AD7FC0,
	UICanvasManager_UpdateToolTip_m13510C3590867DC2E5B2F8D45E0BA522479103D7,
	UICanvasManager_ClearToolTip_mFA69FDBDB5CADD0F18A31D1AC407D38D81F2B3BF,
	UICanvasManager_SelectPreviousPE_m88ABF3CB63D07DBBEFCAE908864103FF09C21E30,
	UICanvasManager_SelectNextPE_m5838FA28DBEEC3DFF2A84E671842B86C366A2367,
	UICanvasManager_SpawnCurrentParticleEffect_m8E4B86D6B40EBB353D2EA0D33B22785FB3A55B1B,
	UICanvasManager_UIButtonClick_m0BE04EA2C0BA8E35CE5B22BFF4CBD4E9128B3CE4,
	UICanvasManager__ctor_m8BDA3D4D6FD64344E6CCBEF6F7B314DEE7180E22,
	FPSWalkerEnhanced_Start_m252E88E0B24056C673BB14447377688734E7B8E2,
	FPSWalkerEnhanced_FixedUpdate_m1473D52C9F5E268E25165FBE5F518E164748B3B1,
	FPSWalkerEnhanced_Update_mEA0CA073B5D3809A37EAB7C2281CF734F5531DC6,
	FPSWalkerEnhanced_OnControllerColliderHit_m19912E41ADB24963D0B31F1CC7DA6478C303073A,
	FPSWalkerEnhanced_FallingDamageAlert_mB445332F8682AFBDEBCF35666278BD745299E23F,
	FPSWalkerEnhanced__ctor_m1D5E7200BD8BEDDC09FDE08D0BB6B91D51BC7AFB,
	SmoothMouseLook_Update_m981BA5E8AB3B079E3B340A5051950642F3E1A109,
	SmoothMouseLook_Start_m94730D34E9DBB8830F51F6E3DE68A50B80C40DC8,
	SmoothMouseLook_ClampAngle_mEE917D4C3904EB31D4DDDD84D0FF1F216990C0FC,
	SmoothMouseLook__ctor_m41C510FC83836FF572D9767E8F8433B8BAB341D7,
	SkyboxChanger_Awake_m277CAAE5694E0F626BC667F43F17F9937A5CE0F8,
	SkyboxChanger_ChangeSkybox_mDDFA30D0CA245B27A82D0E3C499725B0C4A2A7A8,
	SkyboxChanger__ctor_mF2042E40C757632172F960BB139550F779D36080,
	SkyboxRotator_Update_m2E2891AA030A8BD0F61C1FDEF10D1E1C161C6DDE,
	SkyboxRotator_ToggleSkyboxRotation_mCB6825B6E7E9FE18DF218AC2306523E747BCBC5F,
	SkyboxRotator__ctor_m7BFD040FB7F3C18ABA7AED90C283AC89B46D7912,
	ETFXButtonScript_Start_m25A1DDC0BEB25CBCB20EB6FBE9E71F006138B3C1,
	ETFXButtonScript_Update_m22634A284FB8C975D9929F15988F09FA1ACDF332,
	ETFXButtonScript_getProjectileNames_mA13342862E48B4A8B6BC0666BC7EFE2BB5625D3E,
	ETFXButtonScript_overButton_mEFE1C511D8EEA91AE9E54B97CD0A155E542290B0,
	ETFXButtonScript__ctor_m65F38109C2CBF445E711A2C6646B34C2483E0455,
	ETFXFireProjectile_Start_m6586051FBDDC4104ED997E585ABEA089694B805D,
	ETFXFireProjectile_Update_m4F39FFB37EDCE10D6037FDE13FF274C027F60990,
	ETFXFireProjectile_nextEffect_m406DBBEAF4133E7C3660AB0F5BC32CD6B3B0C7C5,
	ETFXFireProjectile_previousEffect_m33C82A976F62A20D43E2FF8C57E3066DCB00AE5F,
	ETFXFireProjectile_AdjustSpeed_m0A76F61B5320E509827716B5B302AE41B028288A,
	ETFXFireProjectile__ctor_mB48108DD5AB58E5907180ED79690E217D8EB1D65,
	ETFXLoopScript_Start_mE67C5E09B7AB2A78688B1877DAC3766A0868CCA8,
	ETFXLoopScript_PlayEffect_m2EAE66A60E00C863C15BE46D10AC4522789A218E,
	ETFXLoopScript_EffectLoop_m728B76DFD989B681D2C93D7A88E9D76C9696CAF4,
	ETFXLoopScript__ctor_mC0B4A260353A71EC89B2E1ADA7834286E0F60E5B,
	U3CEffectLoopU3Ed__6__ctor_m553F1C586BBB6C68EEB9710F62438A8EDCE136DA,
	U3CEffectLoopU3Ed__6_System_IDisposable_Dispose_m8142A69BF60253C0139648E9350176EB9EFD8BFA,
	U3CEffectLoopU3Ed__6_MoveNext_mE11D3E615622E410D2DAF1960011C76F8ABC322D,
	U3CEffectLoopU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m93EE11485A4BA42D80F240C1703D673774889545,
	U3CEffectLoopU3Ed__6_System_Collections_IEnumerator_Reset_mB35745D308CDED6CAB69D52DEFC2D84AB6CC5896,
	U3CEffectLoopU3Ed__6_System_Collections_IEnumerator_get_Current_mB56FD34E205892C6B85E8FD18DAE68F19ECBDE2E,
	ETFXMouseOrbit_Start_m4FB8182859BA3A3D8139156DFFFF58A6001F9297,
	ETFXMouseOrbit_LateUpdate_m27A95AC3E473534D0DAD68B5B76579A0811D8CEC,
	ETFXMouseOrbit_ClampAngle_mA204BF97BDDC49A732917C75BDC934D4CF56B314,
	ETFXMouseOrbit__ctor_mB2E0C52CC44B7FE5D064C3183F9DED7714DDCBDC,
	ETFXTarget_Start_m60CA8E1079E17A7C9A47D10C9FA8AE9220DB1E9F,
	ETFXTarget_SpawnTarget_m00485209171AF5E1A8DB572C256C99B0E821BF96,
	ETFXTarget_OnTriggerEnter_m057AF200E64829505145549ED629FAA79935B1F5,
	ETFXTarget_Respawn_m679D0B0188B3DA281D2BE313E7212FC1D6F2B905,
	ETFXTarget__ctor_mD0F9BFFB2CC402DC5C6FF274EEDD58EB891A22C6,
	U3CRespawnU3Ed__7__ctor_m415513F665DE8F3777A3B21AF9C5B45A90CB1BF3,
	U3CRespawnU3Ed__7_System_IDisposable_Dispose_m63C5D414DB8D33EB86AFF6D5569B6C4E18C32615,
	U3CRespawnU3Ed__7_MoveNext_m58D6E6D0CFF4964FDE4083115A10119C4FB6FEE3,
	U3CRespawnU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF70DB558C29B7FFE4A790553093711C3478A2C54,
	U3CRespawnU3Ed__7_System_Collections_IEnumerator_Reset_m37DFF9115E528EA7997B687DD51321AB843D9BE7,
	U3CRespawnU3Ed__7_System_Collections_IEnumerator_get_Current_mA84FD67FBB5DE784889C67146E66570FC2E5D80E,
	ETFXLightFade_Start_m80EEDD11315E813676DA8E550C806F8C96374733,
	ETFXLightFade_Update_m3705B92646C8612BC70987DF39BC94B8F88EC49D,
	ETFXLightFade__ctor_mCDF41DC7334BC08F86DD3EF815FC1F3EA4C44F11,
	ETFXPitchRandomizer_Start_m64BE3B77C622908C2E324C6D3DFD17CB9092D21E,
	ETFXPitchRandomizer__ctor_m8493E0829884B225C1FC98134FA1EFCC44CDD238,
	ETFXRotation_Start_m3DC21DA9C26DBA1E14C27B9B997F46BEBBFB22C7,
	ETFXRotation_Update_mED37E3F435A6166C540A1DA30B49354F05279100,
	ETFXRotation__ctor_mC1C7849D64611953B026F069EE879C10B1399ABA,
	GreedBeh_Start_m37A027DE7AB581CEACE80F5AD2CCF9BD700AA225,
	GreedBeh_OnDestroy_mE81CC0941EDFE81287AB889554662A121745412A,
	GreedBeh_StartGame_m0B7D5597408C27ED8834B4B3CE96C2234B64C9EF,
	GreedBeh_StopGame_m794D4D2CA4AB1525FC98400D15A1AD29B1820F5F,
	GreedBeh_Pause_mD45FF1DE015FFE2500C7F8EFB5A2DCDFEAB175A0,
	GreedBeh_Unpause_m9CA7600C6AE26BD969EFA8ECAD208CCA3EF65C07,
	GreedBeh_StopRoutines_m33ECD4E704E9CAE7B0B704C33F7C3CE4A785D3A4,
	GreedBeh_GreedController_m9AD3972A3D4EDF41FE9B6BB5AAC75AF6071E14AD,
	GreedBeh_DifficultyApper_mD7A702FA04D913E24905B2B1082A39093F270381,
	GreedBeh_SomethingCollected_m5A4C00B6CB8EC9CB3FF336A7425169D95BDC53C7,
	GreedBeh_Punch_m5FE8B4C7B5606A61603938B72C205996FD28EC5F,
	GreedBeh__ctor_mE4C9FFDC5DEDBB9368AD549BCFAD2E17328FD3C5,
	U3CGreedControllerU3Ed__23__ctor_mED24E9A036599767030BB969BE03320D52DE3BB8,
	U3CGreedControllerU3Ed__23_System_IDisposable_Dispose_m778A80884DF8544DB4F7E374C965D73586794490,
	U3CGreedControllerU3Ed__23_MoveNext_mBF26CA0FE56398652BC8B53E190FA535CA4C924A,
	U3CGreedControllerU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8C0F4A03686C2661E36FC7B7A700315ADC72348F,
	U3CGreedControllerU3Ed__23_System_Collections_IEnumerator_Reset_m2114CC24AB04A2370B6F76FD9AE1FEB45F236F55,
	U3CGreedControllerU3Ed__23_System_Collections_IEnumerator_get_Current_mACA549512D9667723F550D555D208986BF10C8AD,
	U3CDifficultyApperU3Ed__24__ctor_mBA4F037F96B2F476A714F6DBDC8B7A2CA90287BC,
	U3CDifficultyApperU3Ed__24_System_IDisposable_Dispose_m134F71DF2B1632B26105A2275EABFABCE0E632F3,
	U3CDifficultyApperU3Ed__24_MoveNext_mF693A32CB9519D25CB8D6F97CE7124E9F1FED735,
	U3CDifficultyApperU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C742F57DD6CF120F66DD900101944356EC7B002,
	U3CDifficultyApperU3Ed__24_System_Collections_IEnumerator_Reset_m3A9261AFA76DEFAEF6A962E88D9F46E5ACBEC7E1,
	U3CDifficultyApperU3Ed__24_System_Collections_IEnumerator_get_Current_m8690A70EEAC79F6399EE311D2B93DC5F7AA85198,
	CameraBeh_Start_m2D4A3857C1AED278AE858524A1EE52DAC86CD43D,
	CameraBeh_LateUpdate_m0E7FFAAA766F5F77BDCD43B0E3E8ED8197F1BF7B,
	CameraBeh__ctor_m65480E2C59A6682B181A1D7C2F5207945DC3C29B,
	CoinBeh_Awake_m01D2140F8F9DE8E80FD63358896F320793C432C8,
	CoinBeh_OnEnable_m884DE0E8AC5A790C61FF43764EF267E934C11A42,
	CoinBeh_Rotator_m40AA8668E10BFADA112DA289E78E735E46B7E4D5,
	CoinBeh_Fall_m4200C6B1973602768A994122AFBAF516C305FDC7,
	CoinBeh_PlayOnCollionVFX_m6E170B523E220A38C3A3B7BD8D7EA44935C93CF5,
	CoinBeh_PlayStaticVFX_m7E71482DE74F9B07E65E5E3FE20EDFC9C12EDCAF,
	CoinBeh_CollectVFX_m556BDCB482694FA8BEE78E1C9DB25F4F6D58738E,
	CoinBeh_DelayedDie_mF36E21BA2FF53B40C20F22FAC0460B9972D72EAA,
	CoinBeh_Colected_mBE61F5482F2316C16268A479E508214A8DF81D6B,
	CoinBeh_Die_m46CCF4DEB2F99B8DAAC8813DE86104E7066CF98A,
	CoinBeh_OnTriggerEnter_mF0AC5F3DC88438F55BBE1133DCC9BDB3A97BCE99,
	CoinBeh__ctor_m38F4E15B9888DAA8FAE1EB626F4C314EE2FFA6B3,
	U3CRotatorU3Ed__17__ctor_m114D28EEABEDC896272B845F7A91892F68306FFE,
	U3CRotatorU3Ed__17_System_IDisposable_Dispose_m6315BB8051C0C7EE5D1625FC4E03031B2219CD05,
	U3CRotatorU3Ed__17_MoveNext_mD068A7E198C1F87088ACCFBE4336AC0F57F74F20,
	U3CRotatorU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8A3D8CB7F45697A23FC98BAB9AA05B83C4A447CC,
	U3CRotatorU3Ed__17_System_Collections_IEnumerator_Reset_m81DCA7E77FC0A97396961845CDD5DCBA2F9C5154,
	U3CRotatorU3Ed__17_System_Collections_IEnumerator_get_Current_m9CE4E05E678854976D2A6843AE0B6262F41E8693,
	U3CFallU3Ed__18__ctor_mA1BD6BEE887A2EE42B90B2F75315E7E1FB4EC535,
	U3CFallU3Ed__18_System_IDisposable_Dispose_mF309B4C364F44A922728EA07BDA6CF9635CFA32F,
	U3CFallU3Ed__18_MoveNext_m02659C42F0AFF67C3FA36961639FF26B776EAAFE,
	U3CFallU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAC3583E0B3BBC249538DAFF1FC5D2A1D0F69BC0F,
	U3CFallU3Ed__18_System_Collections_IEnumerator_Reset_m9D7DEE6CEADC090BFB3D63C3937B4341E28F3A8D,
	U3CFallU3Ed__18_System_Collections_IEnumerator_get_Current_m1211A1CDA0C02D5916843A755AE7A032E0954ADE,
	U3CDelayedDieU3Ed__22__ctor_m83D599148B986FE4A385354A2414B1B1F207E131,
	U3CDelayedDieU3Ed__22_System_IDisposable_Dispose_mC5D8224A1F4D3929672FDDE8AD96BE060E09CAFA,
	U3CDelayedDieU3Ed__22_MoveNext_mB4213CBB5B8B28DED206A67A468446769290F729,
	U3CDelayedDieU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAD32D4B0BCD495AC64F7C0E2B6B380AA6D349240,
	U3CDelayedDieU3Ed__22_System_Collections_IEnumerator_Reset_m787F6488D83FA9BC2AD52E672155FEB4EA8658C7,
	U3CDelayedDieU3Ed__22_System_Collections_IEnumerator_get_Current_mA3ED1713C218EF84A4DC28FB92BA6E398DF2C36C,
	CounterBeh_SetValue_m45E2A922029303B7A0C1D58EB7E57F53D12CB3EC,
	CounterBeh_Start_mCCB2E0A232D8A997BCD9C99DEC0548CDBEBC76DE,
	CounterBeh_OnDestroy_m6409272BFA70967C280983417813B4DD29F8266C,
	CounterBeh_UpdateCounter_m9BCEC3577B12A30D4BD5947AA7A569D28D137DD0,
	CounterBeh_LerperNumber_m599BE2B23127C037AA97382BA0BA024FFF2D3D2F,
	CounterBeh_Punch_m5F37EA170C05200CDFDAA54C5989632817422342,
	CounterBeh_SendResult_mA7E11FC19354244D39AB5B57BA95A7DEB883D8A0,
	CounterBeh__ctor_m3D022E9113F17CECD96158366DAEF25962E8768E,
	U3CLerperNumberU3Ed__11__ctor_mD9FD4C2897BEC16FC1A9C2B18F6FE69619970BE8,
	U3CLerperNumberU3Ed__11_System_IDisposable_Dispose_m6660869AC0EA25A327B028E9F253931B2072A96B,
	U3CLerperNumberU3Ed__11_MoveNext_m438653738F78C50FE8C47AC3A54E8320AF9A1A99,
	U3CLerperNumberU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB9730D565D67363A535F32E2EA770B2950AF17ED,
	U3CLerperNumberU3Ed__11_System_Collections_IEnumerator_Reset_m868765C8474E200FCC40548774D832A1BB02DD86,
	U3CLerperNumberU3Ed__11_System_Collections_IEnumerator_get_Current_mB0792E3850208F01A09783A4C819C80F9558CF92,
	PropBeh_OnEnable_mED626DF56D3F705182C190FBEB02647E36B1D126,
	PropBeh_Update_mBBBD0FF57DF4C56231B0F42B3AA5677A38F85D0A,
	PropBeh__ctor_m1DEB1BF0FF2117AAFF4F636D859F3954B3470BAE,
	ShadowBeh_Init_mFA0429FB0CB028F969261909A1ACEB03702D5446,
	ShadowBeh_SetPos_m2D863ED8A0A771CC93804803BD4D31868BA0A157,
	ShadowBeh_SetSize_mD2F466F246495E05354166D65B4AA6A1CB4E3D22,
	ShadowBeh_Store_mCFCE63BA58298796A4E5BD6E82BCEAF6F7E9C368,
	ShadowBeh__ctor_mA9D1C7071FB406D3BA4C8087F055C36F2B0C4F42,
	BundleInfo__ctor_mF4DB7CACD68EBEE05A80861D744354744816008F,
	ButtonBehaviour_Start_m049E3E93A130B44EA85CCFE19A6434A18A95EB63,
	ButtonBehaviour_OnDestroy_mD6D9A99C2B5C795E055E6E001D12EDF5CDB28DEC,
	ButtonBehaviour_ButtonOff_m6EEBD45A64D5C12A9909F299F4FAE53A6EDF0CC3,
	ButtonBehaviour_ButtonOn_m6C3208E028821C06A24D81E29A835BE2D20B0F3F,
	ButtonBehaviour_SetButtunInteractibility_mE489AE585AAC4814F04ADFB1F16C3EB6F29C78E3,
	ButtonBehaviour_Punch_m6B84826B8C58B2751DAB82FB4D025C38EDB42EB2,
	ButtonBehaviour_PingPong_m8803139DB310C3BAAECD057565E16FE8AB9B429F,
	ButtonBehaviour__ctor_m8D8AACDAA926C024974434FE7B399320F8C2A79B,
	U3CU3Ec__cctor_m1D8E1CE79E22EC1DADB8C6B130CF56CF4F0FD392,
	U3CU3Ec__ctor_m7E06856CE747177228F8B1A739141903D0CA8702,
	U3CU3Ec_U3CStartU3Eb__3_0_m2BDB301B5FA71EE500D1024242D28F3C7BBFD611,
	CheckBoxBeh_Start_m0767B99C3D7E1600C6CDE0C039D9CA09E19D6D78,
	CheckBoxBeh_Init_m0997F07C4A65D9684661F19C98DD9B0637ACCE96,
	CheckBoxBeh_Switcher_mE2A5DF844CB50826A4CE2F65B28AF838CF37BAE3,
	CheckBoxBeh_Anim_m4C11517FDC91F8A84F257392FE8BCA18ECF90BAF,
	CheckBoxBeh__ctor_mA5200223D1EEB9D8275D69D7FE26E64EC6D6C332,
	U3CU3Ec__DisplayClass9_0__ctor_mA68A37A4E847EC8F295CF885D8DDD78C197FA0C1,
	U3CU3Ec__DisplayClass9_0_U3CAnimU3Eb__0_m59F45D3050DFE99BEC9E9CD5726787197FFDC0E0,
	U3CU3Ec__DisplayClass9_0_U3CAnimU3Eb__1_m6532E359BF3C3101D79AC4493F95A5DF132CCDBC,
	CheckBoxBehMus_Init_m9FEB18592EAB7F4C47D06F4A7E872ECEE0054DAB,
	CheckBoxBehMus_Switcher_mD530956C8B67C1FF6E7BF482E9D05FC39E009D06,
	CheckBoxBehMus__ctor_m4E1275BF1BFEB8DF53225C103BDA1137BE2FFB27,
	CheckBoxBehSfx_Init_mCA8A2397D813A10CC3529027BEB8E9578DF390C3,
	CheckBoxBehSfx_Switcher_mE5709A22FA91FF8EF7ED1E686EF1A29483AC283D,
	CheckBoxBehSfx_Anim_mC8E49AD557D54FBB51E970814E5AD23E02EEE667,
	CheckBoxBehSfx__ctor_m74DE136E09B3BFAE1A3175AA4668012431E25922,
	U3CU3Ec__DisplayClass2_0__ctor_m299FDC95A04148C28B0D683290F7234A8E27FDB4,
	U3CU3Ec__DisplayClass2_0_U3CAnimU3Eb__0_m2019EAE8BB86AE4290DB80C54B9A5DA695158B04,
	U3CU3Ec__DisplayClass2_0_U3CAnimU3Eb__1_m6ADFA764B62E0DCC3CC3AFC91EF3442ED3DD8FE6,
	LogoClorsChanger_ColorChange_m99C81E7CCB1FCA3BDE8BC27565980302D1CFB37F,
	LogoClorsChanger__ctor_m4368AFF6E4603BFC4B14F4BD321F3516D5D20FE9,
	RectTransformExtensions_SetDefaultScale_m756116B110805E0750BD50CFB5AC9EAD5FEB0E0C,
	RectTransformExtensions_SetPivotAndAnchors_m3739A3FDBB6ECF890D2794F3A0D140C241EAD927,
	RectTransformExtensions_GetSize_m5E15B6433CDE84F88039436390CC83E1D87EC4D9,
	RectTransformExtensions_GetWidth_m65D963AF477887BF495B2F9CB66E81B921CA32F3,
	RectTransformExtensions_GetHeight_m061AB7A3F469AD5BB5C432F69063FB29E198E3E0,
	RectTransformExtensions_SetPositionOfPivot_mB94D62C0DA9123D1A5237B7F9352F36C40ECDCE9,
	RectTransformExtensions_SetLeftBottomPosition_m9A969464222C09A18146A52B02C4A16646B12A54,
	RectTransformExtensions_SetLeftTopPosition_m262E8022AE28161E22ED3442CF58FBB35909667B,
	RectTransformExtensions_SetRightBottomPosition_m0E6A4FE54AD6CF0F22617AA9E942207757C1D64C,
	RectTransformExtensions_SetRightTopPosition_mED1168AD12631EECFD0C863BD87E7430B5CF183C,
	RectTransformExtensions_SetSize_mECA4608AA6FAE6B4F3955D38542102448BF50C7C,
	RectTransformExtensions_SetWidth_mF6725AB65F1C2BB7355870EAE41A5A3BF5E16CF5,
	RectTransformExtensions_SetHeight_m23A05DB16E19D8931FC02B61AD32D22B52ECEE83,
	BattleManager_get_PlayerTrans_m22A007F333B019DBD0E3337B1E903F380A5B8005,
	BattleManager_Start_m87E7EFEA0928318604248D3ADC75A0DC153CB808,
	BattleManager_OnDestroy_m5E2DCE2EFA52F203521771E7715377876D7D2607,
	BattleManager_StartRouts_mEFB6D66B0BA36B6B0B0713C1AE5B074E1CE7549A,
	BattleManager_CoinsDropper_m394A526819130312751C5AB1A3730B358709802F,
	BattleManager_PropsSpawner_mC5067D1A2AFFABEC2BFDD6FEBD7F92E0A275DB29,
	BattleManager_Pause_m64FC78FA91FD6E0704399C0739E22B9BD74550F4,
	BattleManager_Unpause_m9C8D46FF5745AFC37C7C1216C7D19615EAB965DD,
	BattleManager_StopGame_m0F809FBAC640CB843CD737805DF8F7C8F4FCDE5D,
	BattleManager_StartGame_m9E74E827DAC7F35D451C9996DB58092859300BB2,
	BattleManager_StopRoutines_mE7D861E5B095596F007992919DC94A1FBED9A04A,
	BattleManager__ctor_m7C2C690C92F16BD4782694DE18929CD3E7FFE577,
	U3CCoinsDropperU3Ed__16__ctor_m5B20A37FCEFC5792A1ECBBE76109AAA212EDFE78,
	U3CCoinsDropperU3Ed__16_System_IDisposable_Dispose_m2513921149CA83BF9D4ED0949FB0A1CB476A73C4,
	U3CCoinsDropperU3Ed__16_MoveNext_mBC651FF96409D2420A3FC4C4803541F3BDFA190A,
	U3CCoinsDropperU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mED52FFF71BA21C416E7500AE61BDD67E9BA46538,
	U3CCoinsDropperU3Ed__16_System_Collections_IEnumerator_Reset_m0D4F891AA49D23F2917BBE2D2C97BB9318997040,
	U3CCoinsDropperU3Ed__16_System_Collections_IEnumerator_get_Current_mF4B5A889FECEAB4450FCD66D115160011B90FF50,
	U3CPropsSpawnerU3Ed__17__ctor_m0C53E3153219B6888EFF50E1FBEBDA78A1E9FE2B,
	U3CPropsSpawnerU3Ed__17_System_IDisposable_Dispose_m5F2BA115EA1588C1E45604A9E7EE633291E6D742,
	U3CPropsSpawnerU3Ed__17_MoveNext_mF95422EEC4543954729ABC45C8812FC2012F7408,
	U3CPropsSpawnerU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD2BCDF6BD7EA85A558C6090E32A48DAFF313AB19,
	U3CPropsSpawnerU3Ed__17_System_Collections_IEnumerator_Reset_m8670F90638804486CC750EBA42D27124A4A4F4D1,
	U3CPropsSpawnerU3Ed__17_System_Collections_IEnumerator_get_Current_m54019008A86128E9F048DF85037C0518527FEB58,
	BattleUiManager_get_screenWidth_m6D85E238C793826ADA714401A92A81B03599C655,
	BattleUiManager_Start_mE843B18FE823ECE79AE644F71F21AF52A121F267,
	BattleUiManager_OnDestroy_m0D8E7DB1A7A9441EE4B68D394882F354617ED917,
	BattleUiManager_RectLerper_m0381C4B0C57388EBA10CA3C1C8EDEAA217AB3EF8,
	BattleUiManager_GoToMenuScene_mB382857A123BEA1A6918A9ACE06877837E96C75E,
	BattleUiManager_GoToBattleScene_m93DA5B4C04D3D935FCE859564B3F4A366A88B81A,
	BattleUiManager_GoToSettings_mC91798FAB041A48A6CEAFF5BD92EE7F56E2A6685,
	BattleUiManager_BackFromSettings_mA439894FAB4873C9BD53CE9BA8E786054B4E7217,
	BattleUiManager_StartGame_m184774B8BEACE8A21933D26FC36AC2E02FA6CF68,
	BattleUiManager_SaveScore_mADA825146532AF5C7B083959AE4854B75D87C832,
	BattleUiManager_ShowResultWIndow_m28BF86BD732209A7BACDAFDE378B1ABB4B6D5D5C,
	BattleUiManager__ctor_mFEB5665F3BFA8264611702E7EB164AA7C0BD694E,
	U3CRectLerperU3Ed__22__ctor_mFE4BC35956BB1F5CAEB16BB2382C1C7BAB5FA335,
	U3CRectLerperU3Ed__22_System_IDisposable_Dispose_m3BA1315708803BD83C616C77D7A255BC598135F1,
	U3CRectLerperU3Ed__22_MoveNext_m644DB85F89D46040A80F948208A4E1F92C61184B,
	U3CRectLerperU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9E6376E59EBF96B900D203887FE32F6EE74A3AC7,
	U3CRectLerperU3Ed__22_System_Collections_IEnumerator_Reset_m0395461C746FA32266CEAB64C0CAF0B1F5D14219,
	U3CRectLerperU3Ed__22_System_Collections_IEnumerator_get_Current_m2F39E8D4723F7EA8CE96441AB4D337E0956F07A5,
	EventManager_get_ManagerLoaded_m0CDF1ABA9D71D9FDF98A55DED0580ABE7BB09119,
	EventManager_set_ManagerLoaded_mF465E0FC890BAFCD46E08A7B45C341CCFF51180C,
	EventManager_InitManager_mA82C80579104290B0B3F9EACEB60A7338D84BECD,
	NULL,
	NULL,
	NULL,
	NULL,
	EventManager_ClearAllEvents_m21D2168AAA4F9D43DB4CD2E52AEA1E332F3C9824,
	EventManager_ClearAllEventsBut_mDE1909B896E1E3A4B6F71B021849CB2A01EEA723,
	EventManager_ReloadManager_m1C639031EEB26BA2A292EC97245F4E1047E218EE,
	EventManager_OnDisable_m331BD5C8ECB71257C3FD01D38ED36AB6ACB0F517,
	EventManager__ctor_m5C11E791A8B96426FB471901E5907CC78EB362E4,
	GameCore_Awake_m55A860B8AE16BA8FD8CAE492C12B607227F5211F,
	GameCore_InitCore_m8B8D858F49C6159AC07980638C0F6155DCFF0B3C,
	GameCore_ManagersInit_m628635156AB71C4AB20CD389F9FA13204DA224C6,
	GameCore__ctor_m5B2B882D40F3C2F7BE008F519B0038B1F04AAE48,
	GameCore__cctor_mCEB86C2ED43FB983B668205684D7E21580516DB6,
	Info_get_GameCoreIsLoaded_m9CDDD2913BDC5E60498E144D567CF787E7790F44,
	Managers_get_ScenesManager_m9CBDA8A26D4E496A22AD25136F595882923E5473,
	Managers_get_EventManager_m26C1B23AB798F474FC2DC701D8E41BA71FAFB22B,
	Managers_get_PoolManager_m8C6B883E85E3B151FA32350DC2882F27DA9BE6A2,
	Managers_get_MenuManager_m810D98BC12C0B35E1C3561C74E1B400261E9F85C,
	Managers_get_SoundsPlayerManager_mEE039E8C9E04636A04B19251E3B3B028B9C4F8C2,
	U3CInitCoreU3Ed__3__ctor_mCCAD943136895CF43DF40DAD28D6D05461041FA7,
	U3CInitCoreU3Ed__3_System_IDisposable_Dispose_m214566DFB3B6CC50D2C9CC9AD7B65452E2FDBCF9,
	U3CInitCoreU3Ed__3_MoveNext_m703072F3DBF5336A53ED0B4EE02D0889BC9539DE,
	U3CInitCoreU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF66EBC679A96E1B1F5BFF2DD05003DFA1A3C8BF7,
	U3CInitCoreU3Ed__3_System_Collections_IEnumerator_Reset_mE2AC84D59ACB0435151AC467DBC64F928500E0EA,
	U3CInitCoreU3Ed__3_System_Collections_IEnumerator_get_Current_mAAA0F1C1AC6138B672DC1A1CD62FC68E5CE1ED33,
	U3CManagersInitU3Ed__4__ctor_m9D17335FFA81B6E19CFAC5F36D195B5B6465EA49,
	U3CManagersInitU3Ed__4_System_IDisposable_Dispose_m3BC8E51F2662DD30EA6D7F7F5ECF3D3DA7599584,
	U3CManagersInitU3Ed__4_MoveNext_mF2C1957685E71DFB25732A4EF16A53180A7B947A,
	U3CManagersInitU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDBAA898997E6235197484255E05B6FF5E23EDAE8,
	U3CManagersInitU3Ed__4_System_Collections_IEnumerator_Reset_m2FBF32B1158983877657F90DEFDA128510E8FCA9,
	U3CManagersInitU3Ed__4_System_Collections_IEnumerator_get_Current_m00C0BEFE87BD041DAEEDC27CA297B07357197B3D,
	MenuManager_get_screenWidth_mF99F0B9631E3B0727CF01F63B56275D9440561EA,
	MenuManager_GoToSettings_m5214A257804B21F7C46111349B17E50869C82949,
	MenuManager_BackToMainWindow_mDBB195CCACFA95FA20B98951391086D80D901B4A,
	MenuManager_RectLerper_m673FD921A02A52C9AEEA52EBBB0BA25822612C27,
	MenuManager_PlayClicked_m79AE20784456C671AFD84F7467AE17827654360A,
	MenuManager__ctor_m987520DA540050F86F6FC46014A7C2CE617B0BB7,
	U3CRectLerperU3Ed__7__ctor_m30B56C1B8F94B603CDD28B9094630DBCC58DBB37,
	U3CRectLerperU3Ed__7_System_IDisposable_Dispose_m07B7C2EF1F8B0D6485FC4D0AC507D3047424491C,
	U3CRectLerperU3Ed__7_MoveNext_mA07A95F0A6BCF91EA95A49E130BB8B505DDFB0DD,
	U3CRectLerperU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0DE627B04D8C907BA9536B54F2DDCCA4E18CB2D5,
	U3CRectLerperU3Ed__7_System_Collections_IEnumerator_Reset_m087D959A04E82FA45CF5482C60AE988A0E886B50,
	U3CRectLerperU3Ed__7_System_Collections_IEnumerator_get_Current_mD7CE3F95967A694D0005D7CD0A7792718586875E,
	PoolManager_get_ManagerLoaded_mEA8DC0C106DAFF98B4E44BE29BD05D25B898CFD6,
	PoolManager_set_ManagerLoaded_mB1934AED08E82FA5E05C24928872827F339EB026,
	PoolManager_InitManager_m8BA3F7B828A2EF8415AAE0F4643131F37FC47AF6,
	PoolManager_ReloadManager_m321BBC86F78D1B498F13D4D25C926BDD0702F3C5,
	PoolManager_GetObj_mEABC8FEE015CACFF56AAF14609FED4B0F31C6712,
	PoolManager_GetObjectWithTimer_m71EFD815C24FB9EEFEAC32C1FB9524CDD52E4E92,
	PoolManager_StoreObj_m3693648A1673A06264CDBEC6B5826960BC8F59BF,
	PoolManager_DelayedStore_m5EDF6D0D777DAC9E6D302B544C76ACEE2A2CCF17,
	PoolManager_DelayedExecuteRout_m925F863DF8679D4EAD358D9CEF05963B6DA83ADB,
	PoolManager_StoreEverithing_m54F97D7594D5B675AF76E3ED800992D7EAF4CB61,
	PoolManager__ctor_mAB5AC56BD81688C8EA329ADD3CEFA86FA0D12060,
	U3CU3Ec__DisplayClass11_0__ctor_mA71F5A03F602E54EB53C989C91721E74E3BF1196,
	U3CU3Ec__DisplayClass11_0_U3CDelayedStoreU3Eb__0_m1D5193886E722EFC5BFA01DCD3140E973837E10F,
	U3CDelayedExecuteRoutU3Ed__12__ctor_mD9E6417E2A02D10C7437F9EB77CE8D88F83AB8E8,
	U3CDelayedExecuteRoutU3Ed__12_System_IDisposable_Dispose_m9ACD873B22A38611DBA9C4ACBD113A473FE78DB8,
	U3CDelayedExecuteRoutU3Ed__12_MoveNext_m16D259BE12B3167513F74AB5B2AA659E7E927918,
	U3CDelayedExecuteRoutU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDE80C087E891DEAEC694EA8BC3153EEAD3D96953,
	U3CDelayedExecuteRoutU3Ed__12_System_Collections_IEnumerator_Reset_mC12C650FE8CF3A3377E0CB43B7C950128D2666F0,
	U3CDelayedExecuteRoutU3Ed__12_System_Collections_IEnumerator_get_Current_m9C8E239CAC36693CAB41741151A0461C56FD1137,
	ScenesManager_get_ManagerLoaded_m9853E6F6C20C9A0A8B8AE1CCB175DBC4A6CC1A7B,
	ScenesManager_set_ManagerLoaded_m498FBC0E72C9775D90B33FB968326DDA1AE027BB,
	ScenesManager_InitManager_m431D0F1C232148EBC0660BB89F8F3A7FB7598002,
	ScenesManager_ReloadManager_m47F9946E7108DC995F8BF282907394A348008B21,
	ScenesManager_LoadBattle_m4B99735109F3E5E7B4D8562392ABBB6F2B8B88B2,
	ScenesManager_LoadMenu_mBCB6C19089DB732CC8872BC0236F36676F8F5EB7,
	ScenesManager_LoadMenu_mB5F53C0CBB07D0F8DCCD0582718698BE354350A9,
	ScenesManager_LoadBattleRout_m6E134CC1BC70F1A493C32FE27BDE87D0B9796218,
	ScenesManager_FadeOut_mEBB374EF89EF0B3E0877D98781086296C31A243B,
	ScenesManager_FadeIn_m87C3AEC0086B75BBFDA66725C0E3F6621222D69B,
	ScenesManager_DisableImage_m632A6EEEA32B9CAB3D63D714F91C777BF0B1176E,
	ScenesManager_Update_m7E98163D9637E6DF78D365658DFAF1DACBD7B258,
	ScenesManager_LoadBundle_m6E2FD6AEC587F263C243CE835383630A5C4A840D,
	ScenesManager_GetBundlesVersionInfo_m877050724519A0516789CF3A91D43C14F5CFC385,
	ScenesManager__ctor_mB5A34F3B7CB07104B2A46DD948E0C75793F29A17,
	U3CLoadMenuU3Ed__28__ctor_m83C8D766D0CA9BCE7DEC0052E90A6478B49DF083,
	U3CLoadMenuU3Ed__28_System_IDisposable_Dispose_mCCA53E59ABB7B1BF4C661A5FEE7D434EC29D695A,
	U3CLoadMenuU3Ed__28_MoveNext_m79ADB24A9F941386C16E91D2E7C7C7B5D071D476,
	U3CLoadMenuU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD0B1FF8419F91341A03CF453DDF1D7BBA16C16AB,
	U3CLoadMenuU3Ed__28_System_Collections_IEnumerator_Reset_mF54E18EE6E6C2D73AA2A862B2003F8A6FF3873BF,
	U3CLoadMenuU3Ed__28_System_Collections_IEnumerator_get_Current_m8C521593D1D04B269A3554CC7C18CEB8E5C17F28,
	U3CLoadBattleRoutU3Ed__29__ctor_m49F6DDD14DC52A8E2123C7F81BED86B23CB4A997,
	U3CLoadBattleRoutU3Ed__29_System_IDisposable_Dispose_m75BE0F49721F852BAEEBCEC8EE7B1B74CBF57D76,
	U3CLoadBattleRoutU3Ed__29_MoveNext_m8E9B6A2FF17CD5874E0DD1ED52B97F63E2A242F4,
	U3CLoadBattleRoutU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1BF2ABDA675CFC41535E484C711B664B0E3FE2F3,
	U3CLoadBattleRoutU3Ed__29_System_Collections_IEnumerator_Reset_mEA0F3257C44B1B2EE12C96B014A43D9F5C06BA3C,
	U3CLoadBattleRoutU3Ed__29_System_Collections_IEnumerator_get_Current_mC09B797D42E91CDDD03F19717E8BD5EFBD03573C,
	U3CLoadBundleU3Ed__34__ctor_mA0E45B9A1CAC6DF82370452648B4E06B8C8D4AB2,
	U3CLoadBundleU3Ed__34_System_IDisposable_Dispose_mD7EDC1DDC94917E540440B118748896313146141,
	U3CLoadBundleU3Ed__34_MoveNext_m4C9D6EADA30FDA46E21C9B01D3DBB4C359A0859C,
	U3CLoadBundleU3Ed__34_U3CU3Em__Finally1_m3ADE189D4915A6B08129BF8C6E2B9B00C4DB5ADE,
	U3CLoadBundleU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEC471A946990DDDC262A7554F9921392ED0015CB,
	U3CLoadBundleU3Ed__34_System_Collections_IEnumerator_Reset_mC3E16195AF7E2154763F4D93431FE337E2BCF4F7,
	U3CLoadBundleU3Ed__34_System_Collections_IEnumerator_get_Current_mDFF6DFD397416B0B367B626FC77730FB584084D0,
	U3CGetBundlesVersionInfoU3Ed__35__ctor_mDDE8FD2C504647E80E61EA74994A1088842B0AFB,
	U3CGetBundlesVersionInfoU3Ed__35_System_IDisposable_Dispose_m51692A5E82E73E57C977E6505ACFE136F76DDCEE,
	U3CGetBundlesVersionInfoU3Ed__35_MoveNext_m80A91D071D4E6588465CDA28B26BF666AEE49EC3,
	U3CGetBundlesVersionInfoU3Ed__35_U3CU3Em__Finally1_m06621A51E45F0E9A2C740AA696CBAAD2B69A9573,
	U3CGetBundlesVersionInfoU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m57156DE3AB27AB6D386535BCB919FFB23D356260,
	U3CGetBundlesVersionInfoU3Ed__35_System_Collections_IEnumerator_Reset_m09D18BFEF78670C3C13CD082C67704B45BB9B75D,
	U3CGetBundlesVersionInfoU3Ed__35_System_Collections_IEnumerator_get_Current_m5C57BB4F73F450C9DA8370F4F95A126E7ADF487D,
	SoundsPlayerManager_get_ManagerLoaded_m9414366FE26AB2E3DA2A51F41BF130FB941BD6DD,
	SoundsPlayerManager_set_ManagerLoaded_m19ECE5D36E48CADC44A635B447A3118F27CEACB2,
	SoundsPlayerManager_get_IsMusicOn_m1B4AE98BA33C0B42882EAE5DA37784ADDD1A85C7,
	SoundsPlayerManager_set_IsMusicOn_m76345AC43E617E1DF6892232BFE151BE108EF821,
	SoundsPlayerManager_get_IsSfxOn_m34E199685D320CE232180EAC17708945FB22ED31,
	SoundsPlayerManager_set_IsSfxOn_m454038EABCA58D8C1169C251EB29032758180BE4,
	SoundsPlayerManager_InitManager_mA490057E1979AA812171B450CE5429451BF48303,
	SoundsPlayerManager_ReloadManager_m8E4D9167DC61190551DF48B66DCFBE0510539CB3,
	SoundsPlayerManager_PlaySound_m73CCAA90E9ACBD4D9E9524084012898F787997DB,
	SoundsPlayerManager_StopPlayingSound_m955084EFC869E3A3D1CA1542141B368C6710C94C,
	SoundsPlayerManager_PlayLoopingSound_m0A2F74BF5F943C93E6F775CFEBE5C20F39BB347C,
	SoundsPlayerManager_MusicSettingsChanged_mA14729B6936FD71170E6A2AF47E3312318450C54,
	SoundsPlayerManager_SfxSettingsChanged_m2DBFE84BC53DFAF30EB0269C77792C9E75938801,
	SoundsPlayerManager_TryPlayMenuMus_mB09CBE076C0C8F4CCC872697703AA3E830235E6F,
	SoundsPlayerManager_TryPlayBattleMus_mD64963AB53388E8A8FB1395FD85129EE2A88B51B,
	SoundsPlayerManager_GetSource_mBB708DB1715892EDB160E436AD74E3F92F7223A0,
	SoundsPlayerManager__ctor_m700A9BC79469C4848662333E4EC231D13754C6F4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PlayerController_Start_mF64B77CC329DE34A8045AE0C940B514587556DB8,
	PlayerController_OnDestroy_m5985E026FE030644DBEEBDA8C3397D1778B3E739,
	PlayerController_Update_m648FD430DD4F92DDFB8DF8D6A43B5EB18568CB82,
	PlayerController_DeviceInput_m7383D4E09D3E1A24F9E0FEFD8DD9153C9D8E46A1,
	PlayerController_UnityInput_m5A04F6A17CD494367146B37FFAAF560E143DBFFA,
	PlayerController_SetRandoHat_mB20BEDCF3A08A340653C305FA45DEA2D355B1B7F,
	PlayerController_OnTriggerEnter_mB4A82DDBE0722DF3770696CEAAA9430ADCF39BB4,
	PlayerController_PlayerDie_m5AF9D3B1B42627E2833BA81423C3ABECEE778480,
	PlayerController_GreedDeath_mD3FCDA1BB8A23CE9E13D9EF2B0B3F759FF021403,
	PlayerController_StartRun_m538A2496290D2C3CACA749D95E894FF7C17F1A2A,
	PlayerController_StopRun_m7D55B447F8102133C2EFCFBB17366D61710A5402,
	PlayerController_ContinueRun_mCBE1F8739BA551FE2A80673560617F2794AB48AD,
	PlayerController_RunRout_mA22B2A74036B92E6C4B3B76B212981F7F44AE3D4,
	PlayerController_SayHello_mA637BCB7DB14816CCBC35803E77F9CF1B5385C26,
	PlayerController_Rotator_m441BB00CC5F6329CAF42EE05CD76279368F9ED09,
	PlayerController__ctor_m13056FC83D815319EB5775895D761DA8CC5DF7BE,
	PlayerController__cctor_m1787AFA429A030E78DE7E659642C31C17D83BF2F,
	U3CRunRoutU3Ed__30__ctor_mCA94540393D6FF23BFAEE95F33AC7F75D69CDAF9,
	U3CRunRoutU3Ed__30_System_IDisposable_Dispose_mD9B16A132D4A399D11929531A6CE9AFBC63EA4EE,
	U3CRunRoutU3Ed__30_MoveNext_m0D1027224C343700B1363E500361CEAD8291BBBD,
	U3CRunRoutU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m205313068F1B73E8EA33743E806702693E22766D,
	U3CRunRoutU3Ed__30_System_Collections_IEnumerator_Reset_m5B8DA141B9C088A61F3302CB6C7F279A7E475A88,
	U3CRunRoutU3Ed__30_System_Collections_IEnumerator_get_Current_m7EEC891E87CA1E9B2C39B4355D7F4553F0C9D0A8,
	U3CSayHelloU3Ed__31__ctor_mA603E85A8DB0B4D7B67E720F7530EC6BBA9DDF7E,
	U3CSayHelloU3Ed__31_System_IDisposable_Dispose_m23FC1D002C29FCD2BF7E5430B56A3D92BF2C3816,
	U3CSayHelloU3Ed__31_MoveNext_mF8C6CF4333D756B4EEE05C30FC9A9093B373546B,
	U3CSayHelloU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m788DCE676AD27D2E45809A95C2D721FF39525713,
	U3CSayHelloU3Ed__31_System_Collections_IEnumerator_Reset_m0CE46B566DA780F05F8FEC06127AB405182FA7E2,
	U3CSayHelloU3Ed__31_System_Collections_IEnumerator_get_Current_mACA149E6143CEEC49B09738A94AEC131886D2F80,
	U3CRotatorU3Ed__32__ctor_m48279ADDC8D8C49C9C9B8B818F1CA7CBAF0C1057,
	U3CRotatorU3Ed__32_System_IDisposable_Dispose_mB23FB46D08F56888E6913A5D78182521004F0F81,
	U3CRotatorU3Ed__32_MoveNext_m91FEBB66B89346CB884EAF429A47B7CB5C16655E,
	U3CRotatorU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0114479E05754C49ECF28B18186E2E2C8682AD31,
	U3CRotatorU3Ed__32_System_Collections_IEnumerator_Reset_m1FF085920A616461360A3512B2185E8CE39381A3,
	U3CRotatorU3Ed__32_System_Collections_IEnumerator_get_Current_mB4B770B1451A7086036EFAB0EC8056DFE025FAB5,
};
extern void BundleInfo__ctor_mF4DB7CACD68EBEE05A80861D744354744816008F_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[1] = 
{
	{ 0x06000105, BundleInfo__ctor_mF4DB7CACD68EBEE05A80861D744354744816008F_AdjustorThunk },
};
static const int32_t s_InvokerIndices[512] = 
{
	3863,
	3823,
	4282,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4293,
	4394,
	4371,
	4391,
	4287,
	4388,
	-1,
	-1,
	-1,
	4057,
	4057,
	4057,
	4053,
	4057,
	4057,
	4496,
	4364,
	4372,
	4372,
	4505,
	4505,
	4505,
	4056,
	4056,
	3629,
	4079,
	4491,
	4492,
	4493,
	4139,
	3776,
	4557,
	3528,
	3259,
	3529,
	3529,
	3528,
	4138,
	4138,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	1374,
	4152,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2429,
	2429,
	2899,
	2899,
	2899,
	2899,
	2840,
	2899,
	2899,
	2488,
	2899,
	2899,
	2899,
	2899,
	2411,
	2899,
	2899,
	2899,
	2899,
	2411,
	2899,
	2899,
	2899,
	2899,
	2429,
	2454,
	2899,
	2899,
	2899,
	3771,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2862,
	2899,
	2899,
	2899,
	2899,
	2899,
	2454,
	2899,
	2899,
	2899,
	2840,
	2899,
	2411,
	2899,
	2862,
	2840,
	2899,
	2840,
	2899,
	2899,
	3771,
	2899,
	2899,
	2899,
	2429,
	2840,
	2899,
	2411,
	2899,
	2862,
	2840,
	2899,
	2840,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2469,
	2437,
	2431,
	2484,
	2899,
	2840,
	2840,
	2367,
	2454,
	2899,
	2411,
	2899,
	2862,
	2840,
	2899,
	2840,
	2411,
	2899,
	2862,
	2840,
	2899,
	2840,
	2899,
	2899,
	2899,
	2899,
	2899,
	2840,
	2840,
	2454,
	2899,
	2899,
	2840,
	2899,
	2899,
	2429,
	2899,
	2411,
	2899,
	2862,
	2840,
	2899,
	2840,
	2411,
	2899,
	2862,
	2840,
	2899,
	2840,
	2411,
	2899,
	2862,
	2840,
	2899,
	2840,
	2411,
	2899,
	2899,
	2367,
	1123,
	2454,
	2437,
	2899,
	2411,
	2899,
	2862,
	2840,
	2899,
	2840,
	2899,
	2899,
	2899,
	2899,
	2488,
	2454,
	2899,
	2899,
	1387,
	2899,
	2899,
	2363,
	2362,
	2451,
	2899,
	2899,
	2899,
	4559,
	2899,
	2899,
	2899,
	2899,
	2899,
	1493,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	1493,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	4505,
	4173,
	4486,
	4407,
	4407,
	4173,
	4173,
	4173,
	4173,
	4173,
	4173,
	4170,
	4170,
	4543,
	2899,
	2899,
	2899,
	2840,
	2840,
	2431,
	2484,
	2437,
	2469,
	2899,
	2899,
	2411,
	2899,
	2862,
	2840,
	2899,
	2840,
	2411,
	2899,
	2862,
	2840,
	2899,
	2840,
	2824,
	2899,
	2899,
	1153,
	2899,
	2899,
	2899,
	2899,
	2899,
	2428,
	1399,
	2899,
	2411,
	2899,
	2862,
	2840,
	2899,
	2840,
	2862,
	2451,
	2899,
	-1,
	-1,
	-1,
	-1,
	2899,
	2429,
	2899,
	2899,
	2899,
	2899,
	2840,
	2840,
	2899,
	4559,
	4551,
	4543,
	4543,
	4543,
	4543,
	4543,
	2411,
	2899,
	2862,
	2840,
	2899,
	2840,
	2411,
	2899,
	2862,
	2840,
	2899,
	2840,
	2824,
	2899,
	2899,
	1153,
	2899,
	2899,
	2411,
	2899,
	2862,
	2840,
	2899,
	2840,
	2862,
	2451,
	2899,
	2899,
	1136,
	796,
	2429,
	1496,
	1146,
	2899,
	2899,
	2899,
	2899,
	2411,
	2899,
	2862,
	2840,
	2899,
	2840,
	2862,
	2451,
	2899,
	2899,
	2401,
	2402,
	2840,
	2840,
	2899,
	2899,
	2451,
	2899,
	2840,
	2840,
	2899,
	2411,
	2899,
	2862,
	2840,
	2899,
	2840,
	2411,
	2899,
	2862,
	2840,
	2899,
	2840,
	2411,
	2899,
	2862,
	2899,
	2840,
	2899,
	2840,
	2411,
	2899,
	2862,
	2899,
	2840,
	2899,
	2840,
	2862,
	2451,
	2862,
	2451,
	2862,
	2451,
	2899,
	2899,
	2411,
	2411,
	2411,
	2426,
	2453,
	2424,
	2359,
	2840,
	2899,
	2862,
	2899,
	2899,
	-1,
	-1,
	-1,
	2899,
	2899,
	2899,
	2899,
	2899,
	2899,
	2429,
	2899,
	2404,
	2469,
	2431,
	2484,
	2840,
	2840,
	2840,
	2899,
	4559,
	2411,
	2899,
	2862,
	2840,
	2899,
	2840,
	2411,
	2899,
	2862,
	2840,
	2899,
	2840,
	2411,
	2899,
	2862,
	2840,
	2899,
	2840,
};
static const Il2CppTokenRangePair s_rgctxIndices[16] = 
{
	{ 0x0200006B, { 70, 3 } },
	{ 0x06000004, { 0, 8 } },
	{ 0x06000005, { 8, 8 } },
	{ 0x06000006, { 16, 5 } },
	{ 0x06000007, { 21, 2 } },
	{ 0x06000008, { 23, 2 } },
	{ 0x06000009, { 25, 5 } },
	{ 0x0600000A, { 30, 6 } },
	{ 0x0600000B, { 36, 6 } },
	{ 0x06000012, { 42, 2 } },
	{ 0x06000013, { 44, 3 } },
	{ 0x06000014, { 47, 4 } },
	{ 0x0600015F, { 51, 7 } },
	{ 0x06000160, { 58, 4 } },
	{ 0x06000161, { 62, 5 } },
	{ 0x06000162, { 67, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[73] = 
{
	{ (Il2CppRGCTXDataType)3, 2541 },
	{ (Il2CppRGCTXDataType)2, 3176 },
	{ (Il2CppRGCTXDataType)3, 9630 },
	{ (Il2CppRGCTXDataType)3, 2540 },
	{ (Il2CppRGCTXDataType)3, 5661 },
	{ (Il2CppRGCTXDataType)3, 9156 },
	{ (Il2CppRGCTXDataType)3, 9631 },
	{ (Il2CppRGCTXDataType)3, 5660 },
	{ (Il2CppRGCTXDataType)3, 2543 },
	{ (Il2CppRGCTXDataType)2, 3193 },
	{ (Il2CppRGCTXDataType)3, 9670 },
	{ (Il2CppRGCTXDataType)3, 2542 },
	{ (Il2CppRGCTXDataType)3, 5663 },
	{ (Il2CppRGCTXDataType)3, 9157 },
	{ (Il2CppRGCTXDataType)3, 9671 },
	{ (Il2CppRGCTXDataType)3, 5662 },
	{ (Il2CppRGCTXDataType)3, 2537 },
	{ (Il2CppRGCTXDataType)3, 2536 },
	{ (Il2CppRGCTXDataType)3, 5655 },
	{ (Il2CppRGCTXDataType)3, 5656 },
	{ (Il2CppRGCTXDataType)3, 5654 },
	{ (Il2CppRGCTXDataType)3, 18501 },
	{ (Il2CppRGCTXDataType)3, 9154 },
	{ (Il2CppRGCTXDataType)3, 18502 },
	{ (Il2CppRGCTXDataType)3, 9158 },
	{ (Il2CppRGCTXDataType)3, 2539 },
	{ (Il2CppRGCTXDataType)3, 2538 },
	{ (Il2CppRGCTXDataType)3, 5658 },
	{ (Il2CppRGCTXDataType)3, 5659 },
	{ (Il2CppRGCTXDataType)3, 5657 },
	{ (Il2CppRGCTXDataType)3, 2545 },
	{ (Il2CppRGCTXDataType)3, 2544 },
	{ (Il2CppRGCTXDataType)3, 5665 },
	{ (Il2CppRGCTXDataType)3, 5666 },
	{ (Il2CppRGCTXDataType)3, 9159 },
	{ (Il2CppRGCTXDataType)3, 5664 },
	{ (Il2CppRGCTXDataType)3, 2535 },
	{ (Il2CppRGCTXDataType)3, 2534 },
	{ (Il2CppRGCTXDataType)3, 5652 },
	{ (Il2CppRGCTXDataType)3, 5653 },
	{ (Il2CppRGCTXDataType)3, 9155 },
	{ (Il2CppRGCTXDataType)3, 5651 },
	{ (Il2CppRGCTXDataType)3, 9614 },
	{ (Il2CppRGCTXDataType)3, 9615 },
	{ (Il2CppRGCTXDataType)3, 9612 },
	{ (Il2CppRGCTXDataType)3, 9613 },
	{ (Il2CppRGCTXDataType)3, 9611 },
	{ (Il2CppRGCTXDataType)3, 9609 },
	{ (Il2CppRGCTXDataType)3, 9610 },
	{ (Il2CppRGCTXDataType)2, 214 },
	{ (Il2CppRGCTXDataType)3, 9608 },
	{ (Il2CppRGCTXDataType)1, 148 },
	{ (Il2CppRGCTXDataType)2, 3197 },
	{ (Il2CppRGCTXDataType)3, 9679 },
	{ (Il2CppRGCTXDataType)3, 9682 },
	{ (Il2CppRGCTXDataType)3, 9683 },
	{ (Il2CppRGCTXDataType)3, 9681 },
	{ (Il2CppRGCTXDataType)3, 9680 },
	{ (Il2CppRGCTXDataType)1, 150 },
	{ (Il2CppRGCTXDataType)2, 3199 },
	{ (Il2CppRGCTXDataType)3, 9686 },
	{ (Il2CppRGCTXDataType)3, 9687 },
	{ (Il2CppRGCTXDataType)1, 149 },
	{ (Il2CppRGCTXDataType)2, 3198 },
	{ (Il2CppRGCTXDataType)3, 9685 },
	{ (Il2CppRGCTXDataType)3, 361 },
	{ (Il2CppRGCTXDataType)3, 9684 },
	{ (Il2CppRGCTXDataType)1, 147 },
	{ (Il2CppRGCTXDataType)2, 3196 },
	{ (Il2CppRGCTXDataType)3, 9678 },
	{ (Il2CppRGCTXDataType)2, 3754 },
	{ (Il2CppRGCTXDataType)2, 574 },
	{ (Il2CppRGCTXDataType)1, 574 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	512,
	s_methodPointers,
	1,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	16,
	s_rgctxIndices,
	73,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
